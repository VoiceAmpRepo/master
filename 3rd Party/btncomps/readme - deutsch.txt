ButtonComponents v1.0
f�r Delphi 2,3,4,5

Copyright � 1999 Yunus Hi-Tec Labs
Alle Rechte vorbehalten.

Autor: Yunus Morkramer

=====================================================================

1. Installation
2. �ber ButtonComponents
3. Fehlerbehebungen
4. Lizenz

=====================================================================


1. Installation
================

- Starten Sie Delphi.
- Klicken Sie auf Komponente/Installieren
- W�hlen Sie die Datei ButtonComps.pas
- Klicken Sie auf OK.


2. �ber ButtonComponents
=========================

ButtonComponents stellt Ihnen 5 verschieden gestaltelte Buttons zur 
Verf�gung. Wir hoffen dass Sie Ihnen gefallen.


3. Fehlerbehebungen
====================

Delphi 4:

Fehler beim Compilieren:
- Datei nicht gefunden "ButtonComps.dcu",
Fehlerbehebung:
- 1. Klicken Sie auf Tools/Umgebungsoptionen.
  2. Wechseln Sie zu "Bibliothek".
  3. F�gen Sie am Ende des Bibliothekpfades den Pfad des 
     ButtonComponents-Verzeichnis ein.
   

5. Lizenz
==========

ButtonComponents ist Freeware. Sie d�rfen alle Buttons ohne 
irgendwelche Verpflichtungen verwenden.

=====================================================================

F�r weitere Delphi Komponenten, besuchen Sie unsere Homepage:
www.hi-tec-labs.de

F�r Fragen, Kritik etc. schicken Sie uns eine Mail:
info@hi-tec-labs.de
