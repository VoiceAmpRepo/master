
{
This is a simple browser demo using the ThtmlLite HTML display component and the
Internet Direct (Indy) internet components.

Important!

This demo is for use with Indy version 9.x.  It will not work with version 8
or 10.

Version 9 of the Indy components comes with Delphi 7.  The version that comes
with Delphi 6 is version 8.  To compile this demo using Delphi 4, 5, or 6, you
will need to download and install the Indy components.  They are available
(no charge) at http://www.nevrona.com/Indy/.

This is a very basic demo designed to illustrate downloading and displaying an
HTML document and its images.  It demostrates the use of ThtmlLite's
OnImageRequest event and InsertImage method to handle the image downloading.
To keep things simple, many browser nicities (requirements) have been omitted,
such as:

  Form submission
  Non HTML downloads (zip files, etc)
  Disk Caching (although images are cached)
  Proxies
  History list
  Cookies
  Frames

For examples that cover the above, see the demo program for the
TFrameBrowser component available at www.pbear.com, filename brzdemoXXX.zip.
}

unit LiteBrows;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Gauges, HTMLLite, StdCtrls, Buttons, ExtCtrls, UrlSubs,
  IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, ToolWin, ComCtrls, ImgList, Menus;

const
  wm_LoadURL = wm_User+124;
  wm_DoImages = wm_User+127;

type
  THTTPForm = class(TForm)
    Viewer: ThtmlLite;
    Panel20: TPanel;
    Status3: TPanel;
    Status2: TPanel;
    IdHTTP: TIdHTTP;
    IdAntiFreeze1: TIdAntiFreeze;
    CoolBar1: TCoolBar;
    UrlCombobox: TComboBox;
    ToolBar1: TToolBar;
    BackButton: TToolButton;
    ForwardButton: TToolButton;
    Panel1: TPanel;
    ToolBar2: TToolBar;
    GoButton: TToolButton;
    AbortButton: TToolButton;
    Panel2: TPanel;
    ImageList1: TImageList;
    Animate: TAnimate;
    Status1: TPanel;
    Gauge: TGauge;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    OpenLocalFile1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    OpenDialog: TOpenDialog;
    procedure GoButtonClick(Sender: TObject);
    procedure ViewerImageRequest(Sender: TObject; const SRC: String;
      var Stream: TMemoryStream);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure URLComboBoxKeyPress(Sender: TObject; var Key: Char);
    procedure ViewerHotSpotClick(Sender: TObject; const SRC: String;
      var Handled: Boolean);
    procedure AbortButtonClick(Sender: TObject);
    procedure ViewerHotSpotCovered(Sender: TObject; const SRC: String);
    procedure BackButtonClick(Sender: TObject);
    procedure ForwardButtonClick(Sender: TObject);
    procedure UrlComboboxClick(Sender: TObject);
    procedure IdHTTPWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure IdHTTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure OpenLocalFile1Click(Sender: TObject);
  private
    { Private declarations }
    UrlBase: string;
    NewLoadUrl: string;
    ImageList: TStringList;    {a list of images to load}
    MStream: TMemoryStream;
    NumImageTot, NumImageDone: integer;
    WorkCountMax: integer;
    CurrentLocalFile: string;
    procedure LoadViewer(const Url: string);
    procedure WMDoImages(var Message: TMessage); message WM_DoImages;
    procedure WMLoadURL(var Message: TMessage); message WM_LoadURL;
    procedure CheckEnableControls;
    procedure DisableControls;
    procedure EnableControls;
    function GetStream(const Url: string): TMemoryStream;
    procedure Progress(Num, Den: integer);
  public
    { Public declarations }
  end;

var
  HTTPForm: THTTPForm;

implementation

uses LiteUn2;   {for WaitStream definition}

{$R *.DFM}

procedure THTTPForm.FormCreate(Sender: TObject);
begin
if Screen.Width <= 800 then   {make window fit appropriately}
  begin
  Left := Left div 2;
  Width := (Screen.Width * 9) div 10;
  Height := (Screen.Height * 7) div 8;
  end
else
  begin
  Width := 850;
  Height := 600;
  end;
ImageList := TStringList.Create;
MStream := TMemoryStream.Create;
Animate.Filename := ExtractFilePath(ParamStr(0))+'starcross.avi';
end;

procedure THTTPForm.FormDestroy(Sender: TObject);
begin
ImageList.Free;
MStream.Free;
end;

function THTTPForm.GetStream(Const Url: string): TMemoryStream;
{download an HTML document or image.  Return it in stream form}
var
  Proto, S: string;
  I: integer;
begin
MStream.Clear;
Proto := GetProtocol(Url);
try
  if Proto = 'http' then
    IdHTTP.Get(Url, MStream)
  else if Proto = 'file' then
    begin
    I := Pos('file:///', Url);
    S := Copy(Url, I+8, Length(Url)-(I+7));
    S := HTMLToDos(S);
    MStream.LoadFromFile(S);
    end;
except
  MStream.Clear;
  end;
Result := MStream;
end;

procedure THTTPForm.GoButtonClick(Sender: TObject);
{initiate loading of a main document}
var
  Url: string;
begin
Url := Normalize(URLCombobox.Text);   {put in standard form}
DisableControls;
Status2.Caption := '';
NumImageTot := 0;
NumImageDone := 0;
Progress(0, 0);
Gauge.Visible := True;
try
  Animate.Active := True;
  Animate.Visible := True;
  IdHTTP.OnWork := IdHTTPWork;
  LoadViewer(URL);    
  if URLComboBox.Items.IndexOf(URL) = -1 then
    begin
    URLComboBox.Items.Add(URL);
    URLComboBox.ItemIndex := URLComboBox.Items.Count-1;
    end;
finally
  IdHTTP.OnWork := Nil;
  CheckEnableControls;
  Viewer.SetFocus;
  end;
end;

procedure THTTPForm.LoadViewer(const Url: string);
var
  Url1, Dest: string;
  I: integer;
  Stream: TMemoryStream;
begin
DisableControls;
Url1 := URL;
I := Pos('#', Url1);  {see if Url has local destination part}
if I >= 1 then
  begin
  Dest := System.Copy(Url1, I, Length(Url1)-I+1);  {local destination}
  Url1 := System.Copy(Url1, 1, I-1);     {document Url}
  end
else
  Dest := '';    {no local destination}
Stream := GetStream(Url1);   {do the download}
UrlBase := GetBase(Url1);     {save the base directory}
{while Viewer is being loaded, a series of OnImageRequest events will occur.
 see ViewerImageRequest below}
Viewer.LoadFromStream(Stream, '');
if Dest <> '' then
  Viewer.PositionTo(Dest);
end;

procedure THTTPForm.ViewerImageRequest(Sender: TObject; const SRC: String;
  var Stream: TMemoryStream);
{the OnImageRequest handler}
begin
Stream := WaitStream;   {wait indicator, means image will be inserted later}
ImageList.Add(SRC);    {add to list of images to download}
Inc(NumImageTot);
if ImageList.Count = 1 then
  PostMessage(Handle, wm_DoImages, 0, 0);
end;

procedure THTTPForm.WMDoImages(var Message: TMessage);
{loop through the ImageList to download and insert the images}
var
  S, Src: string;
begin
if ImageList.Count > 0 then
  begin
  Src := ImageList[0];
  ImageList.Delete(0);
  if not IsFullUrl(Src) then
    S := Combine(UrlBase, Src)
  else S := Src;
  Viewer.InsertImage(Src, GetStream(S));
  Inc(NumImageDone);
  Progress(NumImageDone, NumImageTot);
  if ImageList.Count > 0 then
    PostMessage(Handle, wm_DoImages, 0, 0) {more to do}
  else CheckEnableControls;
  end;
end;

procedure THTTPForm.URLComboBoxKeyPress(Sender: TObject; var Key: Char);
{trap CR in combobox}
begin
if (Key = #13) and (URLComboBox.Text <> '') then
  Begin
  Key := #0;
  GoButtonClick(Self);
  end;
end;

procedure THTTPForm.ViewerHotSpotClick(Sender: TObject; const Src: String;
  var Handled: Boolean);
{the OnHotspotClick event handler, a link has been clicked}
begin
if (Length(Src) > 0) and (Src[1] = '#') then
  begin   {it's a local jump}
  Handled := False;
  Exit;
  end;

if not IsFullUrl(Src) then
  NewLoadUrl := Combine(UrlBase, Src)
else NewLoadUrl := Src;

if GetProtocol(NewLoadUrl) = 'http' then
  begin
  {download can't be done here.  Post a message to do it later at WMLoadUrl}
  PostMessage(handle, wm_LoadUrl, 0, 0);
  Handled := True;
  end
else Handled := False;
end;

procedure THTTPForm.WMLoadURL(var Message: TMessage);
begin
UrlCombobox.Text := NewLoadUrl;
GoButtonClick(Self);
end;

procedure THTTPForm.AbortButtonClick(Sender: TObject);
begin
IdHTTP.DisconnectSocket;
ImageList.Clear;
CheckEnableControls;
end;

procedure THTTPForm.CheckEnableControls;
begin
if ImageList.Count = 0 then
  begin
  EnableControls;
  Animate.Active := False;
  Animate.Visible := False;
  Status2.Caption   := 'DONE';
  end;
end;

procedure THTTPForm.DisableControls;
begin
  URLCombobox.Enabled:=false;
  BackButton.Enabled := False;
  ForwardButton.Enabled := False;
  GoButton.Enabled := False;
  AbortButton.Enabled:=true;
end;

procedure THTTPForm.EnableControls;
begin
  URLCombobox.Enabled:=true;
  BackButton.Enabled := URLComboBox.ItemIndex > 0;
  ForwardButton.Enabled := URLComboBox.ItemIndex <= URLComboBox.Items.Count-2;
  AbortButton.Enabled:=false;
  Gauge.Visible := False;
  GoButton.Enabled := True;
end;

procedure THTTPForm.ViewerHotSpotCovered(Sender: TObject;
  const SRC: String);
{mouse moved over or away from a link.  Change the status line}
begin
  Status3.Caption := SRC;
end;

procedure THTTPForm.BackButtonClick(Sender: TObject);
begin
  if URLComboBox.ItemIndex <= 0 then
    exit;
  AbortButton.Click;
  URLComboBox.ItemIndex := URLComboBox.ItemIndex-1;
  GoButton.Click;
end;

procedure THTTPForm.ForwardButtonClick(Sender: TObject);
begin
  if URLComboBox.ItemIndex = URLComboBox.Items.Count - 1 then
    exit;
  AbortButton.Click;
  URLComboBox.ItemIndex := URLComboBox.ItemIndex+1;
  GoButton.Click;
end;

procedure THTTPForm.UrlComboboxClick(Sender: TObject);
begin
GoButton.Click;
end;

procedure THTTPForm.Progress(Num, Den: integer);
var
  Percent: integer;
begin
if Den = 0 then Percent := 0
else
  Percent := (100*Num) div Den;
Gauge.Progress := Percent;
Gauge.Update;
end;

procedure THTTPForm.IdHTTPWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
Status1.Caption := 'Text: ' + IntToStr(AWorkCount) + ' bytes';
Status1.Update;
Progress(AWorkCount, WorkCountMax);
end;

procedure THTTPForm.IdHTTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
WorkCountMax := AWorkCountMax;
end;

procedure THTTPForm.OpenLocalFile1Click(Sender: TObject);
{Open a local disk file}
begin
if CurrentLocalFile <> '' then
  OpenDialog.InitialDir := ExtractFilePath(CurrentLocalFile)
else OpenDialog.InitialDir := ExtractFilePath(ParamStr(0));
OpenDialog.FilterIndex := 1;
OpenDialog.Filename := '';
if OpenDialog.Execute then
  begin
  UrlComboBox.Text := 'file:///'+DosToHTML(OpenDialog.Filename);
  GoButtonClick(Nil);
  CurrentLocalFile := Viewer.CurrentFile;
  end;
end;

end.
