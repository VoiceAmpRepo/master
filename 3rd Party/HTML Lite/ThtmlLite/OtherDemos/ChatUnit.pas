
{This demo is to answer the many questions about how to use ThtmlLite to make
 a chat program.

 When the start button is pressed in this program, dummy chat lines are
 generated about every half second.  These are added to the display which is
 positioned to keep the most recent addition visible.

 After the number of lines reaches a predetermined limit, the oldest line is
 removed whenever a new line is added.  This keeps the display from becoming too
 unwieldy.  For demo purposes, this has been set to 40 lines but might be greatly
 increased in a real application.

 More Details

 The complete HTML document that's being displayed is kept in a TStringList
 object.  As each line is added to the StringList, the top line is removed if
 appropriate, and the StringList loaded into the ThtmlLite viewer.  Loading is
 fast enough that it's not apparent.
}

unit ChatUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, HTMLLite, ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Viewer: ThtmlLite;
    StartButton: TButton;
    Timer1: TTimer;
    StopButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
  private
    { Private declarations }
    SL: TStringList;
    Count: integer;
    Stop: boolean;
    procedure AddLine(const S: string);
  public
    { Public declarations }
  end;

const
  LineLimit = 40;    {determines how many total lines are to appear.}

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
{Create the stringlist at the start}
SL := TStringList.Create;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
{Free the stringlist at exit time}
SL.Free;
end;

procedure TForm1.AddLine(const S: string);
{Adds a line to the StringList and the ThtmlLite display.  If there are too
 many lines, removes the oldest line}
begin
SL.Add(S);   {add line to StringList}

{if too many lines, remove the top line}
if SL.Count > LineLimit then
  SL.Delete(0);

{Load the stringlist into ThtmlLite}
Viewer.LoadFromString(SL.Text, ExtractFilePath(ParamStr(0)));

{Position the display to the end so new line will be visible}
Viewer.VScrollbarPosition := Viewer.VScrollbarRange;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
{the timer routine adds a dummy chat line at time intervals set by the
 timer's Interval Property}
begin
Inc(Count);
Timer1.Enabled := False;
AddLine('<img src="note.gif"> Chat Line <font color="blue">'+IntToStr(Count)+'</font><br>');
{Enable the timer unless the stop button has been pressed}
Timer1.Enabled := not Stop;
end;

procedure TForm1.StartButtonClick(Sender: TObject);
begin
Timer1.Enabled := True;
Stop := False;
end;

procedure TForm1.StopButtonClick(Sender: TObject);
begin
Stop := True;
Timer1.Enabled := False;
end;

end.
