Abakus VCL Delphi 1&2 + Cbuilder 1 Freeware package
===================================================

Installation:
-------------
- read the file License.txt
- Create a folder "C:\Abakus\" and unpack all files from
  Abakus.zip to that folder.
- go ahead with the installation as discriped in 
  ..\Abakus\install.txt.


Contact address
---------------  
Hard- & Software  
A. Baecker		EMail: support@abaecker.de
Hauptstr. 137		Web:   http:\\www.abaecker.com (.de)
63773 Goldbach		Fax:   06021-52581
Germany


=======================================================
Copyright (C) 1998,1999,2000 Hard- & Software A.Baecker
