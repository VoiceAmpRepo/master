// Author : Jay Dubal
// E-mail : delphisoft@gmx.net
// URL : delphisoft.topcities.com
// copyleft � DelphiSoft

// This is a Free! component. You can modify it as you like
// and you don'nt need to inform me!. Use it at my risk as
// it is fully tested and works perfectly (Delphi 6.0)

//        _a__
//       |    |b
//     f |    |
//        -g--        // The basic 7 segment Light Emitting Diode (LED)
//     e |    | c
//       |    |
//        ----
//         d

unit JDLed;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Graphics;

type
  TJDLed = class(TgraphicControl)
  private
    procedure SetLedDigit(value : integer);
    procedure SetLedThick(value: integer);
    procedure SetLedColour(value: TColor);
    { Private declarations }
  protected
    FDigit: integer;
    FThick: integer;
    FColour: TColor;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    { Protected declarations }
  public
    { Public declarations }
  published
    property Digit : integer read FDigit write SetLedDigit;
    property Thick : integer read FThick write SetLedThick;
    property Colour : TColor read FColour write SetLedColour;
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DelphiSoft', [TJDLed]);
end;

{ TJDLed }


constructor TJDLed.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);  { always call the inherited constructor }
  FDigit := 8;
  FThick := 2;
  Height := 30;
  Width := 15;
  FColour := ClRed;
  with Constraints do
  begin
    MinHeight := 30;
    MinWidth := 15;
  end;
end;

destructor TJDLed.Destroy;
begin
  inherited Destroy;
end;


procedure TJDLed.Paint;
var
  X1,X2,X3,X4,X5 : integer;
begin
  X1 := FThick;
  X2 := (self.Width - FThick);
  X3 := (self.Height - FThick);
  X4 := (self.Height div 2) - (abs(FThick div 2));
  X5 := (self.Height div 2) + (abs(FThick div 2));

  inherited;
  with Canvas do
  begin
    Refresh;
    Brush.Color := clBtnFace;
    Pen.Color := ClBtnFace;

    Rectangle(0,0,self.Width,self.Height); //a
{    Rectangle(X2,X1,self.Width,X4); //b
    Rectangle(X2,X5,self.Width,X3); // c
    Rectangle(X1,X3,X2,self.Height); //d
    Rectangle(0,X5,X1,X3); // e
    Rectangle(0,X1,X1,X4); //f
    Rectangle(X1,X4,X2,X5); //g   }

    Brush.Color := FColour; // clRed;
    Pen.Color := FColour; //clRed;
    Refresh;
    
    case FDigit of

    0:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X2,X5,self.Width,X3); // c
      Rectangle(X1,X3,X2,self.Height); //d
      Rectangle(0,X5,X1,X3); // e
      Rectangle(0,X1,X1,X4); //f
    end;
    1:
    begin
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X2,X5,self.Width,X3); // c
    end;
    2:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X1,X3,X2,self.Height); //d
      Rectangle(0,X5,X1,X3); // e
      Rectangle(X1,X4,X2,X5); //g
    end;
    3:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X1,X4,X2,X5); //g
      Rectangle(X2,X5,self.Width,X3); // c
      Rectangle(X1,X3,X2,self.Height); //d
    end;

    4:
    begin
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X2,X5,self.Width,X3); // c
      Rectangle(0,X1,X1,X4); //f
      Rectangle(X1,X4,X2,X5); //g
    end;
    5:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X5,self.Width,X3); // c
      Rectangle(0,X1,X1,X4); //f
      Rectangle(X1,X4,X2,X5); //g
      Rectangle(X1,X3,X2,self.Height); //d
    end;
    6:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X5,self.Width,X3); // c
      Rectangle(0,X1,X1,X4); //f
      Rectangle(X1,X4,X2,X5); //g
      Rectangle(X1,X3,X2,self.Height); //d
      Rectangle(0,X5,X1,X3); // e
     end;
    7:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X2,X5,self.Width,X3); // c
    end;
    8:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X2,X5,self.Width,X3); // c
      Rectangle(X1,X3,X2,self.Height); //d
      Rectangle(0,X5,X1,X3); // e
      Rectangle(0,X1,X1,X4); //f
      Rectangle(X1,X4,X2,X5); //g
    end;
    9:
    begin
      Rectangle(X1,0,X2,X1); //a
      Rectangle(X2,X1,self.Width,X4); //b
      Rectangle(X2,X5,self.Width,X3); // c
      Rectangle(X1,X3,X2,self.Height); //d
      Rectangle(0,X1,X1,X4); //f
      Rectangle(X1,X4,X2,X5); //g
    end;
   end;
   Refresh;
  end;
end;

procedure TJDLed.SetLedColour(value: TColor);
begin
  FColour := value;
  Paint;
end;

procedure TJDLed.SetLedDigit(value: integer);
begin
  if (value >= 0) and (value <= 9) then
  begin
    FDigit := value;
    Paint;
  end;
end;

procedure TJDLed.SetLedThick(value: integer);
begin
  if (value > 1) then
  begin
    FThick := Value;
    Paint;
  end;
end;


end.
