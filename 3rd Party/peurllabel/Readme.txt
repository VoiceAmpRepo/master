  TURLLabel v1.0 by Philipp Engel
  Copyrights (C) 1999 by Noseghost Software


TURLLabel is a small component that lauches your default web browser
or mail client and go to the site specified in the URL property.
When the mouse is over the label, the font and color changes to 
the font and color specified in MouseOverColor and MouseOverFont.


Contacts: 

    e-mail:         Philipp_Engel@gmx.net

    internet:       http://noseghost.findhere.com


Philipp Engel
Buchenweg 10
85757 Karlsfeld
GERMANY

New Properties:
---------------

  -URL             specifies the destination address. Example:
		   'http://www.noseghost.findhere.com'         launches default web browser
		   'mailto:Philipp_Engel@gmx.net'		     launches default email-client
  
  -MouseOverFont   this defines how the font of the label changes when the
		   mouse points on it.


Regards, Philipp