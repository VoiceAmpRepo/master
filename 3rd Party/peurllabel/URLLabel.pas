unit URLLabel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ShellApi;

type
  TCustomFont = CLASS(TPersistent)
              private
               FCharSet : TFontCharSet;
               FColor : TColor;
               FHeight :  Integer;
               FName : TFontName;
               FPitch : TFontPitch;
               FSize : Integer;
               FFont  : TFontStyles;
              published
               property CharSet : TFontCharSet read FCharSet write FCharSet;
               property Color : TColor read FColor write FColor;
               property Height : Integer read FHeight write FHeight;
               property Name : TFontName read FName write FName;
               property Pitch : TFontPitch read FPitch write FPitch;
               property Size : Integer read FSize write FSize;
               property Style  : TFontStyles read FFont write FFont;
              END;
  TURLLabel = class(TLabel)
  private
    { Private-Deklarationen}
    {FMouseOverColor : TColor;
    FMouseOverFont : TFontStyles;     }
    FURL : String;
    FMouseOverFont : TCustomFont;
    OFFMouseOverFont : TCustomFont;
    {OFFMouseOverColor : TColor;
    OFFMouseOverFont  : TFontStyles;   }
  protected
    { Protected-Deklarationen}
    PROCEDURE MouseDown(Button : TMouseButton; Shift : TShiftState;
      X, Y : Integer); OVERRIDE;
    PROCEDURE CMMouseEnter(VAR msg:TMessage); message CM_MOUSEENTER;
    PROCEDURE CMMouseLeave(VAR msg: TMessage); message CM_MOUSELEAVE;
  public
    { Public-Deklarationen}
    CONSTRUCTOR Create(AOwner : TComponent); override;
  published
    { Published-Deklarationen }
    PROPERTY URL : String read FURL write FURL;
    PROPERTY MouseOverFont : TCustomFont read FMouseOverFont
              write FMouseOverFont;
   { PROPERTY MouseOverColor : TColor read FMouseOverColor
              write FMouseOverColor;               }
  end;

procedure Register;

implementation

CONSTRUCTOR TURLLabel.Create(AOwner : TComponent);
BEGIN
 INHERITED Create(AOwner);
 OFFMouseOverFont := TCustomFont.Create;
 FMouseOverFont := TCustomFont.Create;
 URL := 'http://noseghost.findhere.com';
 Font.Color := clBlack;
 Font.Style := [];
 FMouseOverFont.Color := clBlue;
 FMouseOverFont.Style := [fsUnderline];
 FMouseOverFont.Name := Font.Name;
 FMouseOverFont.CharSet := Font.CharSet;
 FMouseOverFont.Pitch := Font.Pitch;
 FMouseOverFont.Height := Font.Height;
 FMouseOverFont.Size := Font.Size;
 Caption := 'http://noseghost.findhere.com';
END;

PROCEDURE TURLLabel.CMMouseEnter(var msg : TMessage);
begin
     INHERITED;
     OFFMouseOverFont.CharSet := Font.CharSet;
     OFFMouseOverFont.Height := Font.Height;
     OFFMouseOverFont.Name := Font.Name;
     OFFMouseOverFont.Color := Font.Color;
     OFFMouseOverFont.Pitch := Font.Pitch;
     OFFMouseOverFont.Size := Font.Size;
     OFFMouseOverFont.Style := Font.Style;
     Font.CharSet := FMouseOverFont.CharSet;
     Font.Color := FMouseOverFont.Color;
     Font.Height := FMouseOverFont.Height;
     Font.Name := FMouseOverFont.Name;
     Font.Pitch := FMouseOverFont.Pitch;
     Font.Size := FMouseOverFont.Size;
     Font.Style := FMouseOverFont.Style;
     Refresh;
end;

PROCEDURE TURLLabel.CMMouseLeave(var msg: TMessage);
BEGIN
 INHERITED;
 Font.CharSet := OFFMouseOverFont.CharSet;
 Font.Color := OFFMouseOverFont.Color;
 Font.Height := OFFMouseOverFont.Height;
 Font.Name := OFFMouseOverFont.Name;
 Font.Pitch := OFFMouseOverFont.Pitch;
 Font.Size := OFFMouseOverFont.Size;
 Font.Style := OFFMouseOverFont.Style;
 ReFresh;
END;

PROCEDURE TURLLabel.MouseDown(Button : TMouseButton; Shift : TShiftState;
      X, Y : Integer);
BEGIN
 INHERITED MouseDown(Button, Shift, X, Y);
 ShellExecute(ValidParentForm(Self).Handle,'open',PChar(URL),
              NIL,NIL,SW_SHOWNORMAL);
END;

procedure Register;
begin
  RegisterComponents('Freeware', [TURLLabel]);
end;

end.
