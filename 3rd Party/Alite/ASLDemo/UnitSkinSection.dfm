inherited FormSkinSection: TFormSkinSection
  Left = 221
  Top = 208
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'SkinSection property using'
  ClientHeight = 104
  ClientWidth = 343
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object sComboBox1: TsComboBox [0]
    Left = 12
    Top = 72
    Width = 148
    Height = 21
    Alignment = taLeftJustify
    BoundLabel.Active = True
    BoundLabel.Caption = 'Button.SkinData.SkinSection :'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clBlack
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'COMBOBOX'
    Style = csDropDownList
    Anchors = [akLeft, akBottom]
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ItemIndex = -1
    ParentFont = False
    TabOrder = 2
    OnChange = sComboBox1Change
    Items.Strings = (
      'BUTTON'
      'BUTTON_BIG'
      'SPEEDBUTTON'
      'TOOLBUTTON'
      'WEBBUTTON'
      'SPEEDBUTTON_SMALL'
      'FORMTITLE'
      'NONE')
  end
  object sButton4: TsBitBtn [1]
    Left = 12
    Top = 11
    Width = 148
    Height = 38
    Anchors = [akLeft, akTop, akBottom]
    Caption = 'Button'
    TabOrder = 1
    Margin = 12
    NumGlyphs = 2
    Spacing = 3
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'BUTTON_BIG'
    FocusMargin = 3
    ImageIndex = 2
    Images = MainForm.ImageList1
    ShowFocus = False
  end
  object sComboBox2: TsComboBox [2]
    Left = 169
    Top = 72
    Width = 148
    Height = 21
    Alignment = taLeftJustify
    BoundLabel.Active = True
    BoundLabel.Caption = 'Button.SkinData.SkinSection :'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clBlack
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'COMBOBOX'
    Style = csDropDownList
    Anchors = [akLeft, akRight, akBottom]
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ItemIndex = -1
    ParentFont = False
    TabOrder = 3
    OnChange = sComboBox2Change
    Items.Strings = (
      'PANEL_LOW'
      'PANEL'
      'GROUPBOX'
      'DIALOG'
      'FORM'
      'BUTTON'
      'BUTTON_BIG'
      'SPEEDBUTTON'
      'TOOLBUTTON'
      'TOOLBAR'
      'FORMTITLE'
      'NONE')
  end
  object sPanel7: TsPanel [3]
    Left = 169
    Top = 11
    Width = 148
    Height = 38
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvLowered
    Caption = 'SkinSection property test'
    TabOrder = 0
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'GROUPBOX'
  end
end
