unit UnitSkinSection;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnitChildForm, sSkinProvider, StdCtrls, sComboBox, Buttons, sBitBtn,
  ExtCtrls, sPanel;

type
  TFormSkinSection = class(TChildForm)
    sPanel7: TsPanel;
    sButton4: TsBitBtn;
    sComboBox1: TsComboBox;
    sComboBox2: TsComboBox;
    procedure sComboBox2Change(Sender: TObject);
    procedure sComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSkinSection: TFormSkinSection;

implementation

uses MainUnit;

{$R *.DFM}

procedure TFormSkinSection.sComboBox2Change(Sender: TObject);
begin
  sPanel7.SkinData.SkinSection := sComboBox2.Text
end;

procedure TFormSkinSection.sComboBox1Change(Sender: TObject);
begin
  sButton4.SkinData.SkinSection := sComboBox1.Text
end;

end.
