unit UnitChildForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  sSkinProvider;

type
  TChildForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
  public
    procedure WMNCHitTest(var Message: TWMNCHitTest); message WM_NCHITTEST;
  end;

implementation

{$R *.DFM}

procedure TChildForm.WMNCHitTest(var Message: TWMNCHitTest);
begin
  Message.Result := HTCLIENT
end;

end.
