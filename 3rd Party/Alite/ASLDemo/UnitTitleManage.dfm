inherited FormTitle: TFormTitle
  Left = 646
  Top = 236
  Caption = 'Managing the forms title'
  ClientHeight = 93
  ClientWidth = 258
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object sComboBox3: TsComboBox [0]
    Left = 117
    Top = 64
    Width = 131
    Height = 21
    Alignment = taLeftJustify
    BoundLabel.Active = True
    BoundLabel.Caption = 'SkinSection :'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clBlack
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'COMBOBOX'
    Style = csDropDownList
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ItemIndex = -1
    ParentFont = False
    TabOrder = 2
    OnChange = sComboBox3Change
    Items.Strings = (
      'BUTTON'
      'BUTTON_BIG'
      'SPEEDBUTTON'
      'TOOLBUTTON'
      'WEBBUTTON'
      'SPEEDBUTTON_SMALL'
      'FORMTITLE'
      'NONE')
  end
  object ComboBox2: TsComboBox [1]
    Left = 117
    Top = 39
    Width = 131
    Height = 21
    Alignment = taLeftJustify
    BoundLabel.Active = True
    BoundLabel.Caption = 'Caption alignment :'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clBlack
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'COMBOBOX'
    Style = csDropDownList
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ItemIndex = -1
    ParentFont = False
    TabOrder = 1
    OnChange = ComboBox2Change
    Items.Strings = (
      'taLeftJustify'
      'taCenter'
      'taRightJustify')
  end
  object sCheckBox1: TsCheckBox [2]
    Left = 24
    Top = 12
    Width = 132
    Height = 20
    Cursor = crHandPoint
    Caption = 'Show application icon'
    TabOrder = 0
    OnClick = sCheckBox1Click
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
    MultiLine = False
  end
end
