unit UnitColorChanging;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnitChildForm, sSkinProvider, sScrollBar, StdCtrls, sLabel;

type
  TFormColorChanging = class(TChildForm)
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sScrollBar1: TsScrollBar;
    sScrollBar2: TsScrollBar;
    procedure sScrollBar1Change(Sender: TObject);
    procedure sScrollBar2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormColorChanging: TFormColorChanging;

implementation

uses MainUnit, sStyleSimply, sSKinManager, sSkinProps;

{$R *.DFM}

procedure TFormColorChanging.sScrollBar1Change(Sender: TObject);
begin
  if not aSkinChanging then MainForm.sSkinManager1.Saturation := sScrollBar1.Position;
end;

procedure TFormColorChanging.sScrollBar2Change(Sender: TObject);
begin
  if not aSkinChanging then MainForm.sSkinManager1.HueOffset := sScrollBar2.Position
end;

end.
