unit UnitAnimation;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnitChildForm, sSkinProvider, StdCtrls, sCheckBox;

type
  TFormAnimation = class(TChildForm)
    sCheckBox1: TsCheckBox;
    sCheckBox2: TsCheckBox;
    sCheckBox3: TsCheckBox;
    sCheckBox4: TsCheckBox;
    procedure sCheckBox1Click(Sender: TObject);
    procedure sCheckBox2Click(Sender: TObject);
    procedure sCheckBox3Click(Sender: TObject);
    procedure sCheckBox4Click(Sender: TObject);
  end;

var
  FormAnimation: TFormAnimation;

implementation

uses sConst;

{$R *.DFM}

procedure TFormAnimation.sCheckBox1Click(Sender: TObject);
begin
  if sCheckBox1.Checked
    then sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents + [aeMouseEnter]
    else sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents - [aeMouseEnter]
end;

procedure TFormAnimation.sCheckBox2Click(Sender: TObject);
begin
  if sCheckBox2.Checked
    then sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents + [aeMouseLeave]
    else sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents - [aeMouseLeave]
end;

procedure TFormAnimation.sCheckBox3Click(Sender: TObject);
begin
  if sCheckBox3.Checked
    then sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents + [aeMouseDown]
    else sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents - [aeMouseDown]
end;

procedure TFormAnimation.sCheckBox4Click(Sender: TObject);
begin
  if sCheckBox4.Checked
    then sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents + [aeMouseUp]
    else sConst.GlobalAnimateEvents := sConst.GlobalAnimateEvents - [aeMouseUp]
end;

end.
