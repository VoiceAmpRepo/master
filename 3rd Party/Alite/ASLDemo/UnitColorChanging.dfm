inherited FormColorChanging: TFormColorChanging
  Left = 14
  Top = 157
  Caption = 'FormColorChanging'
  ClientHeight = 55
  ClientWidth = 258
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object sLabel1: TsLabel [0]
    Left = 24
    Top = 11
    Width = 48
    Height = 13
    Caption = 'Saturation'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 7771037
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object sLabel2: TsLabel [1]
    Left = 20
    Top = 35
    Width = 52
    Height = 13
    Caption = 'HUE offset'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 7771037
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object sScrollBar1: TsScrollBar [2]
    Left = 83
    Top = 9
    Width = 166
    Height = 16
    TabOrder = 0
    OnChange = sScrollBar1Change
    LargeChange = 10
    Min = -100
    PageSize = 0
  end
  object sScrollBar2: TsScrollBar [3]
    Left = 83
    Top = 33
    Width = 166
    Height = 16
    TabOrder = 1
    OnChange = sScrollBar2Change
    LargeChange = 10
    Max = 360
    PageSize = 0
  end
end
