inherited FormAnimation: TFormAnimation
  Left = 221
  Top = 208
  Anchors = [akLeft, akTop, akBottom]
  Caption = 'Animation events'
  ClientHeight = 104
  ClientWidth = 132
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object sCheckBox1: TsCheckBox [0]
    Left = 18
    Top = 12
    Width = 85
    Height = 20
    Cursor = crHandPoint
    Anchors = [akLeft, akTop, akRight]
    Caption = 'MouseEnter'
    Checked = True
    State = cbChecked
    TabOrder = 0
    OnClick = sCheckBox1Click
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
    MultiLine = False
  end
  object sCheckBox2: TsCheckBox [1]
    Left = 18
    Top = 33
    Width = 90
    Height = 20
    Cursor = crHandPoint
    Anchors = [akLeft, akTop, akRight]
    Caption = 'MouseLeave'
    Checked = True
    State = cbChecked
    TabOrder = 1
    OnClick = sCheckBox2Click
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
    MultiLine = False
  end
  object sCheckBox3: TsCheckBox [2]
    Left = 18
    Top = 57
    Width = 88
    Height = 20
    Cursor = crHandPoint
    Anchors = [akLeft, akTop, akRight]
    Caption = 'MouseDown'
    Checked = True
    State = cbChecked
    TabOrder = 2
    OnClick = sCheckBox3Click
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
    MultiLine = False
  end
  object sCheckBox4: TsCheckBox [3]
    Left = 18
    Top = 78
    Width = 74
    Height = 20
    Cursor = crHandPoint
    Anchors = [akLeft, akTop, akRight]
    Caption = 'MouseUp'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = sCheckBox4Click
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
    MultiLine = False
  end
end
