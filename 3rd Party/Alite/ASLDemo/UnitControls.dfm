inherited FormControls: TFormControls
  Left = 221
  Top = 208
  Anchors = [akLeft, akTop, akRight]
  Caption = 'Some skinned controls'
  ClientHeight = 93
  ClientWidth = 258
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object sGauge1: TsGauge [0]
    Left = 88
    Top = 48
    Width = 165
    Height = 33
    Anchors = [akLeft, akTop, akRight]
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'GAUGE'
    ForeColor = clBlack
    Suffix = '%'
  end
  object sButton1: TsButton [1]
    Left = 9
    Top = 8
    Width = 71
    Height = 24
    Caption = 'sButton1'
    TabOrder = 0
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'BUTTON'
  end
  object sScrollBar3: TsScrollBar [2]
    Left = 88
    Top = 9
    Width = 165
    Height = 32
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    OnChange = sScrollBar3Change
    LargeChange = 30
    PageSize = 0
    Position = 47
  end
  object sButton2: TsButton [3]
    Left = 9
    Top = 33
    Width = 71
    Height = 24
    Caption = 'sButton2'
    TabOrder = 2
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'BUTTON'
  end
  object sButton3: TsButton [4]
    Left = 9
    Top = 58
    Width = 71
    Height = 24
    Caption = 'sButton3'
    TabOrder = 3
    SkinData.SkinManager = MainForm.sSkinManager1
    SkinData.SkinSection = 'BUTTON'
  end
  inherited sSkinProvider1: TsSkinProvider
    SkinData.SkinSection = 'FORM'
  end
end
