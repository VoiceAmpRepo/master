unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, sScrollBar, ExtDlgs, sEdit, Menus,
  sButton, StdCtrls, sSkinProvider, sSkinManager, sCheckBox, Buttons,
  sBitBtn, sComboBox, sLabel, ImgList, sAlphaListBox, sGauge, sPanel;

type
  TMainForm = class(TForm)
    sSkinManager1: TsSkinManager;
    sPanel4: TsPanel;
    ComboBox1: TsComboBox;
    OpenPictureDialog1: TOpenPictureDialog;
    MainMenu1: TMainMenu;
    MenuItem11: TMenuItem;
    MenuItem111: TMenuItem;
    MenuItem121: TMenuItem;
    MenuItem131: TMenuItem;
    MenuItem141: TMenuItem;
    MenuItem151: TMenuItem;
    MenuItem161: TMenuItem;
    MenuItem1511: TMenuItem;
    MenuItem1521: TMenuItem;
    MenuItem1531: TMenuItem;
    MenuItem1541: TMenuItem;
    MenuItem1551: TMenuItem;
    sSkinProvider1: TsSkinProvider;
    sCheckBox1: TsCheckBox;
    sButton9: TsBitBtn;
    About1: TMenuItem;
    Gotoonlinehome1: TMenuItem;
    Writetosupport1: TMenuItem;
    ImageList1: TImageList;
    sLabel1: TsLabel;
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure sButton9Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenPictureDialog1SelectionChange(Sender: TObject);
    procedure sSkinManager1AfterChange(Sender: TObject);
    procedure sCheckBox1Click(Sender: TObject);
    procedure sSkinManager1BeforeChange(Sender: TObject);
  end;

var
  MainForm: TMainForm;
  Loading : boolean;
  NewBGName : string;

implementation

uses
  sSkinProps, FileCtrl, sStyleSimply, sMaskData, UnitContacts,
  UnitColorChanging, UnitTitleManage, UnitAnimation, UnitControls,
  UnitSkinSection;

{$R *.DFM}

procedure TMainForm.FormCreate(Sender: TObject);
const
  Spacing = 24;
begin
  // First column
  FormColorChanging := TFormColorChanging.Create(Application);
  FormColorChanging.Left := Spacing;
  FormColorChanging.Top := sPanel4.Top + sPanel4.Height + Spacing;

  FormTitle := TFormTitle.Create(Application);
  FormTitle.Left := FormColorChanging.Left;
  FormTitle.Top := FormColorChanging.Top + FormColorChanging.Height + Spacing;

  FormAnimation := TFormAnimation.Create(Application);
  FormAnimation.Left := FormColorChanging.Left;
  FormAnimation.Top := FormTitle.Top + FormTitle.Height + Spacing;

  // Second column
  FormContacts := TFormContacts.Create(Application);
  FormContacts.Left := FormColorChanging.Left + FormColorChanging.Width + Spacing;
  FormContacts.Top := sPanel4.Top + sPanel4.Height + Spacing;

  FormControls := TFormControls.Create(Application);
  FormControls.Left := FormColorChanging.Left + FormColorChanging.Width + Spacing;
  FormControls.Top := FormColorChanging.Top + FormColorChanging.Height + Spacing;

  FormSkinSection := TFormSkinSection.Create(Application);
  FormSkinSection.Left := FormAnimation.Left + FormAnimation.Width + Spacing;
  FormSkinSection.Top := FormTitle.Top + FormTitle.Height + Spacing;
  FormSkinSection.Width := FormContacts.Left + FormContacts.Width - FormSkinSection.Left;

  // Form size
  ClientHeight := FormTitle.Top + FormTitle.Height + Spacing + FormAnimation.Height + Spacing;
  ClientWidth := FormColorChanging.Left + FormColorChanging.Width + Spacing + 270 + Spacing;
  Constraints.MinHeight := Height;
  Constraints.MinWidth := Width;

  // Defining Parent property of forms
  FormColorChanging.Parent := MainForm;
  FormTitle.Parent := MainForm;
  FormAnimation.Parent := MainForm;
  FormContacts.Parent := MainForm;
  FormControls.Parent := MainForm;
  FormSkinSection.Parent := MainForm;

  // Defining Visible property of forms
  FormColorChanging.Visible := True;
  FormTitle.Visible := True;
  FormAnimation.Visible := True;
  FormContacts.Visible := True;
  FormControls.Visible := True;
  FormSkinSection.Visible := True;
end;

procedure TMainForm.ComboBox1Change(Sender: TObject);
var
  sl : TStringList;
  s : string;
  i : integer;
begin
  if Loading then Exit;
  if ComboBox1.ItemIndex = 0 then begin
    if SelectDirectory(s, [], 0) then begin
      sSkinManager1.SkinDirectory := s;
      sl := TStringList.Create;
      sSkinManager1.SkinName := sSkinManager1.GetSkinNames(sl);
      ComboBox1.Items.Clear;
      ComboBox1.Items.Add('Skins directory...');
      for i := 0 to sl.Count - 1 do begin
        ComboBox1.Items.Add(sl[i]);
      end;
      FreeAndNil(sl);
    end;
  end
  else begin
    sSkinManager1.SkinName := ComboBox1.Text;
  end;
end;

procedure TMainForm.sButton9Click(Sender: TObject);
begin
  if OpenPictureDialog1.Execute then begin
    NewBGName := OpenPictureDialog1.FileName;
    // SkinSections and PropNames are defined in sSkinProps.pas unit
    ChangeImageInSkin(NormalForm, PatternFile, OpenPictureDialog1.FileName, sSkinManager1);
    ChangeImageInSkin(NormalForm, HotPatternFile, OpenPictureDialog1.FileName, sSkinManager1);
    // Update of all controls
    sSkinManager1.UpdateSkin;
  end;
end;

procedure TMainForm.FormShow(Sender: TObject);
var
  sl : TStringList;
  i : integer;
begin
  sl := TStringList.Create;
//  sSkinManager1.SkinName :=
  sSkinManager1.GetSkinNames(sl);
  ComboBox1.Clear;
  ComboBox1.Items.Add('Skins directory...');
  for i := 0 to sl.Count - 1 do begin
    ComboBox1.Items.Add(sl[i]);
  end;
  // If no available skins...
  if ComboBox1.Items.Count < 1 then begin
    ComboBox1.Items.Add('No skins available');
    ComboBox1.ItemIndex := 0;
  end
  else begin
    // Sets ComboBox to current skin name value without skin changing
    Loading := True;
    ComboBox1.ItemIndex := sl.IndexOf(sSkinManager1.SkinName) + 1;
    Loading := False;
  end;
  FreeAndNil(sl);
  FormTitle.sComboBox3.ItemIndex := 1;
end;

procedure TMainForm.OpenPictureDialog1SelectionChange(Sender: TObject);
begin
  if (pos('.BMP', UpperCase(OpenPictureDialog1.FileName)) > 0) or
       (pos('.JPG', UpperCase(OpenPictureDialog1.FileName)) > 0) or
         (pos('.BMP', UpperCase(OpenPictureDialog1.FileName)) > 0) then begin
    // SkinSections and PropNames are defined in sSkinProps.pas unit
    NewBGName := OpenPictureDialog1.FileName;
    ChangeImageInSkin(s_Form, s_Pattern, OpenPictureDialog1.FileName, sSkinManager1);
    ChangeImageInSkin(s_Form, s_HotPattern, OpenPictureDialog1.FileName, sSkinManager1);
    // Update of all controls
    sSkinManager1.UpdateSkin;
  end;
end;

procedure TMainForm.sSkinManager1AfterChange(Sender: TObject);
var
  i : integer;
begin
  i := sSkinManager1.GetSkinIndex(NormalForm);
  if sSkinManager1.IsValidSkinIndex(i) then sButton9.Enabled := sSkinManager1.gd[i].ImagePercent > 0;
end;

procedure TMainForm.sCheckBox1Click(Sender: TObject);
begin
  sSkinManager1.Active := sCheckBox1.Checked;
  ComboBox1.Enabled := sSkinManager1.Active;

  FormTitle.sCheckBox1.Enabled := sSkinManager1.Active;
  Formtitle.ComboBox2.Enabled := sSkinManager1.Active;
  Formtitle.sComboBox3.Enabled := sSkinManager1.Active;
  FormSkinSection.sComboBox1.Enabled := sSkinManager1.Active;
  FormSkinSection.sComboBox2.Enabled := sSkinManager1.Active;
end;

procedure TMainForm.sSkinManager1BeforeChange(Sender: TObject);
begin
  sSkinManager1.FHueOffset := 0;
  sSkinManager1.FSaturation := 0;
  FormColorChanging.sScrollBar1.Position := 0;
  FormColorChanging.sScrollBar2.Position := 0;
end;

end.
