unit UnitControls;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnitChildForm, sSkinProvider, sScrollBar, StdCtrls, sEdit, sButton,
  sGauge;

type
  TFormControls = class(TChildForm)
    sGauge1: TsGauge;
    sButton1: TsButton;
    sScrollBar3: TsScrollBar;
    sButton2: TsButton;
    sButton3: TsButton;
    procedure sScrollBar3Change(Sender: TObject);
  end;

var
  FormControls: TFormControls;

implementation

{$R *.DFM}

procedure TFormControls.sScrollBar3Change(Sender: TObject);
begin
  sGauge1.Progress := sScrollBar3.Position
end;

end.
