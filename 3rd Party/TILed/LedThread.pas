unit LedThread;

interface

uses
  Windows,Classes, Graphics, Controls;

type
  TLedThread = class(TThread)
  private
    { Private declarations }
    ITargetControl : TWinControl;
    R,G,B : byte;
    Procedure SetColor;
  protected
    procedure Execute; override;
  public
    constructor Create(TargetControl : TWinControl);
  end;

implementation
uses TILed;

constructor TLedThread.Create(TargetControl : TWinControl);
begin
     ITargetControl := TargetControl;
     Inherited Create (true);
end;


procedure TLedThread.Execute;
var i,j:byte;
begin
     //set start color depended LedOn status
     if TTILed(ITargetControl).LedOn then
        //enabled color
        begin
             if TTILed(ITargetControl).LedColor = Green then
                TTILed(ITargetControl).Color := RGB(0,128,0);
             if TTILed(ITargetControl).LedColor = Yellow then
                TTILed(ITargetControl).Color := RGB(128,128,0);
             if TTILed(ITargetControl).LedColor = Red then
             TTILed(ITargetControl).Color := RGB(128,0,0);
        end
     else
        //disabled color
         begin
             if TTILed(ITargetControl).LedColor = Green then
                TTILed(ITargetControl).Color := RGB(0,255,0);
             if TTILed(ITargetControl).LedColor = Yellow then
                TTILed(ITargetControl).Color := RGB(255,255,0);
             if TTILed(ITargetControl).LedColor = Red then
             TTILed(ITargetControl).Color := RGB(255,0,0);
         end;
     //wait user defined time (ms) before changing colors
     Sleep(TTILed(ITargetControl).LedDelayBeforeFade);
     //get base color (current control's color before fade)
     {
     R := GetRValue(TTILed(ITargetControl).Color);
     G := GetGValue(TTILed(ITargetControl).Color);
     B := GetBValue(TTILed(ITargetControl).Color);
     }
     if TTILed(ITargetControl).LedSmoothFlash then
        //fade from start color to end color
        begin
             //fading from upper color to lower color
             if not TTILed(ITargetControl).LedOn then
                begin
                     j := 255;
                     while (j > 128) do
                       begin
                            j := j - TTILed(ITargetControl).LedFadeSpeed;
                            if j < 128 then
                               break;
                            if TTILed(ITargetControl).LedColor = Green then
                               begin
                                    R := 0;
                                    G := j;
                                    B := 0;
                               end;
                            if TTILed(ITargetControl).LedColor = Yellow then
                               begin
                                    R := j;
                                    G := j;
                                    B := 0;
                               end;
                            if TTILed(ITargetControl).LedColor = Red then
                               begin
                                    R := j;
                                    G := 0;
                                    B := 0;
                               end;
                            Synchronize(SetColor);
                            sleep(50);
                       end;
                end
             else
                //fading from lower color to upper color
                begin
                     j := 128;
                     while (j < 255) and (j>=128) do
                       begin
                            j := j + TTILed(ITargetControl).LedFadeSpeed;
                            if j < 128 then
                               break;
                            if TTILed(ITargetControl).LedColor = Green then
                               begin
                                    R := 0;
                                    G := j;
                                    B := 0;
                               end;
                            if TTILed(ITargetControl).LedColor = Yellow then
                               begin
                                    R := j;
                                    G := j;
                                    B := 0;
                               end;
                            if TTILed(ITargetControl).LedColor = Red then
                               begin
                                    R := j;
                                    G := 0;
                                    B := 0;
                               end;
                            Synchronize(SetColor);
                            sleep(50);
                       end;
                end;
        end
     else
         //change straight from start color to end color (no fading)
         begin
              i := 128;
              while (i < 255) and (i >= 128) do
              begin
                   i := i + TTILed(ITargetControl).LedFadeSpeed;
                   sleep(50);
              end;
         end;

         //SET END COLOR
         if not TTILed(ITargetControl).LedOn then
            begin
                 if TTILed(ITargetControl).LedColor = Green then
                    begin
                         R := 0;
                         G := 128;
                         B := 0;
                    end;
                 if TTILed(ITargetControl).LedColor = Yellow then
                    begin
                         R := 128;
                         G := 128;
                         B := 0;
                    end;
                 if TTILed(ITargetControl).LedColor = Red then
                    begin
                         R := 128;
                         G := 0;
                         B := 0;
                    end;
            end
         else
             begin
                 if TTILed(ITargetControl).LedColor = Green then
                    begin
                         R := 0;
                         G := 255;
                         B := 0;
                    end;
                 if TTILed(ITargetControl).LedColor = Yellow then
                    begin
                         R := 255;
                         G := 255;
                         B := 0;
                    end;
                 if TTILed(ITargetControl).LedColor = Red then
                    begin
                         R := 255;
                         G := 0;
                         B := 0;
                    end;
             end;
     Synchronize(SetColor);
     Self.Free;
end;

Procedure TLedThread.SetColor;
begin
     //update component's color
     TTILed(ITargetControl).Color := RGB(R,G,B);
end;

end.
