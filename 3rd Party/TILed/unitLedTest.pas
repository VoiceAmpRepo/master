unit unitLedTest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, TILed, ComCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    TILed1: TTILed;
    TILed2: TTILed;
    TILed3: TTILed;
    CheckBox1: TCheckBox;
    GroupBox1: TGroupBox;
    TrackBar1: TTrackBar;
    GroupBox2: TGroupBox;
    TrackBar2: TTrackBar;
    CheckBox2: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TILed1Click(Sender: TObject);
    procedure TILed2Click(Sender: TObject);
    procedure TILed3Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
  private
    { Private declarations }
     procedure GenClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var CrStr,Str : string;
    i : integer;
    Led : TTiLed;
begin
     for i := 1 to 21 do
         begin
              Led := TTILed(FindComponent('Led'+ IntToStr(i-1)));
              if Led = nil then continue;
              Led.Flash;
              application.ProcessMessages;
              Sleep(100);
         end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var i:integer;
    Led :TTIled;
begin
     for i := 1 to 20 do
         begin
             Led := TTILed.Create(TComponent(Sender));
             Led.OnClick := GenClick;
             Led.Parent := TWinControl(Sender);
             Led.Name := 'Led' + IntToStr(i);
             Led.LedColor := Red;
             //Led.LedSmoothFlash := false;
             //Led.Color := clNavy;
             Led.LedDelayBeforeFade := 100;
             Led.Top := 15;
             if i > 1 then
                Led.Left := TTILed(FindComponent('Led'+ IntToStr(i-1))).Left +
                            TTILed(FindComponent('Led' + IntToStr(i-1))).Width + 1
             else
                 Led.Left := 0;
         end;
end;

procedure TForm1.GenClick(Sender: TObject);
begin
     TTILed(Sender).Flash;
end;

procedure TForm1.TILed1Click(Sender: TObject);
begin
     TILed1.Flash;
end;

procedure TForm1.TILed2Click(Sender: TObject);
begin
     TILed2.Flash;
end;

procedure TForm1.TILed3Click(Sender: TObject);
begin
     TILed3.Flash;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
     GroupBox1.Caption := 'Led delay before fade : ' + IntTostr(TrackBar1.Position) + ' ms';
     TILed1.LedDelayBeforeFade := TrackBar1.Position;
     TILed2.LedDelayBeforeFade := TrackBar1.Position;
     TILed3.LedDelayBeforeFade := TrackBar1.Position;
end;

procedure TForm1.TrackBar2Change(Sender: TObject);
begin
     GroupBox2.Caption := 'Led fade speed: ' + IntToStr(TrackBar2.Position) + ' ms';
     TILed1.LedFadeSpeed := TrackBar2.Position;
     TILed2.LedFadeSpeed := TrackBar2.Position;
     TILed3.LedFadeSpeed := TrackBar2.Position;
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
     TILed1.LedOn := TCheckBox(Sender).Checked;
     TILed2.LedOn := TCheckBox(Sender).Checked;
     TILed3.LedOn := TCheckBox(Sender).Checked;
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
     TILed1.LedSmoothFlash := TCheckBox(Sender).Checked;
     TILed2.LedSmoothFlash := TCheckBox(Sender).Checked;
     TILed3.LedSmoothFlash := TCheckBox(Sender).Checked;
end;

end.
