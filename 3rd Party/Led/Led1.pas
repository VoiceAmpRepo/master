//Led v1.0
//Made by Deaky Bogdan
//Email: stormofwar@yahoo.com
//http://www.-storm-.home.ro

unit Led1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type

  tLedStyle = (lsGradient1,lsGradient2,lsAlternative);

  TLed = class(TGraphicControl)
  private
  FStyle:tledstyle;
  FColor1:Tcolor;
  FColor2:Tcolor;
  FOffColor1:Tcolor;
  FOffColor2:Tcolor;
  FRadius:byte;
  FOn:boolean;
  FDiagShining:boolean;
  FClickAction:boolean;
  FDblClickAction:boolean;
 // FFlicker:boolean;
 // timer:Ttimer;
  procedure SetStyle(Style:tLedStyle);
  procedure SetColor1(val:tcolor);
  procedure SetColor2(val:tcolor);
  procedure SetOffColor1(col:tcolor);
  procedure SetOffColor2(val:tcolor);
  procedure SetRadius(val:Byte);
  function GetRadius:Byte;
  procedure SetOn(val:boolean);
  function GetOn:boolean;
  procedure SetDiagShining(val:boolean);
  function GetDiagShining:boolean;
  procedure SetClickAction(val:boolean);
  function GetClickAction:boolean;
  procedure SetDblClickAction(val:boolean);
  function GetDblClickAction:boolean;
//  procedure SetFlicker(val:boolean);
//  function GetFlicker:boolean;
    { Private declarations }
  protected
  procedure paint; override;
  Procedure Click; override;
  Procedure DblClick; override;
    { Protected declarations }
  public
      procedure DrawLed;
      procedure DrawLed2;
    { Public declarations }
  published
    { Published declarations }
    //inherited
    property OnClick;
    property Canvas;
    property OnDblClick;
    Property OnMouseUp;
    Property OnMouseDown;
    Property OnMouseMove;
    property PopupMenu;
    constructor Create(AOwner: TComponent); override;

    property Style:tLedStyle read FStyle
    Write SetStyle;
    property Color1 : tColor read FColor1
    Write SetColor1;
    property Color2 : tColor read FColor2
    Write SetColor2;
    property OffColor1 : tColor read FOffColor1
    Write SetOffColor1;
    property OffColor2 : tColor read FOffColor2
    Write SetOffColor2;
    property Radius : byte read getradius
    Write setradius;
    property On : boolean read getOn
    Write setOn;
    property DiagShining : boolean read getDiagShining
    Write setDiagShining;
    property ClickAction : boolean read getClickAction
    Write setClickAction;
    property DblClickAction : boolean read getDblClickAction
    Write setDblClickAction;
//    property Flicker : boolean read getFlicker
//    Write setFlicker;
  end;

procedure Register;

implementation

procedure TLed.drawled;
var i:word;
    cl1,cl2:Tcolor;
begin
if FOn then
begin
cl1:=FColor1;
cl2:=FColor2;
end else
begin
cl1:=FOffColor1;
cl2:=FOffColor2;
end;
with canvas do
begin
for i:=0 to (width-1) div 4 do
begin
pen.color:=cl1;
if not FDiagShining then
begin
brush.color:=cl2;
end;
roundrect(i*2,i*2,width-(i*2+1),height-(i*2+1),Fradius,Fradius);
pen.color:=cl2;
if not FDiagShining then
begin
brush.color:=cl1;
end;
roundrect(i*2+1,i*2+1,width-(i*2+2),height-(i*2+2),Fradius,Fradius);
end;
end;
end;

procedure TLed.drawled2;
type
 tRGB = Record
         R,G,B : byte;
        End;

Function ColorToRGB(Color:TColor):TRGB;
Begin
 Result.R:=GetRValue(Color);
 Result.G:=GetGValue(Color);
 Result.B:=GetBValue(Color);
End;

var i,m:word;
    rgb1,rgb2:Trgb;
    saltr,saltg,saltb:byte;
    cl1,cl2:Tcolor;
begin
saltr:=0;
saltg:=0;
saltb:=0;

if FOn then
begin
cl1:=FColor1;
cl2:=FColor2;
end else
begin
cl1:=FOffColor1;
cl2:=FOffColor2;
end;

if FStyle=lsGradient1 then
begin
if height>=width then
m:=height-1  else
m:=width-1;
end else
if FStyle=lsGradient2 then
begin
if height>=width then
m:=width-1  else
m:=height-1;
end;

with canvas do
begin
//brush.style:=bssolid;
for i:=0 to (m) div 2 do
begin
RGB1:=colortorgb(cl1);
RGB2:=colortorgb(cl2);
if rgb1.r > rgb2.r then
begin
saltr:=-(rgb1.r-rgb2.r) div ((m) div 2);
end
else
if rgb1.r < rgb2.r then
saltr:=(rgb2.r-rgb1.r) div ((m) div 2);

if rgb1.g > rgb2.g then
saltg:=-(rgb1.g-rgb2.g) div ((m) div 2)
else
if rgb1.g < rgb2.g then
saltg:=(rgb2.g-rgb1.g) div ((m) div 2);

if rgb1.b > rgb2.b then
saltb:=-(rgb1.b-rgb2.b) div ((m) div 2)
else
if rgb1.b < rgb2.b then
saltb:=(rgb2.b-rgb1.b) div ((m) div 2);

pen.color:=rgb(rgb1.r+i*saltr,rgb1.g+i*saltg,rgb1.g+i*saltb);
if not FDiagShining then
begin
brush.color:=pen.color;
end;
roundrect(i,i,width-(i+1),height-(i+1),Fradius,Fradius);
lineto(width,height);
end;
end;
end;


//overriden procedures
procedure TLed.paint;
begin
if Fstyle=lsAlternative then
drawled else
if (Fstyle=lsGradient1) or (Fstyle=lsGradient2) then
drawled2;
end;

procedure TLed.Click;
begin
if FClickAction then
begin
if Fon then
begin
Fon:=false;
paint;
end
else
begin
Fon:=true;
paint;
end;
end;
end;

procedure TLed.DblClick;
begin
if FDblClickAction then
begin
if Fon then
begin
Fon:=false;
paint;
end
else
begin
Fon:=true;
paint;
end;
end;
end;


//
procedure TLed.SetStyle(Style:tledstyle);
begin
if (Style<>FStyle) then
begin
FStyle:=Style;
paint;
end;
end;

//
procedure TLed.SetColor1(val:tcolor);
begin
if (val<>Fcolor1) then
begin
Fcolor1:=val;
paint;
end;
end;

//
procedure TLed.SetColor2(val:tcolor);
begin
if (val<>Fcolor2) then
begin
Fcolor2:=val;
paint;
end;
end;

//
procedure TLed.SetOffColor1(col:tcolor);
begin
if (col<>FOffcolor1) then
begin
FOffcolor1:=col;
paint;
end;
end;

//
procedure TLed.SetOffColor2(val:tcolor);
begin
if (val<>FOffcolor2) then
begin
FOffcolor2:=val;
paint;
end;
end;

//
procedure TLed.SetRadius(val:byte);
begin
if (val<>Fradius) then
begin
Fradius:=val;
paint;
end;
end;

//
function TLed.GetRadius:byte;
begin
GetRadius:=FRadius;
end;

//
procedure TLed.SetOn(val:boolean);
begin
if (val<>FOn) then
begin
FOn:=val;
paint;
end;
end;

//
function TLed.GetOn:boolean;
begin
GetOn:=FOn;
end;

//
procedure TLed.SetDiagShining(val:boolean);
begin
if (val<>FDiagShining) then
begin
FDiagShining:=val;
paint;
end;
end;

//
function TLed.GetDiagShining:boolean;
begin
GetDiagShining:=FDiagShining;
end;

//
procedure TLed.SetClickAction(val:boolean);
begin
if (val<>FClickAction) then
begin
FClickAction:=val;
end;
end;

//
function TLed.GetClickAction:boolean;
begin
GetClickAction:=FClickAction;
end;

//
procedure TLed.SetDblClickAction(val:boolean);
begin
if (val<>FDblClickAction) then
begin
FDblClickAction:=val;
end;
end;

//
function TLed.GetDblClickAction:boolean;
begin
GetDblClickAction:=FDblClickAction;
end;

//
{procedure TLed.SetFlicker(val:boolean);
begin
if (val<>FFlicker) then
begin
FFlicker:=val;
if FFlicker then
begin
timer:=Ttimer.create(self);
timer.interval:=500
end else
begin
timer.free;
end;
end;
end; }

{//
function TLed.GetFlicker:boolean;
begin
GetFlicker:=FFlicker;
end; }

//
constructor TLed.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Width:=10;
  Height:=33;
  Fcolor1:=clblack;
  Fcolor2:=clyellow;
  Foffcolor1:=clblack;
  Foffcolor2:=clWhite;
  FStyle:=lsGradient2;
  Fradius:=1;
  FDiagShining:=false;
  ClickAction:=false;
  DblClickAction:=false;
  FOn:=false;
end;

//
procedure Register;
begin
  RegisterComponents('Storm''s', [TLed]);
end;

end.
 