unit JSILamp;

////////////////////////////////////////////////////////
// TJSILamp - Panel Lamp v1.1                         //
//                                                    //
//   Copyright (C) 2005 Stefan Ion                    //
//   J.S.Industries S.R.L.                            //
//   Software Development, Embedded Controllers,      //
//   Automation, Custom Design and Prototyping        //
//   http://www.jsindustries.ro                       //
//   office@jsindustries.ro                           //
//   ROMANIA                                          //
//                                                    //
//   License:                                         //
//   This source code is distributed as a freeware.   //
//   You may use and modify this source code for your //
//   freeware products.                               //
//   All functions, procedures and classes may        //
//   NOT BE USED IN COMMERCIAL PRODUCTS               //
//   without  the permission of the author.           //
//                                                    //
//   Disclaimer of warranty:                          //
//   "This software is supplied as is. The author     //
//   disclaims all warranties, expressed or implied,  //
//   including, without limitation, the warranties    //
//   of merchantability and of fitness for any        //
//   purpose. The author assumes no liability for     //
//   damages, direct or consequential, which may      //
//   result from the use of this software."           //
//                                                    //
//   Please report bugs to:                           //
//   Stefan Ion                                       //
//   office@jsindustries.ro                           //
//                                                    //
//   Please contact use for your suggestions or       //
//   projects proposal                                //
////////////////////////////////////////////////////////

interface

uses
   Windows, Classes, Graphics, Controls, Buttons, SysUtils, Extctrls, Math;

type
  TJSILamp = class(TCustomControl)
  private
    FTrueColor       : TColor;
    FFalseColor      : TColor;
    FState           : Boolean;
    FCaption         : TCaption;
    Lamp             : TRect;
  public
    constructor Create(AOwner:TComponent); override;

  protected
     procedure SetTrueColor(Value:TColor);
     procedure SetFalseColor(Value:TColor);
     procedure SetState(Value:Boolean);
     procedure SetCaption(Value: TCaption);
     procedure Paint; override;
     procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
     procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
     procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
     procedure Redraw(AOwner:TObject);
     Property  Canvas;

  published
    Property State:Boolean read fState write SetState default false;
    property TrueColor:TColor read fTrueColor write SetTrueColor;
    property FalseColor:TColor read fFalseColor write SetFalseColor;
    property Caption: TCaption read FCaption write SetCaption;
    property Color;
    Property Enabled;
    property Font;
    property Height;
    property Width;

    property onMouseMove;
    property onMouseDown;
    Property onMouseUp;
  end;

procedure Register;

implementation

procedure Register;
begin
   RegisterComponents('JSIComponents', [TJSILamp]);
end;

{##############################################################################}

constructor TJSILamp.Create(aOwner: TComponent);
begin
   inherited Create(aOwner);
   State:=False;
   TrueColor:=clRed;
   FalseColor:=clGray;
   Caption:=Name;
   Height := 20;
   Width := 80;
   Repaint;
end;

procedure TJSILamp.SetTrueColor(Value:TColor);
begin
   if fTrueColor <> Value then
      begin
         fTrueColor:=Value;
         Repaint;
      end;
end;

procedure TJSILamp.SetFalseColor(Value:TColor);
begin
   if fFalseColor <> Value then
      begin
         fFalseColor:=Value;
         Repaint;
      end;
end;
procedure TJSILamp.Redraw(AOwner:Tobject);
begin
  Paint;
end;

procedure TJSILamp.SetState(Value:Boolean);
begin
   if fState <> Value then
      begin
         fState:=Value;
         Repaint;
      end;
end;

procedure TJSILamp.SetCaption(Value: TCaption);
begin
  if Value='' then FCaption:=Name;
  if AnsiCompareStr(Value, FCaption) <> 0 then FCaption := Value;
  Repaint;
end;

procedure TJSILamp.Paint;
var
  ARect, AreaText:TRect;
  LText,RText,TText,BText,WText,HText: integer;
begin
     inherited Paint;
     Canvas.Brush.color:=Color;
     with Canvas do begin
//draw
         Lamp:=Rect(0,0,Width,Height);
         with Pen do begin
            Style:= psSolid;
            Mode:=pmCopy;
         end;

         Frame3D(Canvas, Lamp, clBtnHighlight, clBtnShadow,1);
         ARect:=Rect(3,3,width-3,height-3);
         Frame3D(Canvas, ARect, cl3DDkShadow, cl3DLight,1);

         Pen.Color:= Self.Color;
         ARect := Rect(4, 4, width-4, Height-4);
         Pen.Style:= psSolid;
         Pen.Mode:=pmCopy;
//first color
         Font.Assign(Self.Font);
         if State then begin
             Brush.Color:=fTrueColor;
             Font.Color:=cl3DLight;
         end else begin
             Brush.Color:=fFalseColor;
             Font.Color:=cl3DDkShadow;
         end;
         FillRect(ARect);

//text
         WText:= TextWidth(Caption) div 2;
         HText:= TextHeight(Caption) div 2;
         LText:=(Width div 2)-WText;
         TText:=(Height div 2)-HText;
         RText:=LText+2*WText;
         BText:=TText+2*HText;
         AreaText := Rect(LText-1, TText, RText+1, BText);
         TextRect(AreaText, LText, TText, Caption);
     end;

end;

procedure TJSILamp.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
 inherited MouseDown(Button, Shift, X, Y);
end;

procedure TJSILamp.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);
end;

procedure TJSILamp.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
end;

end.
