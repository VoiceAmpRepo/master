unit JSILedButton;

////////////////////////////////////////////////////////
// TJSILedButton - Panel Button with led v2.0         //
//                                                    //
//   Copyright (C) 2005 Stefan Ion                    //
//   J.S.Industries S.R.L.                            //
//   Software Development, Embedded Controllers,      //
//   Automation, Custom Design and Prototyping        //
//   http://www.jsindustries.ro                       //
//   office@jsindustries.ro                           //
//   ROMANIA                                          //
//                                                    //
//   License:                                         //
//   This source code is distributed as a freeware.   //
//   You may use and modify this source code for your //
//   freeware products.                               //
//   All functions, procedures and classes may        //
//   NOT BE USED IN COMMERCIAL PRODUCTS               //
//   without  the permission of the author.           //
//                                                    //
//   Disclaimer of warranty:                          //
//   "This software is supplied as is. The author     //
//   disclaims all warranties, expressed or implied,  //
//   including, without limitation, the warranties    //
//   of merchantability and of fitness for any        //
//   purpose. The author assumes no liability for     //
//   damages, direct or consequential, which may      //
//   result from the use of this software."           //
//                                                    //
//   Please report bugs to:                           //
//   Stefan Ion                                       //
//   office@jsindustries.ro                           //
//                                                    //
//   Please contact use for your suggestions or       //
//   projects proposal                                //
////////////////////////////////////////////////////////

interface

uses
   Windows, Classes, Graphics, Controls, Buttons, SysUtils;

type
  TJSILedButtonThread= class(TThread)

   public
      iSpeed: integer;
      FNotify: TNotifyEvent;
      constructor Create(DoNotify: TNotifyEvent);
      procedure Execute; override;
      procedure DoEvent;
   end;

  TJSILedButton = class(TCustomControl)
  private
    FCaption         : TCaption;
    FSpeed           : integer;
    FThread          : TJSILedButtonThread;
    FTrueColor       : TColor;
    FFalseColor      : TColor;
    FState           : Boolean;
    FFlash           : Boolean;
    FButtonState     : TButtonState;
    FDown            : Boolean;
    FLedRadius       : Word;
    FFromBorder      : Word;
    Led              : TRect;
    bFlag            : boolean;

  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Click; override;
//    procedure DblClick; override;

  protected
    procedure SetTrueColor(Value:TColor);
    procedure SetFalseColor(Value:TColor);
    procedure SetFromBorder(Value:Word);
    procedure SetState(Value:Boolean);
    procedure SetFlash(Value:Boolean);
    procedure SetLedRadius(Value:Word);
    procedure SetCaption(Value: TCaption);
    procedure SetDown(Value:Boolean);
    procedure Paint; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Redraw(AOwner:TObject);
    procedure DoChange(Sender: TObject);
    procedure PreThread;

  published
    Property State:Boolean read fState write SetState default false;
    Property Flash:Boolean read fFlash write SetFlash default false;
    property TrueColor:TColor read fTrueColor write SetTrueColor;
    property FalseColor:TColor read fFalseColor write SetFalseColor;
    property FromBorder:Word read fFromBorder write SetFromBorder;
    Property LedRadius:Word read fLedRadius write SetLedRadius;
    property Speed: integer read FSpeed write FSpeed;
    property Caption: TCaption read FCaption write SetCaption;
    Property Down:Boolean read fDown write SetDown;


    property Hint;
    property ShowHint;
    property Color;
    Property Enabled;
    property Font;
    property onClick;
//    property onDblClick;
    property onMouseMove;
    property onMouseDown;
    Property onMouseUp;
    property onEnter;
    property onExit;
  end;

procedure Register;

implementation

procedure Register;
begin
   RegisterComponents('JSIComponents', [TJSILedButton]);
end;

{##############################################################################}

constructor TJSILedButtonThread.Create(DoNotify: TNotifyEvent);
begin
   inherited Create(true);
   FreeOnTerminate := true;
   FNotify := DoNotify;
end;

procedure TJSILedButtonThread.Execute;
begin
//led flash timer
   while not Terminated do
   begin
      SleepEx(iSpeed, false);
      Synchronize(DoEvent);
   end;
end;

procedure TJSILedButtonThread.DoEvent;
begin
   FNotify(Self);
end;

{##############################################################################}

constructor TJSILedButton.Create(aOwner: TComponent);
begin
   inherited Create(aOwner);
   Down:=false;
   State:=False;
   Flash:=False;
   TrueColor:=clRed;
   FalseColor:=clGray;
   Caption:=Name;
   LedRadius:=5;
   FromBorder:=4;
//   Font.onChange:=Redraw;
   DragMode:=dmManual;
   Speed := 500;
   Height := 25;
   Width := 100;
   FThread := TJSILedButtonThread.Create(DoChange);
   Repaint;
end;

destructor TJSILedButton.Destroy;
begin
   FThread.Terminate;
   inherited;
end;

procedure TJSILedButton.SetTrueColor(Value:TColor);
begin
   if fTrueColor <> Value then
      begin
         fTrueColor:=Value;
         Repaint;
      end;
end;

procedure TJSILedButton.SetFromBorder(Value:Word);
begin
   if fFromBorder <> Value then
      begin
         fFromBorder:=Value;
         Repaint;
      end;
end;

procedure TJSILedButton.SetLedRadius(Value:Word);
begin
   if fLedRadius <> Value then
      begin
         fLedRadius:=Value;
         Repaint;
      end;
end;

procedure TJSILedButton.SetFalseColor(Value:TColor);
begin
   if fFalseColor <> Value then
      begin
         fFalseColor:=Value;
         Repaint;
      end;
end;
procedure TJSILedButton.Redraw(AOwner:Tobject);
begin
  Paint;
end;

procedure TJSILedButton.SetState(Value:Boolean);
begin
   if fState <> Value then
      begin
         fState:=Value;
         PreThread;
         Repaint;
      end;
end;

procedure TJSILedButton.SetDown(Value:Boolean);
begin
   if fDown <> Value then
      begin
         fDown:=Value;
         if fDown then
            fButtonState := bsDown
         else
            fButtonState := bsUp;
         PreThread;
         Repaint;
      end;
end;

procedure TJSILedButton.SetFlash(Value:Boolean);
begin
   if fFlash <> Value then
      begin
         fFlash:=Value;
         PreThread;
         Repaint;
      end;
end;

procedure TJSILedButton.SetCaption(Value: TCaption);
begin
  if Value='' then FCaption:=Name;
  if AnsiCompareStr(Value, FCaption) <> 0 then FCaption := Value;
  Repaint;
end;

procedure TJSILedButton.PreThread;
begin
//click event
  FThread.iSpeed := FSpeed;
  if Flash then
    if State then
      FThread.Resume //resume led flash
    else
      FThread.Suspend; //suspend led flash
end;

procedure TJSILedButton.Click;
begin
//click event
  State:= not State;
  if fDown then
    if not fState then
     fButtonState := bsUp
    else
     fButtonState := bsDown;
  PreThread;
  if fState then
     Brush.Color:=fTrueColor
  else
     Brush.Color:=fFalseColor;
  Repaint;
  inherited Click;
end;
{
procedure TJSILedButton.DblClick;
begin
//dblclick event
  State:= not State;
  if fDown then
    if not fState then
     fButtonState := bsUp
    else
     fButtonState := bsDown;
  PreThread;
  if fState then
     Brush.Color:=fTrueColor
  else
     Brush.Color:=fFalseColor;
  Repaint;
  inherited DblClick;
end;
}
procedure TJSILedButton.Paint;
var
  Area, CaptArea:TRect;
  FromBorder:Integer;
  Distance:Integer;
begin
     inherited Paint;
     if Caption ='' then Caption:=Name;
     if not Enabled and not (csDesigning in ComponentState) then
        if fButtonState = bsDisabled then fButtonState := bsUp;
//draw button
     Area:=DrawButtonFace(Canvas,Rect(0,0,Width,Height),2,bsNew,False,fButtonState  in [bsDown],false);
     Canvas.Brush.color:=Color;
     Canvas.FillRect(Area);
     with Canvas do
        begin
         Distance:=2*fLedRadius + fFromBorder;
//led area
         Led:=Rect(Area.left+fFromBorder,Area.top+fFromBorder, Area.left+Distance,Area.top+Distance);
//draw caption text
         CaptArea:=Rect(Led.Right+1,Area.Top+3,Area.Right,Area.Bottom);
         if Caption <> '' then
            begin
              Font.Assign(Self.Font);
              DrawText(handle,pchar(Caption),length(Caption),CaptArea,DT_Center or DT_WordBreak);
            end;
//first led color
         if fState then
             Brush.Color:=fTrueColor
         else
             Brush.Color:=fFalseColor;
//draw led
         Pen.Width:=1;
         Pen.Color:=cl3DDkShadow;
         Ellipse(Led.Left,Led.Top,Led.Right,Led.Bottom);
         pen.Width:=2;
         Pen.color:=clWhite;
         MoveTo(Led.Left+3,Led.Top+fLedRadius-1);
         LineTo(Led.Left+fLedRadius-1,Led.Top+3);
         MoveTo(Led.Left+3,Led.Top+fLedRadius-2);
         LineTo(Led.Left+fLedRadius-2,Led.Top+3);
       end;
end;

procedure TJSILedButton.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if (Button = mbLeft) and Enabled then
  begin
    fButtonState := bsDown;
    Repaint;
  end;
 inherited MouseDown(Button, Shift, X, Y);
end;

procedure TJSILedButton.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewState: TButtonState;
begin
  if (shift =[ssLeft]) then begin
    if (X >= 0) and (X < ClientWidth) and (Y >= 0) and (Y <= ClientHeight) then
      NewState := bsdown else NewState := bsUp;
    if NewState <> fButtonState then
    begin
      fButtonState := NewState;
      Repaint;
    end;
  end;
  inherited MouseMove(Shift, X, Y);
end;

procedure TJSILedButton.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
  DoClick: Boolean;
begin
  begin
    DoClick := (X >= 0) and (X < ClientWidth) and (Y >= 0) and (Y <= ClientHeight);
    if not fDown then
      fButtonState := bsUp;
  end;
  Repaint;
  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TJSILedButton.DoChange(Sender: TObject);
begin
   with canvas do
   begin
//change led color
     if bFlag then
       Brush.Color:=fTrueColor
     else
       Brush.Color:=fFalseColor;
//new led state
     bFlag:=not bFlag;
//redraw led
         Pen.Width:=1;
         Pen.Color:=cl3DDkShadow;
         Ellipse(Led.Left,Led.Top,Led.Right,Led.Bottom);
         pen.Width:=2;
         Pen.color:=clWhite;
         MoveTo(Led.Left+3,Led.Top+fLedRadius-1);
         LineTo(Led.Left+fLedRadius-1,Led.Top+3);
         MoveTo(Led.Left+3,Led.Top+fLedRadius-2);
         LineTo(Led.Left+fLedRadius-2,Led.Top+3);
   end;
end;

end.
