object Form1: TForm1
  Left = 198
  Top = 105
  Width = 583
  Height = 282
  Caption = 'JSI Virtual Instrument v1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object JSIGauge1: TJSIGauge
    Left = 336
    Top = 8
    Width = 120
    Height = 80
    BevelOuter = bv_Raised
    BorderShow = True
    Caption = 'LEVEL'
    Max = 100
    Min = 0
    NeedleWidth = 2
    Text = 'm3'
    WarningPoint = 80.000000000000000000
    WarningWidth = 3
    ScaleFactor = 1.000000000000000000
  end
  object JSIButtonText1: TJSIButtonText
    Left = 168
    Top = 128
    Width = 149
    Height = 110
    OnMouseMove = JSIButtonText1MouseMove
    BevelOuter = bv_Raised
    BorderDraw = True
    Caption = 'MULTIPLICATOR'
    NeedleWidth = 4
    NeedleColor = clLime
    ProgressValue = 0
    SectorNumber = 5
    DrawScale = True
    ScaleColor = clGreen
    TicLabel.Strings = (
      'x1'
      'x2'
      'x3'
      'x4'
      'x5'
      'x6')
  end
  object JSIButton1: TJSIButton
    Left = 336
    Top = 128
    Width = 125
    Height = 110
    OnMouseMove = JSIButton1MouseMove
    BevelOuter = bv_Raised
    BorderShow = True
    Caption = 'GAIN'
    NeedleWidth = 4
    NeedleColor = clBlue
    StepValue = 1.000000000000000000
    SectorNumber = 10
    DrawScale = True
    WarningPoint = 80.000000000000000000
    WarningWidth = 3
  end
  object JSIButtonText2: TJSIButtonText
    Left = 8
    Top = 128
    Width = 149
    Height = 110
    OnMouseMove = JSIButtonText2MouseMove
    BevelOuter = bv_Raised
    BorderDraw = True
    Caption = 'LEVEL'
    NeedleWidth = 4
    ProgressValue = 0
    SectorNumber = 1
    DrawScale = True
    ScaleColor = clRed
    TicLabel.Strings = (
      'MAN'
      'AUTO')
  end
  object StaticText1: TStaticText
    Left = 344
    Top = 96
    Width = 113
    Height = 24
    Alignment = taRightJustify
    AutoSize = False
    BorderStyle = sbsSunken
    Caption = '0'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 0
  end
  object JSILampButton1: TJSILampButton
    Left = 8
    Top = 72
    Width = 153
    Height = 41
    Flash = True
    TrueColor = clRed
    FalseColor = clGray
    Speed = 500
    Caption = 'POWER'
    Down = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    onClick = JSILampButton1Click
    onMouseMove = JSIButton1MouseMove
  end
  object JSILed1: TJSILed
    Left = 472
    Top = 40
    Width = 68
    Height = 14
    Position = poRight
    TrueColor = clRed
    FalseColor = clGray
    Caption = 'LEVEL1'
  end
  object JSILamp1: TJSILamp
    Left = 464
    Top = 64
    Width = 80
    Height = 20
    TrueColor = clRed
    FalseColor = clGray
    Caption = 'WARNING'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
  end
  object JSILed2: TJSILed
    Left = 472
    Top = 16
    Width = 68
    Height = 14
    Position = poRight
    TrueColor = clRed
    FalseColor = clGray
    Caption = 'LEVEL2'
  end
  object JSILed3: TJSILed
    Left = 16
    Top = 136
    Width = 15
    Height = 14
    Position = poRight
    TrueColor = clRed
    FalseColor = clGray
  end
  object JSILedButton1: TJSILedButton
    Left = 472
    Top = 96
    Width = 89
    Height = 25
    Flash = True
    TrueColor = clRed
    FalseColor = clGray
    FromBorder = 4
    LedRadius = 7
    Speed = 500
    Caption = 'LOCK'
    Down = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    onClick = JSILedButton1Click
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer1Timer
    Left = 504
    Top = 192
  end
end
