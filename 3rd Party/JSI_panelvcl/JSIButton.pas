unit JSIButton;

////////////////////////////////////////////////////////
// TJSIButton - Panel Incremental Button v2.1         //
//                                                    //
//   Copyright (C) 2005 Stefan Ion                    //
//   J.S.Industries S.R.L.                            //
//   Software Development, Embedded Controllers,      //
//   Automation, Custom Design and Prototyping        //
//   http://www.jsindustries.ro                       //
//   office@jsindustries.ro                           //
//   ROMANIA                                          //
//                                                    //
//   License:                                         //
//   This source code is distributed as a freeware.   //
//   You may use and modify this source code for your //
//   freeware products.                               //
//   All functions, procedures and classes may        //
//   NOT BE USED IN COMMERCIAL PRODUCTS               //
//   without  the permission of the author.           //
//                                                    //
//   Disclaimer of warranty:                          //
//   "This software is supplied as is. The author     //
//   disclaims all warranties, expressed or implied,  //
//   including, without limitation, the warranties    //
//   of merchantability and of fitness for any        //
//   purpose. The author assumes no liability for     //
//   damages, direct or consequential, which may      //
//   result from the use of this software."           //
//                                                    //
//   Please report bugs to:                           //
//   Stefan Ion                                       //
//   office@jsindustries.ro                           //
//                                                    //
//   Please contact use for your suggestions or       //
//   projects proposal                                //
////////////////////////////////////////////////////////


interface

uses
  Windows, SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, Math, StdCtrls;
type
  TJSI_BevelOuter = (bv_None, bv_Raised, bv_Lowered, bv_Gutter, bv_BlackRect, bv_BoldRect, bv_FocusRect);
  TJSIButton = class(TGraphicControl)
  private
    { Private declarations }
    FFrameStyle:TJSI_BevelOuter;
    FCaptionText:String;
    FBorderShow:boolean;
    FCaptionColor: TColor;
    FScaleColor:TColor;
    FSectorNo: integer;
    FDrawScale:boolean;
    FNeedleColor:TColor;
    FWarningColor:TColor;
    FWarningPoint:Single;
    FWarningWidth:Integer;
    FButtonHeight:Integer;
    FButtonLeft:Integer;
    FMax:Longint;
    FMin:Longint;
    FNeedleDegree:Integer;
    FNeedleWidth:Integer;
    FOuterRadius:Integer;
    FProgress:single;
    FStep: single;
    FSectorDegree:Integer;
    FButtonTop:Integer;
    FButtonWidth:Integer;
    FButton: TMouseButton;
    FButtonDown: Boolean;
    PosDeg0,PosDeg: Integer;
    FMaxDeg: integer;
    procedure SetBevelOuter(const Value:TJSI_BevelOuter);
    Procedure DrawCanvas;
    Procedure DrawButton(x1,y1,x2,y2,R,PieDeg:Integer; WP:single);
    Procedure DrawNeedle(Deg,OldDeg,x1,y1,x2,y2,R:integer);
    Procedure SetScaleColor(Value:TColor);
    Procedure SetWarningPoint(Value:single);
    Procedure SetWarningColor(Value:TColor);
    Procedure SetWarningWidth(Value:Integer);
    Procedure SetMaxValue(Value:LongInt);
    Procedure SetMinValue(Value:LongInt);
    Procedure SetNeedleWidth(Value:Integer);
    Procedure SetNeedleColor(Value:TColor);
    Procedure SetProgress(Value:Single);
    Function  DegToRad(inputDeg:Double):Double;
    Function  GetArcPointX(Deg:Double;x1,x2,d:integer):Integer;
    Function  GetArcPointY(Deg:Double;y1,y2,d:integer):Integer;
    Function  GetNeedleDegree(Min,Max:LongInt; Progress: single):Integer;
    Function  GetRx(x1,x2:Integer):Integer;
    Function  GetRy(y1,y2:Integer):Integer;
    Function  GetR(x1,x2:Integer):Integer;
    function PositionDeg(x,y: integer): integer;
    Function  GetProgress(Min,Max:LongInt; NeedleDegree: integer):single;
    procedure DrawBorder;
    Procedure SetStep(Value: single);
    Procedure SetSectorNo(Value:integer);
    Procedure SetDrawScale(Value:boolean);
    Procedure SetCaptionColor(Value:TColor);
    Procedure SetCaptionText(Value:String);
    Procedure SetBorderShow(Value: boolean);

  protected
    { Protected declarations }
    Procedure Paint; override;
    Property  Canvas;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;

  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;

  published
    { Published declarations }
    Property Enabled;
    Property Hint;
    Property Left;
    Property Name;
    Property ShowHint;
    Property OnClick;
    Property OnDblClick;
    Property OnMouseDown;
    Property OnMouseMove;
    Property OnMouseUp;
    Property Top;
    Property Visible;

    property BevelOuter:TJSI_BevelOuter Read FFrameStyle Write SetBevelOuter;
    Property BorderShow: boolean Read FBorderShow Write SetBorderShow;
    Property Caption:String Read FCaptionText Write SetCaptionText;
    Property Max:Longint Read FMax Write SetMaxValue Default 100;
    Property Min:Longint Read FMin Write SetMinValue Default 0;
    Property CaptionColor:TColor Read FCaptionColor Write SetCaptionColor Default ClBlack;
    Property NeedleWidth:Integer Read FNeedleWidth Write SetNeedleWidth Default 1;
    Property NeedleColor:TColor Read FNeedleColor Write SetNeedleColor Default ClRed;
    Property ProgressValue: single Read FProgress Write SetProgress;
    Property StepValue: single Read FStep Write SetStep;
    Property SectorNumber: integer Read FSectorNo Write SetSectorNo;
    Property DrawScale: boolean Read FDrawScale Write SetDrawScale;
    Property ScaleColor:TColor Read FScaleColor Write SetScaleColor Default ClBlack;
    Property WarningColor:TColor Read FWarningColor Write SetWarningColor Default ClRed;
    Property WarningPoint:Single Read FWarningPoint Write SetWarningPoint;
    Property WarningWidth:Integer Read FWarningWidth Write SetWarningWidth Default 1;

  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('JSIComponents', [TJSIButton]);
end;

constructor TJSIButton.Create(AOwner: TComponent);
begin
     inherited Create(AOwner);
     FFrameStyle:= bv_Raised;
     FBorderShow:=true;
     FScaleColor:=clBlack;
     FCaptionText:='';
     FCaptionColor:=clBlack;
     FWarningColor:=clRed;
     FWarningPoint:=80;
     FWarningWidth:=1;
     FNeedleColor:=clRed;
     FSectorNo:=10;
     FDrawScale:=true;
     FMax:=100;
     FMin:=0;
     FNeedleWidth:=1;
     FOuterRadius:=25;
     FProgress:=0;
     FStep:=1;
     FSectorDegree:=60;

     Height:=110;
     Width:=125;

     FButtonTop:=-11;
     FButtonLeft:=0;
     FButtonWidth:=width;
     FButtonHeight:=width;
     FNeedleDegree:=30;
     PosDeg0:=30;
     Refresh;
end;

Procedure TJSIButton.Paint;
begin
     inherited Paint;
     DrawCanvas;
end;

Procedure TJSIButton.DrawCanvas;
var NeedleOldDegree: integer;
begin
     DrawButton(FButtonLeft,FButtonTop,FButtonLeft+FButtonWidth,FButtonTop+FButtonHeight,
     FOuterRadius,FSectorDegree,FWarningPoint);
     NeedleOldDegree:=FNeedleDegree;
     FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
     DrawNeedle(FNeedleDegree,NeedleOldDegree,FButtonLeft,FButtonTop,FButtonLeft+FButtonWidth,
     FButtonTop+FButtonHeight,FOuterRadius);

end;

Procedure TJSIButton.SetNeedleWidth(Value:Integer);
begin
   if Value<>FNeedleWidth Then begin
        If Value<1 Then Value:=1;
        FNeedleWidth:=Value;
        Refresh;
   end;
end;

Procedure TJSIButton.SetNeedleColor(Value:TColor);
begin
     IF Value<>FNeedleColor Then begin
        FNeedleColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIButton.SetScaleColor(Value:TColor);
begin
     IF Value<>FScaleColor Then begin
        FScaleColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIButton.SetCaptionText(Value:String);
begin
   if Value<>FCaptionText Then begin
        FCaptionText:=Value;
        Refresh;
   end;
end;

Procedure TJSIButton.SetCaptionColor(Value:TColor);
begin
     IF Value<>FCaptionColor Then begin
        FCaptionColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIButton.SetWarningPoint(Value:single);
begin
     if FWarningPoint<>Value Then Begin
        if Value > FMax then Value:=FMax;
        if Value < FMin then Value:=FMin;
        FWarningPoint:=Value;
        Refresh;
     end;
end;

Procedure TJSIButton.SetWarningColor(Value:TColor);
begin
     IF Value<>FWarningColor Then begin
        FWarningColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIButton.SetWarningWidth(Value:Integer);
begin
   if Value<>FWarningWidth Then begin
        If Value<1 Then Value:=1;
        FWarningWidth:=Value;
        Refresh;
   end;
end;


Procedure TJSIButton.SetProgress(Value:single);
var NeedleOldDegree: integer;
begin
    if Value<>FProgress Then
    Begin
       IF Value > FMax Then Value:=FMax;
       IF Value < FMin Then Value:=FMin;
       FProgress:=Value;
       NeedleOldDegree:=FNeedleDegree;
       FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
       DrawNeedle(FNeedleDegree,NeedleOldDegree,FButtonLeft,FButtonTop,FButtonLeft+FButtonWidth,
       FButtonTop+FButtonHeight,FOuterRadius);
    end;
end;

Procedure TJSIButton.SetMinValue(Value:LongInt);
begin
     IF (Value <> FMin) and (Value < FMax) and (Value > 0) Then
     Begin
        FProgress:=(FMax-Value)*(FProgress-FMin)/(FMax-FMin) + Value ;
        FWarningPoint:=Integer(Round((FMax-Value)*(FWarningPoint-FMin)/(FMax-FMin) + Value));
        FStep:=(FMax-Value)*FStep/(FMax-FMin);
        FMin:=Value;
        FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
        Refresh;
     End;
end;

Procedure TJSIButton.SetMaxValue(Value:LongInt);
begin
     IF (Value <> FMax) and (Value > FMin) and (Value > 0) Then
     Begin
        FProgress:=(Value-FMin)*(FProgress-FMin)/(FMax-FMin) + FMin ;
        FWarningPoint:=Integer(Round((Value-FMin)*(FWarningPoint-FMin)/(FMax-FMin) + FMin));
        FStep:=(Value-FMin)*FStep/(FMax-FMin);
        FMax:=Value;
        FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
        Refresh;
     End;
end;

Procedure TJSIButton.SetStep(Value:single);
begin
    if (Value<>FStep) Then
    Begin
       if Value < 0 then Value:=0;
       IF Value > (FMax-FMin)/2 Then Value:=(FMax-FMin);
       IF Value < (FMax-FMin)/100 Then begin
          Value:=Round((FMax-FMin)*1000)/100000;
          if Value > 1 then Value:=1;
       end;
       FStep:=Value;
       Refresh
    end;
end;

Procedure TJSIButton.SetSectorNo(Value:integer);
begin
    if (Value<>FSectorNo) Then
    Begin
       IF Value <= 2 Then Value:=2;
       IF Value > 10 Then Value:=10;
       FSectorNo:=Value;
       Refresh
    end;
end;

Procedure TJSIButton.SetDrawScale(Value:boolean);
begin
    if (Value<>FDrawScale) Then
    Begin
       FDrawScale:=Value;
       Refresh
    end;
end;


Procedure TJSIButton.SetBorderShow(Value: boolean);
begin
    if (Value<>FBorderShow) Then
    Begin
       FBorderShow:=Value;
       Refresh
    end;
end;

Function TJSIButton.DegToRad(inputDeg:Double):Double;
begin
     Result:=inputDeg*pi/180;
end;

procedure TJSIButton.DrawBorder;
var   R, ARect:TRect;

begin
 R:=Rect(0,0,width,height);
 with Canvas do
 begin
   Brush.style:= bsClear;
   with Pen do begin
     Style:= psSolid;
     Mode:=pmCopy;
   end;
   with R do rectangle(left+1, top+1, right-1, bottom-1);
    case fFrameStyle of
      bv_None: with R do begin
         Pen.Color:= Self.Color;
         rectangle(left, top, right, bottom);
         rectangle(left+1, top+1, right-1, bottom-1);
       end;
      bv_Raised : with R do begin
         Frame3D(Canvas, R, clBtnHighlight, clBtnShadow,1);
         Pen.Color:= Self.Color;
         rectangle(left, top, right, bottom);
        end;
      bv_Lowered: with R do begin
         Frame3D(Canvas, R, clBtnShadow, clBtnHighlight,1);
         Pen.Color:= Self.Color;
         rectangle(left, top, right, bottom);
        end;
      bv_Gutter : with R do begin
         Pen.Color := clBtnHighlight;
         rectangle(left + 1, top + 1, right, bottom);
         Pen.Color := clBtnShadow;
         rectangle(left, top, right - 1, bottom - 1);
        end;
      bv_BlackRect: with R do begin
         Pen.Color := clBlack;
         rectangle(left, top, right, bottom);
         Pen.Color:= Self.Color;
         rectangle(left+1, top+1, right-1, bottom-1);
        end;
      bv_BoldRect: with R do begin
         Pen.Color := clBlack;
         rectangle(left, top, right, bottom);
         rectangle(left+1, top+1, right-1, bottom-1);
        end;
      bv_FocusRect: begin
         Brush.Style:= bsSolid;  Brush.Color:= clWhite;
         Pen.Style  := psDot;    Pen.Color  := clBlack;
         with R do polyline([point(left,top),point(right-2,top),
                  point(right-2,bottom-1),point(left+1,bottom-1),point(left+1,top)]);
         with R do polyline([point(left+1,top+1),point(right-1,top+1),
                  point(right-1,bottom-2),point(left,bottom-2),point(left,top)]);
        end;
    end;
    ARect := Rect(1, 1, width-1, height-1);
    Pen.Style:= psSolid;
    Pen.Mode:=pmCopy;
    Brush.Color := clBtnFace;
    FillRect(ARect);

 end;
end;

Procedure TJSIButton.DrawButton (x1,y1,x2,y2,R,PieDeg:integer;WP:single);
var

  S0x,S0y,S1x,S1y,S2x,S2y:integer;
  WSx,WSy: integer;
  Pnx0,Pny0,Pnx1,Pny1,Pnx2,Pny2:integer;
  D,Idx:integer;
  P1Deg,P2Deg,PnDeg,PnDeg0:integer;
  WDeg,SimePieDeg:integer;
  ARect, BRect: TRect;
  Tx, Ty: integer;
  Tic: single;
  S: string;
  left_b,top_b,right_b,bottom_b: integer;
  X, Y: integer;
  WText, HText: integer;

begin
  Height:=110;
  Width:=125;
  DrawBorder;
  with Canvas Do begin
      {Initialize Pen}
      Pen.Mode:=pmCopy;

      left_b:=0;
      top_b:=0;
      right_b:=120;
      bottom_b:=105;

      D:=GetR(x1,x2)-R;
      SimePieDeg:=PieDeg Div 2;

      S0x:=GetArcPointX(SimePieDeg,x1,x2,R);
      S0y:=GetArcPointY(SimePieDeg,y1,y2,R);
      S1x:=GetArcPointX(180+30,x1,x2,R);
      S1y:=GetArcPointY(180+30,y1,y2,R);

      Pen.Color := clBtnHighlight;
      Pen.Width:=2;
      Arc(x1+D+5,y1+D+5,x2-D-5,y2-D-5,S1x,S1y,S0x,S0y);//button-high

      S2x:=GetArcPointX(30,x1,x2,R);
      S2y:=GetArcPointY(30,y1,y2,R);
      Pen.Color:= clBtnShadow;
      Pen.Width:=2;
      Arc(x1+D+5,y1+D+5,x2-D-5,y2-D-5,S2x,S2y,S1x,S1y);//button-low

      S0x:=GetArcPointX(0,x1,x2,R);
      S0y:=GetArcPointY(0,y1,y2,R);
      S1x:=GetArcPointX(0,x1,x2,R);
      S1y:=GetArcPointY(0,y1,y2,R);
      Pen.Color := clGray;
      Pen.Width:=1;
      Arc(x1+D+4,y1+D+4,x2-D-4,y2-D-4,S1x,S1y,S0x,S0y);//button-contur

//Draw tics
      Pen.Width:=1;
      FMaxDeg:=SimePieDeg+FSectorNo*10*(360-PieDeg) Div 100 + 30;
      WDeg:=SimePieDeg+Integer(Round((WP-FMin)*(FMaxDeg-PieDeg)/(FMax-FMin)));
      PnDeg0:=SimePieDeg;
      For Idx:=0 To FSectorNo do begin
            PnDeg:=SimePieDeg+Idx*10*(360-PieDeg) Div 100;
            Pnx1:=GetArcPointX(PnDeg,x1,x2,R);
            Pny1:=GetArcPointY(PnDeg,y1,y2,R);
            if Idx <> 0 then
             begin
               Pnx0:=GetArcPointX(PnDeg0,x1,x2,R);
               Pny0:=GetArcPointY(PnDeg0,y1,y2,R);
               if FDrawScale then
                begin
                 Pen.Color:=FScaleColor;
                 Pen.Width:=1;
                 Arc(x1+D,y1+D,x2-D,Y2-D,Pnx1,Pny1,Pnx0,Pny0);//scale
               end;
               if (WDeg <= PnDeg) and (idx = FSectorNo) and (WP<>FMax) then
               begin
                 WSx:=GetArcPointX(WDeg,x1,x2,R);
                 WSy:=GetArcPointY(WDeg,y1,y2,R);
                 if FDrawScale then
                  begin
                   Pen.Color:=FWarningColor;
                   Pen.Width:=FWarningWidth;
                   Arc(x1+D,y1+D,x2-D,Y2-D,Pnx1,Pny1,WSx,WSy);//warning
                end;
               end;
             end;
            PnDeg0:=PnDeg;

            if Idx Mod 2 = 1 then begin
//minor
               Pnx2:=GetArcPointX(PnDeg,x1,x2,R+5);
               Pny2:=GetArcPointY(PnDeg,y1,y2,R+5);
            end
            else begin
//major
               Pnx2:=GetArcPointX(PnDeg,x1,x2,R+8);
               Pny2:=GetArcPointY(PnDeg,y1,y2,R+8);
               Font := Self.Font;
               if PnDeg>=WDeg then
                 if (WP<>FMin) and (WP<>FMax)then
                   Font.Color := FWarningColor
                 else
                   Font.Color := FScaleColor
               else
                  Font.Color := FScaleColor;

               Brush.Color := Self.Color;
//               Brush.Color := clWhite;
//               Brush.Style := bsClear;
               Font.Style:=Self.Font.Style;

               Tic:=FMin+(FMax-FMin)/FSectorNo*Idx;
               If FMax < 5 then
                 S:=FloatToStrF(Tic,ffFixed, 7, 1)
               else
                 S:=FloatToStrF(Tic,ffFixed, 7, 0);

               Case Idx of
               0:
               begin
               BRect := Rect(Pnx2-25, Pny2, Pnx2, Pny2+11);
               X := Pnx2-25+(24-TextWidth(S));
               Y := Pny2+(11 - TextHeight(S)) div 2;
               TextRect(BRect, X+1, Y, S);
               end;
               2:
               begin
               BRect := Rect(Pnx2-26, Pny2-5, Pnx2-1, Pny2+6);
               X := Pnx2-26+(24 - TextWidth(S));
               Y := Pny2-5+(11 - TextHeight(S)) div 2;
               TextRect(BRect, X+1, Y, S);
               end;
               4, 6:
               begin
               BRect := Rect(Pnx2-12, Pny2-12, Pnx2+13, Pny2-1);
               X := Pnx2-12+(25 - TextWidth(S)) div 2;
               Y := Pny2-12+(11 - TextHeight(S)) div 2;
               TextRect(BRect, X+1, Y, S);
               end;
               8:
               begin
               BRect := Rect(Pnx2+1, Pny2-5, Pnx2+26, Pny2+6);
               X := Pnx2+1;
               Y := Pny2-5+(11 - TextHeight(S)) div 2;
               TextRect(BRect, X+1, Y, S);
               end;
               10:
               begin
               BRect := Rect(Pnx2, Pny2, Pnx2+25, Pny2+11);
               X := Pnx2;
               Y := Pny2+(11 - TextHeight(S)) div 2;
               TextRect(BRect, X+1, Y, S);
               end;
              end;
            end;

            if PnDeg>=WDeg then
              if (WP<>FMin) and (WP<>FMax) then
                 Pen.Color := FWarningColor
               else
                 Pen.Color := FScaleColor
             else
                Pen.Color := FScaleColor;
            MoveTo(Pnx1,Pny1);
            LineTo(Pnx2,Pny2);
      end;

     Brush.style:= bsClear;
     if FBorderShow then
      begin
       Pen.Width:=1;
       Pen.Mode:=pmCopy;
       Pen.Color:= clBtnHighlight;
       Rectangle(3, 3, 123, 101);
       Pen.Color:= clBtnShadow;
       Rectangle(2, 2, 122, 100);
     end;


//Draw caption
      Brush.Color := Self.Color;
//     Brush.Color := clWhite;
      Font.Style:=[fsBold];
      Font.Color:=FCaptionColor;
      X:=(left_b + right_b) div 2;
      Y:=(top_b + bottom_b) div 2+48;
      WText:=TextWidth(FCaptionText) div 2;
      HText:=TextHeight(FCaptionText) div 2;
      BRect := Rect(X-WText, Y-HText, X+WText, Y+HText);
      TextRect(BRect, X-WText, Y-HText, FCaptionText);

  end;
end;

Procedure TJSIButton.DrawNeedle(Deg,OldDeg,x1,y1,x2,y2,R:integer);
var
   x0,y0,x,y:integer;
begin
  if (Deg<=FMaxDeg) and (OldDeg<=FMaxDeg) then
  begin
  with Canvas Do begin
      x0:=GetRx(x1,x2);
      y0:=GetRy(y1,y2);
      x:=GetArcPointX(OldDeg,x1,x2,R-8);
      y:=GetArcPointY(OldDeg,y1,y2,R-8);
      Pen.Mode:=pmCopy;
      Pen.Color:=clBtnFace;
      Pen.Width:=FNeedleWidth;
      MoveTo(x0,y0);
      LineTo(x,y);

      x:=GetArcPointX(Deg,x1,x2,R-8);
      y:=GetArcPointY(Deg,y1,y2,R-8);
      Pen.Color:=FNeedleColor;
      Pen.Mode:=pmCopy;
      Pen.Width:=FNeedleWidth;
      MoveTo(x0,y0);
      LineTo(x,y);
      Pen.Mode:=pmCopy;
  end;
  end;
end;


Function TJSIButton.GetR(x1,x2:integer):integer;
begin
     Result:=Abs(x2-x1) div 2;
end;

Function TJSIButton.GetArcPointX(Deg:double;x1,x2,d:integer):integer;
var
   tmpint:integer;
begin
     tmpint:=Round(d*Sin(DegToRad(Deg)));
     Result:=GetRx(x1,x2)-tmpint;
end;

Function TJSIButton.GetArcPointY(Deg:double;y1,y2,d:integer):integer;
var
   tmpint:integer;
begin
      tmpint:=integer(Round(d*cos(DegToRad(Deg))));
      Result:=GetRy(y1,y2)+tmpint;
end;

Function TJSIButton.GetRx(x1,x2:integer):Integer;
begin
     Result:=x1+(x2-x1) div 2;
end;

Function TJSIButton.GetRy(y1,y2:integer):Integer;
begin
     Result:=y1+(y2-y1) div 2;
end;

Function  TJSIButton.GetNeedleDegree(Min,Max:LongInt; Progress: single):Integer;
var tmp: integer;
begin
     tmp:= Integer(Round((((360-FSectorDegree)*(Progress-Min))/(Max-Min))+(FSectorDegree/2)));
     if tmp <= 30 then Result:=30
     else if tmp > (30*(FSectorNo+1)) then
        Result:=30*(FSectorNo+1)
     else Result:=tmp;

end;

Function  TJSIButton.GetProgress(Min,Max:LongInt; NeedleDegree: integer):single;
var tmp: single;
begin
    try
     begin
       tmp:= (Max-Min)*(NeedleDegree-30)/(FMaxDeg-60)+Min;
       Result:=Round(tmp*100)/100;
     end;
    except
       Result:=0;
    end;
end;

function TJSIButton.PositionDeg(x,y: integer): integer;
var PDeg, Rxy, x0, y0: integer;
begin
      x0:=GetRx(FButtonLeft,FButtonLeft+FButtonWidth);
      y0:=GetRy(FButtonTop,FButtonTop+FButtonHeight);
      Rxy:=Integer(Round(sqrt(sqr(x-x0)+sqr(y-y0))));
      PDeg:=30;
      if Rxy <= FOuterRadius-6 then
       begin
        if Rxy<>0 then PDeg:=Integer(Round(arccos((Y-y0)/Rxy)*180/pi));
        if x < x0 then
          PDeg:=PDeg
        else
          PDeg:=360-PDeg;
       end
      else
        PosDeg0:=PDeg;
      Result:=PDeg;
end;

procedure TJSIButton.SetBevelOuter(const Value:TJSI_BevelOuter);
begin
  if Value<> FFrameStyle then  begin
    FFrameStyle := Value;
    Refresh;
  end;
end;

procedure TJSIButton.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);
  FButton := Button;
  FButtonDown := True;
  PosDeg0:=PositionDeg(x,y);
end;

procedure TJSIButton.MouseMove(Shift: TShiftState; X, Y: Integer);
var NeedleOldDegree: integer;
    StepDeg:integer;
begin
  inherited MouseMove(Shift, X, Y);
  if FButtonDown and (FButton = mbLeft) then
   begin

    StepDeg:=Integer(Round((FMaxDeg-60)*FStep/(FMax-FMin)));
    PosDeg:=PositionDeg(x,y);
    if abs(PosDeg-PosDeg0) > StepDeg then
    begin
      NeedleOldDegree:=FNeedleDegree;
      if (PosDeg-PosDeg0)>=0 then
         FNeedleDegree:=FNeedleDegree+StepDeg
      else
         FNeedleDegree:=FNeedleDegree-StepDeg;
     if FNeedleDegree <= 30 then FNeedleDegree:=30;
     if FNeedleDegree > 30*(FSectorNo+1) then FNeedleDegree:=30*(FSectorNo+1);
     FProgress:=GetProgress(FMin,FMax,FNeedleDegree);//-0.2;
     DrawNeedle(FNeedleDegree,NeedleOldDegree,FButtonLeft,FButtonTop,FButtonLeft+FButtonWidth,
     FButtonTop+FButtonHeight,FOuterRadius);
     PosDeg0:=PosDeg;
    end;
   end;
end;

procedure TJSIButton.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  FButtonDown := False;
  PosDeg0:=PosDeg;
end;

end.







