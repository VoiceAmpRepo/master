unit JSILed;

////////////////////////////////////////////////////////
// TJSILed - Panel Led v2.0                           //
//                                                    //
//   Copyright (C) 2005 Stefan Ion                    //
//   J.S.Industries S.R.L.                            //
//   Software Development, Embedded Controllers,      //
//   Automation, Custom Design and Prototyping        //
//   http://www.jsindustries.ro                       //
//   office@jsindustries.ro                           //
//   ROMANIA                                          //
//                                                    //
//   License:                                         //
//   This source code is distributed as a freeware.   //
//   You may use and modify this source code for your //
//   freeware products.                               //
//   All functions, procedures and classes may        //
//   NOT BE USED IN COMMERCIAL PRODUCTS               //
//   without  the permission of the author.           //
//                                                    //
//   Disclaimer of warranty:                          //
//   "This software is supplied as is. The author     //
//   disclaims all warranties, expressed or implied,  //
//   including, without limitation, the warranties    //
//   of merchantability and of fitness for any        //
//   purpose. The author assumes no liability for     //
//   damages, direct or consequential, which may      //
//   result from the use of this software."           //
//                                                    //
//   Please report bugs to:                           //
//   Stefan Ion                                       //
//   office@jsindustries.ro                           //
//                                                    //
//   Please contact use for your suggestions or       //
//   projects proposal                                //
////////////////////////////////////////////////////////

interface

uses
   Windows, Classes, Graphics, Controls, Buttons, SysUtils, Math;

type
  TLocation=(poTop,poRight,poBottom,poLeft);
  TJSILed = class(TCustomControl)
  private
    FTrueColor       : TColor;
    FFalseColor      : TColor;
    FState           : Boolean;
    FPosition        : TLocation;
    FLedRadius       : Word;
    FCaption         : TCaption;
    Led              : TRect;
  public
    constructor Create(AOwner:TComponent); override;

  protected
     procedure SetTrueColor(Value:TColor);
     procedure SetFalseColor(Value:TColor);
     procedure SetState(Value:Boolean);
     procedure SetPosition(Value:TLocation);
     procedure SetCaption(Value: TCaption);
     procedure Paint; override;
     procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
     procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
     procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
     procedure Redraw(AOwner:TObject);
     Property  Canvas;

  published
    Property Position:TLocation read fPosition write SetPosition;
    Property State:Boolean read fState write SetState default false;
    property TrueColor:TColor read fTrueColor write SetTrueColor;
    property FalseColor:TColor read fFalseColor write SetFalseColor;
    property Caption: TCaption read FCaption write SetCaption;
    property Color;
    Property Enabled;
    property Font;
    property Height;
    property Width;

    property onMouseMove;
    property onMouseDown;
    Property onMouseUp;
  end;

procedure Register;

implementation

procedure Register;
begin
   RegisterComponents('JSIComponents', [TJSILed]);
end;

{##############################################################################}

constructor TJSILed.Create(aOwner: TComponent);
begin
   inherited Create(aOwner);
   State:=False;
   TrueColor:=clRed;
   FalseColor:=clGray;
   Caption:='';
   fLedRadius:=6;
   Position:=poTop;
   Height := 15;
   Width := 12;
   Repaint;
end;

procedure TJSILed.SetTrueColor(Value:TColor);
begin
   if fTrueColor <> Value then
      begin
         fTrueColor:=Value;
         Repaint;
      end;
end;

procedure TJSILed.SetPosition(Value:TLocation);
begin
   if fPosition <> Value then
      begin
         fPosition:=Value;
         Repaint;
      end;
end;

procedure TJSILed.SetFalseColor(Value:TColor);
begin
   if fFalseColor <> Value then
      begin
         fFalseColor:=Value;
         Repaint;
      end;
end;
procedure TJSILed.Redraw(AOwner:Tobject);
begin
  Paint;
end;

procedure TJSILed.SetState(Value:Boolean);
begin
   if fState <> Value then
      begin
         fState:=Value;
         Repaint;
      end;
end;

procedure TJSILed.SetCaption(Value: TCaption);
begin
  if AnsiCompareStr(Value, FCaption) <> 0 then  FCaption := Value;
  Repaint;
end;

procedure TJSILed.Paint;
var
  AreaText:TRect;
  LText,RText,TText,BText,WText,HText: integer;
begin
     inherited Paint;
     Canvas.Brush.color:=Color;
     with Canvas do
        begin
         Font.Assign(Self.Font);
         WText:= TextWidth(Caption) div 2;
         HText:= TextHeight(Caption) div 2;
         case fPosition of
              poTop    ://text top position
              begin
                Width:=Max(Width,Max(TextWidth(Caption), 2*fLedRadius));
                Height:=Max(Height,TextHeight(Caption)+2+2*fLedRadius);
                LText:=(Width div 2)-WText;
                TText:=0;
                RText:=LText+2*WText;
                BText:=2*HText;
                AreaText := Rect(LText-1, TText, RText+1, BText);
                TextRect(AreaText, LText, TText, Caption);
                Led:=Rect((Width div 2)-fLedRadius, BText+2,(Width div 2)+fLedRadius,BText+2+2*fLedRadius);
              end;
              poRight   :
              begin
                Width:=Max(Width,2*fLedRadius+3+TextWidth(Caption));
                Height:=Max(Height,(Max(TextHeight(Caption), 2*fLedRadius)+1));
                LText:=2*fLedRadius+2;
                TText:=(Height div 2)-HText;
                RText:=LText+2*WText+1;
                BText:=(Height div 2)+HText;
                AreaText := Rect(LText, TText, RText, BText);
                TextRect(AreaText, LText, TText, Caption);
                Led:=Rect(0, (Height div 2)-fLedRadius, 2*fLedRadius, (Height div 2)+fLedRadius);
              end;
              poBottom:
              begin
                Width:=Max(Width,Max(TextWidth(Caption), 2*fLedRadius));
                Height:=Max(Height,TextHeight(Caption)+2+2*fLedRadius);
                LText:=(Width div 2)-WText;
                TText:=2*fLedRadius+2;
                RText:=LText+2*WText;
                BText:=TText+2*HText;
                AreaText := Rect(LText-1, TText, RText+1, BText);
                TextRect(AreaText, LText, TText, Caption);
                Led:=Rect((Width div 2)-fLedRadius, 0,(Width div 2)+fLedRadius,2*fLedRadius);
              end;
              poLeft ://text left
              begin
                Width:=Max(Width,2*fLedRadius+3+TextWidth(Caption));
                Height:=Max(Height,(Max(TextHeight(Caption), 2*fLedRadius)+1));
                LText:=0;
                TText:=(Height div 2)-HText;
                RText:=2*WText+1;
                BText:=(Height div 2)+HText;
                AreaText := Rect(LText, TText, RText, BText);
                TextRect(AreaText, LText, TText, Caption);
                Led:=Rect(2*WText+3,(Height div 2)-fLedRadius, 2*WText+3+2*fLedRadius, (Height div 2)+fLedRadius);

              end;
         end;
//first led color
         if State then
             Brush.Color:=fTrueColor
         else
             Brush.Color:=fFalseColor;
//draw led
         Pen.Width:=1;
         Pen.Color:=cl3DDkShadow;
         Ellipse(Led.Left,Led.Top,Led.Right,Led.Bottom);
         pen.Width:=2;
         Pen.color:=clWhite;
         MoveTo(Led.Left+3,Led.Top+fLedRadius-1);
         LineTo(Led.Left+fLedRadius-1,Led.Top+3);
         MoveTo(Led.Left+3,Led.Top+fLedRadius-2);
         LineTo(Led.Left+fLedRadius-2,Led.Top+3);
       end;
end;

procedure TJSILed.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
 inherited MouseDown(Button, Shift, X, Y);
end;

procedure TJSILed.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);
end;

procedure TJSILed.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
end;

end.
