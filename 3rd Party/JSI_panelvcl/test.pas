unit test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JSIButton, JSIButtonText, JSIGauge, JSILed,
  JSILampButton, JSILamp, ExtCtrls, JSILedButton;

type
  TForm1 = class(TForm)
    JSIGauge1: TJSIGauge;
    JSIButtonText1: TJSIButtonText;
    JSIButton1: TJSIButton;
    JSIButtonText2: TJSIButtonText;
    StaticText1: TStaticText;
    JSILampButton1: TJSILampButton;
    JSILed1: TJSILed;
    JSILamp1: TJSILamp;
    Timer1: TTimer;
    JSILed2: TJSILed;
    JSILed3: TJSILed;
    JSILedButton1: TJSILedButton;
    procedure JSIButton1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure JSILampButton1Click(Sender: TObject);
    procedure JSIButtonText1MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure JSIButtonText2MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure JSILedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.JSIButton1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if jsiledbutton1.Down then exit;
  if jsilampbutton1.State then begin
     jsigauge1.ProgressValue:=jsibutton1.ProgressValue;
     statictext1.Caption:=floattostr(jsibutton1.ProgressValue);
     jsiLamp1.State:=(jsibutton1.ProgressValue >= jsibutton1.WarningPoint);
     jsiled1.State:=jsiLamp1.State;
  end;

end;

procedure TForm1.JSILampButton1Click(Sender: TObject);
begin
  if jsilampbutton1.State then begin
     jsiled1.FalseColor:=clLime;
     statictext1.Font.Color:=clLime;
  end else begin
     jsiled1.state:=false;
     jsiled1.FalseColor:=clGray;
     statictext1.Font.Color:=clGray;
     jsigauge1.ProgressValue:=0;
     jsilamp1.State:=false;
  end;
end;

procedure TForm1.JSIButtonText1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
   jsigauge1.ScaleFactor:= JSIButtonText1.ProgressValue+1;
   jsibutton1.Max:=round(100*jsigauge1.ScaleFactor);

end;

procedure TForm1.JSIButtonText2MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  timer1.enabled:=(jsibuttontext2.ProgressValue=1);
  Randomize;
  jsiled3.state:=timer1.enabled;

end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  if jsiledbutton1.Down then exit;
  jsigauge1.ScaleFactor:= JSIButtonText1.ProgressValue+1;
  jsibutton1.Max:=round(100*jsigauge1.ScaleFactor);
  if jsilampbutton1.State then begin
     jsibutton1.ProgressValue:=random(jsibutton1.Max);
     jsigauge1.ProgressValue:=jsibutton1.ProgressValue;
     statictext1.Caption:=floattostr(jsibutton1.ProgressValue);
     jsiLamp1.State:=(jsibutton1.ProgressValue >= jsibutton1.WarningPoint);
     jsiled1.State:=jsiLamp1.State;
  end;
end;

procedure TForm1.JSILedButton1Click(Sender: TObject);
begin
jsiledbutton1.Down:=not jsiledbutton1.Down;
end;

end.
