unit JSIGauge;

////////////////////////////////////////////////////////
// TJSIGauge -  Panel Gauge-VUMeter v1.5              //
//                                                    //
//   Copyright (C) 2005 Stefan Ion                    //
//   J.S.Industries S.R.L.                            //
//   Software Development, Embedded Controllers,      //
//   Automation, Custom Design and Prototyping        //
//   http://www.jsindustries.ro                       //
//   office@jsindustries.ro                           //
//   ROMANIA                                          //
//                                                    //
//   License:                                         //
//   This source code is distributed as a freeware.   //
//   You may use and modify this source code for your //
//   freeware products.                               //
//   All functions, procedures and classes may        //
//   NOT BE USED IN COMMERCIAL PRODUCTS               //
//   without  the permission of the author.           //
//                                                    //
//   Disclaimer of warranty:                          //
//   "This software is supplied as is. The author     //
//   disclaims all warranties, expressed or implied,  //
//   including, without limitation, the warranties    //
//   of merchantability and of fitness for any        //
//   purpose. The author assumes no liability for     //
//   damages, direct or consequential, which may      //
//   result from the use of this software."           //
//                                                    //
//   Please report bugs to:                           //
//   Stefan Ion                                       //
//   office@jsindustries.ro                           //
//                                                    //
//   Please contact use for your suggestions or       //
//   projects proposal                                //
////////////////////////////////////////////////////////

interface

uses
  Windows, SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, Math, StdCtrls;
type
  TJSI_BevelOuter = (bv_None, bv_Raised, bv_Lowered, bv_Gutter, bv_BlackRect, bv_BoldRect, bv_FocusRect);
  TJSIGauge = class(TGraphicControl)
  private
    { Private declarations }
    FFrameStyle:TJSI_BevelOuter;
    FGaugeHeight:Integer;
    FGaugeLeft:Integer;
    FBorderShow:boolean;
    FColor:TColor;
    FBackgroundColor:TColor;
    FCaptionText:String;
    FCaptionColor: TColor;
    FTextColor:TColor;
    FText:String;
    FScaleColor:TColor;
    FScaleFactor:Single;
    FNeedleColor:TColor;
    FWarningColor:TColor;
    FWarningPoint:Single;
    FWarningWidth:Integer;
    FMax:Longint;
    FMin:Longint;
    FProgress0:single;
    FNeedleDegree:Integer;
    FNeedleWidth:Integer;
    FOuterRadius:Integer;
    FSectorDegree:Integer;
    FProgress:single;
    FGaugeTop:Integer;
    FGaugeWidth:Integer;
    procedure SetBevelOuter(const Value:TJSI_BevelOuter);
    Procedure DrawCanvas;
    Procedure DrawGauge(x1,y1,x2,y2,R,PieDeg:Integer; WP:single);
    Procedure DrawNeedle(Deg,x1,y1,x2,y2,R:Integer);
    Procedure SetBackgroundColor(Value:TColor);
    Procedure SetScaleColor(Value:TColor);
    Procedure SetTextColor(Value:TColor);
    Procedure SetWarningPoint(Value:Single);
    Procedure SetWarningColor(Value:TColor);
    Procedure SetWarningWidth(Value:Integer);
    Procedure SetMaxValue(Value:Longint);
    Procedure SetMinValue(Value:Longint);
    Procedure SetNeedleWidth(Value:Integer);
    Procedure SetNeedleColor(Value:TColor);
    Procedure SetProgress(Value:Single);
    Function  DegToRad(inputDeg:Double):Double;
    Function  GetArcPointX(Deg:Double;x1,x2,d:integer):Integer;
    Function  GetArcPointY(Deg:Double;y1,y2,d:integer):Integer;
    Function  GetNeedleDegree(Min,Max:LongInt; FProgress: single):Integer;
    Function  GetRx(x1,x2:Integer):Integer;
    Function  GetRy(y1,y2:Integer):Integer;
    Function  GetR(x1,x2:Integer):Integer;
    Procedure SetCaptionText(Value:String);
    Procedure SetText(Value:String);
    Procedure SetColor(Value:TColor);
    procedure DrawBorder;
    Procedure SetCaptionColor(Value:TColor);
    Procedure SetBorderShow(Value: Boolean);
    Procedure SetScaleFactor(Value: Single);

  protected
    { Protected declarations }
    Procedure Paint; override;
    Property  Canvas;

  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;

  published
    { Published declarations }
    Property Enabled;
    Property Hint;
    Property Left;
    Property Name;
    Property ShowHint;
    Property OnClick;
    Property OnDblClick;
    Property OnMouseDown;
    Property OnMouseMove;
    Property OnMouseUp;
    Property Top;
    Property Visible;

    property BevelOuter:TJSI_BevelOuter Read FFrameStyle Write SetBevelOuter;
    Property BorderShow: boolean Read FBorderShow Write SetBorderShow;
    Property Color:TColor Read FColor Write SetColor Default ClBtnFace;
    Property CaptionColor:TColor Read FCaptionColor Write SetCaptionColor Default ClBlack;
    Property Caption:String Read FCaptionText Write SetCaptionText;
    Property Max:Longint Read FMax Write SetMaxValue;
    Property Min:Longint Read FMin Write SetMinValue;
    Property NeedleWidth:Integer Read FNeedleWidth Write SetNeedleWidth Default 1;
    Property NeedleColor:TColor Read FNeedleColor Write SetNeedleColor Default ClRed;
    Property ProgressValue: single Read FProgress0 Write SetProgress;
    Property BackgroundColor:TColor Read FBackgroundColor Write SetBackgroundColor Default ClWhite;
    Property ScaleColor:TColor Read FScaleColor Write SetScaleColor Default ClBlack;
    Property Text:String Read FText Write SetText;
    Property TextColor:TColor Read FTextColor Write SetTextColor Default ClBlack;
    Property WarningColor:TColor Read FWarningColor Write SetWarningColor Default ClRed;
    Property WarningPoint:Single Read FWarningPoint Write SetWarningPoint;
    Property WarningWidth:Integer Read FWarningWidth Write SetWarningWidth Default 2;
    Property ScaleFactor:Single Read FScaleFactor Write SetScaleFactor;

  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('JSIComponents', [TJSIGauge]);
end;

constructor TJSIGauge.Create(AOwner: TComponent);
begin
     inherited Create(AOwner);
     FFrameStyle:= bv_Raised;
     FCaptionText:='';
     FCaptionColor:=clBlack;
     FText:='';
     FTextColor:=clBlack;
     FBackgroundColor:=clWhite;
     FColor:=clBtnFace;
     FBorderShow:=true;
     FScaleColor:=clBlack;
     FWarningColor:=clRed;
     FWarningPoint:=80;
     FWarningWidth:=1;
     FNeedleColor:=clRed;
     FNeedleWidth:=2;
     FProgress:=0;
     FProgress0:=0;
     FSectorDegree:=250;

     Height:=80;
     Width:=120;
//     Width:=120;
//     Height:=Round(Width/1.5);

     FOuterRadius:=Round(Width/2.8);
     FGaugeTop:=Round(Width/4.8);
     FGaugeLeft:=Round(Width/6.9);

     FGaugeWidth:=Height+10;
     FGaugeHeight:=Height+10;
     FMax:=100;
     FMin:=0;
     FScaleFactor:=1;
     FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
     Refresh;
end;

Procedure TJSIGauge.Paint;
begin
     inherited Paint;
     DrawCanvas;
end;

Procedure TJSIGauge.DrawCanvas;
begin
     DrawGauge(FGaugeLeft,FGaugeTop,FGaugeLeft+FGaugeWidth,FGaugeTop+FGaugeHeight,
     FOuterRadius,FSectorDegree,FWarningPoint);
     FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
     DrawNeedle(FNeedleDegree,FGaugeLeft,FGaugeTop,FGaugeLeft+FGaugeWidth,
     FGaugeTop+FGaugeHeight,FOuterRadius);
end;


Procedure TJSIGauge.SetCaptionText(Value:String);
begin
   if Value<>FCaptionText Then begin
        FCaptionText:=Value;
        Refresh;
   end;
end;

Procedure TJSIGauge.SetText(Value:String);
begin
   if Value<>FText Then begin
        FText:=Value;
        Refresh;
   end;
end;

Procedure TJSIGauge.SetNeedleWidth(Value:Integer);
begin
   if Value<>FNeedleWidth Then begin
        If Value<1 Then Value:=1;
        FNeedleWidth:=Value;
        Refresh;
   end;
end;

Procedure TJSIGauge.SetNeedleColor(Value:TColor);
begin
     IF Value<>FNeedleColor Then begin
        FNeedleColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIGauge.SetColor(Value:TColor);
begin
     IF Value<>FColor Then begin
        FColor:=Value;
	Refresh;
     end;
end;


Procedure TJSIGauge.SetBackgroundColor(Value:TColor);
begin
     IF Value<>FBackgroundColor Then begin
        FBackgroundColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIGauge.SetScaleColor(Value:TColor);
begin
     IF Value<>FScaleColor Then begin
        FScaleColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIGauge.SetTextColor(Value:TColor);
begin
     IF Value<>FTextColor Then begin
        FTextColor:=Value;
	Refresh;
     end;
end;


Procedure TJSIGauge.SetCaptionColor(Value:TColor);
begin
     IF Value<>FCaptionColor Then begin
        FCaptionColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIGauge.SetBorderShow(Value: boolean);
begin
    if (Value<>FBorderShow) Then
    Begin
       FBorderShow:=Value;
       Refresh
    end;
end;

Procedure TJSIGauge.SetWarningPoint(Value:Single);
begin
     if FWarningPoint<>Value Then Begin
        if Value > FMax then Value:=FMax;
        if Value < FMin then Value:=FMin;
        FWarningPoint:=Value;
        Refresh;
     end;
end;

Procedure TJSIGauge.SetWarningColor(Value:TColor);
begin
     IF Value<>FWarningColor Then begin
        FWarningColor:=Value;
	Refresh;
     end;
end;

Procedure TJSIGauge.SetWarningWidth(Value:Integer);
begin
   if Value<>FWarningWidth Then begin
        If Value<1 Then Value:=1;
        FWarningWidth:=Value;
        Refresh;
   end;
end;


Procedure TJSIGauge.SetProgress(Value:Single);
begin
    if Value <> FProgress0 Then
    Begin
       FProgress0:=Value;
       FProgress:=FProgress0/FScaleFactor;
       IF Value/FScaleFactor > FMax Then FProgress:=FMax;
       IF Value/FScaleFactor < FMin Then FProgress:=FMin;
       DrawNeedle(FNeedleDegree,FGaugeLeft,FGaugeTop,FGaugeLeft+FGaugeWidth,
       FGaugeTop+FGaugeHeight,FOuterRadius);
       FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
       DrawNeedle(FNeedleDegree,FGaugeLeft,FGaugeTop,FGaugeLeft+FGaugeWidth,
       FGaugeTop+FGaugeHeight,FOuterRadius);
    end;
end;

Procedure TJSIGauge.SetMinValue(Value:Longint);
begin
     IF (Value <> FMin) and (Value < FMax) Then
     Begin
        FProgress:=(FMax-Value)*(FProgress-FMin)/(FMax-FMin) + Value ;
        FMin:=Value;
        FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
        Refresh;
     End;
end;

Procedure TJSIGauge.SetMaxValue(Value:Longint);
begin
     IF (Value <> FMax) and (Value > FMin) Then
     Begin
      FProgress:=(Value-FMin)*(FProgress-FMin)/(FMax-FMin) + FMin;
      FWarningPoint:=(Value-FMin)*(FWarningPoint-FMin)/(FMax-FMin) + FMin;
      FMax:=Value;
      FNeedleDegree:=GetNeedleDegree(FMin,FMax,FProgress);
      Refresh;
      End;
end;

Procedure TJSIGauge.SetScaleFactor(Value:Single);
begin
   if (Value<>FScaleFactor) and (Value > 0) Then begin
        FScaleFactor:=Value;
        Refresh;
   end;
end;

Function TJSIGauge.DegToRad(inputDeg:Double):Double;
begin
     Result:=inputDeg*pi/180;
end;


procedure TJSIGauge.DrawBorder;
var   R, ARect:TRect;

begin
// R:=Rect(0,0,120,80);
     Height:=80;
     Width:=120;
 R:=Rect(0,0,Width,Height);
 with Canvas do
 begin
   Brush.style:= bsClear;
   with Pen do begin
     Style:= psSolid;
     Mode:=pmCopy;
   end;
   with R do rectangle(left+1, top+1, right-1, bottom-1);
    case fFrameStyle of
      bv_None: with R do begin
         Pen.Color:= Self.Color;
         rectangle(left, top, right, bottom);
         rectangle(left+1, top+1, right-1, bottom-1);
       end;
      bv_Raised : with R do begin
         Frame3D(Canvas, R, clBtnHighlight, clBtnShadow,1);
         Pen.Color:= Self.Color;
         rectangle(left, top, right, bottom);
        end;
      bv_Lowered: with R do begin
         Frame3D(Canvas, R, clBtnShadow, clBtnHighlight,1);
         Pen.Color:= Self.Color;
         rectangle(left, top, right, bottom);
        end;
      bv_Gutter : with R do begin
         Pen.Color := clBtnHighlight;
         rectangle(left + 1, top + 1, right, bottom);
         Pen.Color := clBtnShadow;
         rectangle(left, top, right - 1, bottom - 1);
        end;
      bv_BlackRect: with R do begin
         Pen.Color := clBlack;
         rectangle(left, top, right, bottom);
         Pen.Color:= Self.Color;
         rectangle(left+1, top+1, right-1, bottom-1);
        end;
      bv_BoldRect: with R do begin
         Pen.Color := clBlack;
         rectangle(left, top, right, bottom);
         rectangle(left+1, top+1, right-1, bottom-1);
        end;
      bv_FocusRect: begin
         Brush.Style:= bsSolid;  Brush.Color:= clWhite;
         Pen.Style  := psDot;    Pen.Color  := clBlack;
         with R do polyline([point(left,top),point(right-2,top),
                  point(right-2,bottom-1),point(left+1,bottom-1),point(left+1,top)]);
         with R do polyline([point(left+1,top+1),point(right-1,top+1),
                  point(right-1,bottom-2),point(left,bottom-2),point(left,top)]);
        end;
    end;

//    ARect := Rect(1, 1, 119, 79);
    ARect := Rect(1, 1, width-1, Height-1);
    Pen.Style:= psSolid;
    Pen.Mode:=pmCopy;
    Brush.Color := FColor;
    FillRect(ARect);

 end;
end;



Procedure TJSIGauge.DrawGauge (x1,y1,x2,y2,R,PieDeg:integer;WP:single);
var

  WSx,WSy,Sx,Sy,Ex,Ey:integer;
  Pnx1,Pny1,Pnx2,Pny2:integer;
  D,Idx:integer;
  P1Deg,P2Deg,PnDeg:integer;
  WDeg,SimePieDeg:integer;
  ARect, BRect: TRect;
  Tx, Ty: integer;
  Tic: single;
  S: string;
  left_b,top_b,right_b,bottom_b: integer;
  HText,WText,X, Y: integer;

begin

   DrawBorder;
//   left_b:=6;
//   top_b:=6;
   left_b:=4;
   top_b:=4;
   right_b:=Round(Width/1.03);
   bottom_b:=Round(Width/2.2);

   with Canvas do
   begin
     Brush.style:= bsClear;
   with Pen do begin
     Style:= psSolid;
     Mode:=pmCopy;
   end;
    ARect:=Rect(left_b,top_b,right_b,bottom_b);
    Frame3D(Canvas, ARect, clBtnShadow, clBtnHighlight,1);
    ARect:=Rect(left_b+1,top_b+1,right_b-1,bottom_b-1);
    Frame3D(Canvas, ARect, cl3DDkShadow, cl3DLight,1);

    ARect := Rect(left_b+2,top_b+2, right_b-2, bottom_b-2);
    Brush.Color := FBackgroundColor;
    FillRect(ARect);

//Draw sector
      D:=GetR(x1,x2)-R;
      SimePieDeg:=PieDeg Div 2;

      Sx:=GetArcPointX(-SimePieDeg,x1,x2,R);
      Sy:=GetArcPointY(-SimePieDeg,y1,y2,R);
      Ex:=GetArcPointX(SimePieDeg,x1,x2,R);
      Ey:=GetArcPointY(SimePieDeg,y1,y2,R);
      Pen.Color:=FScaleColor;
      Pen.Width:=1;
      Arc(x1+D,y1+D,x2-D,Y2-D,Sx,Sy,Ex,Ey);

//Draw warning sector
      WDeg:=SimePieDeg+Round((WP-FMin)*(360-PieDeg)) Div (FMax-FMin);
      WSx:=GetArcPointX(WDeg,x1,x2,R);
      WSy:=GetArcPointY(WDeg,y1,y2,R);

      Pen.Color:=FWarningColor;
      Pen.Width:=FWarningWidth;
      Arc(x1+D,y1+D,x2-D,Y2-D,Sx,Sy,WSx,WSy);

//Draw tics
      Pen.Width:=1;
      For Idx:=0 To 10 do begin
            PnDeg:=SimePieDeg+Idx*10*(360-PieDeg) Div 100;
            Pnx1:=GetArcPointX(PnDeg,x1,x2,R);
            Pny1:=GetArcPointY(PnDeg,y1,y2,R);
            if Idx Mod 2 = 1 then begin
//minor
               Pnx2:=GetArcPointX(PnDeg,x1,x2,R+5);
               Pny2:=GetArcPointY(PnDeg,y1,y2,R+5);
            end
            else begin
//major
               Pnx2:=GetArcPointX(PnDeg,x1,x2,R+8);
               Pny2:=GetArcPointY(PnDeg,y1,y2,R+8);
               Font := Self.Font;
               if PnDeg>=WDeg then
                  Font.Color := FWarningColor
               else
                  Font.Color := FTextColor;

               Brush.Color := Self.Color;
//               Brush.Color := clSilver;
               Brush.Style := bsClear;
               Font.Style:=Self.Font.Style;

               BRect := Rect(Pnx2-10, Pny2-11, Pnx2+10, Pny2);
               Tic:=FMin+(FMax-FMin)/10*Idx;
               If FMax < 5 then
                 S:=FloatToStrF(Tic,ffFixed, 7, 1)
               else
                 S:=FloatToStrF(Tic,ffFixed, 7, 0);
               X := Pnx2-10+(21 - TextWidth(S)) div 2;
               Y := Pny2-11+(12 - TextHeight(S)) div 2;
               TextRect(BRect, X+1, Y, S);
            end;
            if PnDeg>=WDeg then
               Pen.Color := FWarningColor
             else
               Pen.Color:=FScaleColor;

            MoveTo(Pnx1,Pny1);
            LineTo(Pnx2,Pny2);
      end;

//Draw text
      Font.Style:=[fsBold];
      Font.Color:=FTextColor;
      X:=(left_b + right_b) div 2;
//      Y:=(top_b + bottom_b) div 2+23;
      Y:=(top_b + bottom_b) div 2+16;
      BRect := Rect(X-35, Y-7, X+35, Y+7);

      if FScaleFactor<0.00999999 then  S:='x'+FloatToStrF(FScaleFactor, ffFixed,5, 3)+' '+FText
       else if FScaleFactor<0.09999999 then  S:='x'+FloatToStrF(FScaleFactor, ffFixed,4, 2)+' '+FText
        else if FScaleFactor<0.99999999 then  S:='x'+FloatToStrF(FScaleFactor, ffFixed,3, 1)+' '+FText
         else S:='x'+IntToStr(Round(FScaleFactor))+' '+FText;

      X := X-35+(71 - TextWidth(S)) div 2;
      Y := Y-7+(15 - TextHeight(S)) div 2;
      TextRect(BRect, X, Y, S);

      Brush.style:= bsClear;
      if FBorderShow then
      begin
       Pen.Width:=1;
       Pen.Mode:=pmCopy;
       Pen.Color:= clBtnHighlight;
//       Rectangle(3, 3, 167, 101);
       Rectangle(3, 3, Width-2, Height-9);
       Pen.Color:= clBtnShadow;
//       Rectangle(2, 2, 166, 100);
       Rectangle(2, 2, Width-3, Height-10);
      end;

//Draw caption
      Brush.Color := Self.Color;
//      Font.Style:=[fsBold];
      Font.Color:=FCaptionColor;
      X:=(left_b + right_b) div 2;
//      Y:=(top_b + bottom_b) div 2+62;
      Y:=(top_b + bottom_b) div 2+34;
      WText:=TextWidth(FCaptionText) div 2;
      HText:=TextHeight(FCaptionText) div 2;
      BRect := Rect(X-WText, Y-HText, X+WText, Y+HText);
      TextRect(BRect, X-WText, Y-HText, FCaptionText);
  end;
end;


Procedure TJSIGauge.DrawNeedle(Deg,x1,y1,x2,y2,R:integer);
var
   rx,ry:integer;
   x,y:integer;
   dx:integer;
begin
  with Canvas Do begin
      rx:=(x1+x2) div 2;
      x:=GetArcPointX(Deg,x1,x2,R+4);
      y:=GetArcPointY(Deg,y1,y2,R+4);
//      dx:=Integer(Round((95-66)/tan(DegToRad(Deg-90))));
      dx:=Integer(Round((Width/1.78-Width/2.25)/tan(DegToRad(Deg-90))));
      Pen.Color:=FNeedleColor xor FBackgroundColor;
      Pen.Mode:=pmxor;
      Pen.Width:=FNeedleWidth;
  //    MoveTo(rx-dx,66);
      MoveTo(rx-dx,Round(Width/2.25));
      LineTo(x,y);
      Pen.Mode:=pmCopy;
  end;
end;


Function TJSIGauge.GetR(x1,x2:integer):integer;
begin
     Result:=Abs(x2-x1) div 2;
end;

Function TJSIGauge.GetArcPointX(Deg:double;x1,x2,d:integer):integer;
var
   tmpint:integer;
begin
     tmpint:=Round(d*Sin(DegToRad(Deg)));
     Result:=GetRx(x1,x2)-tmpint;
end;

Function TJSIGauge.GetArcPointY(Deg:double;y1,y2,d:integer):integer;
var
   tmpint:integer;
begin
      tmpint:=integer(Round(d*cos(DegToRad(Deg))));
      Result:=GetRy(y1,y2)+tmpint;
end;

Function TJSIGauge.GetRx(x1,x2:integer):Integer;
begin
     Result:=x1+(x2-x1) div 2;
end;

Function TJSIGauge.GetRy(y1,y2:integer):Integer;
begin
     Result:=y1+(y2-y1) div 2;
end;

Function  TJSIGauge.GetNeedleDegree(Min,Max:LongInt; FProgress: single):Integer;
begin
    try
     Result:= Integer(Round((((360-FSectorDegree)*(FProgress-FMin))/(FMax-FMin))+(FSectorDegree/2)));
    except
    end;
end;

procedure TJSIGauge.SetBevelOuter(const Value:TJSI_BevelOuter);
begin
  if Value<> FFrameStyle then  begin
    FFrameStyle := Value;
    Refresh;
  end;
end;


end.







