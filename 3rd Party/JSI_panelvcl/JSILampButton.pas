unit JSILampButton;

////////////////////////////////////////////////////////
// TJSILampButton - Panel Button with Lamp v1.1       //
//                                                    //
//   Copyright (C) 2005 Stefan Ion                    //
//   J.S.Industries S.R.L.                            //
//   Software Development, Embedded Controllers,      //
//   Automation, Custom Design and Prototyping        //
//   http://www.jsindustries.ro                       //
//   office@jsindustries.ro                           //
//   ROMANIA                                          //
//                                                    //
//   License:                                         //
//   This source code is distributed as a freeware.   //
//   You may use and modify this source code for your //
//   freeware products.                               //
//   All functions, procedures and classes may        //
//   NOT BE USED IN COMMERCIAL PRODUCTS               //
//   without  the permission of the author.           //
//                                                    //
//   Disclaimer of warranty:                          //
//   "This software is supplied as is. The author     //
//   disclaims all warranties, expressed or implied,  //
//   including, without limitation, the warranties    //
//   of merchantability and of fitness for any        //
//   purpose. The author assumes no liability for     //
//   damages, direct or consequential, which may      //
//   result from the use of this software."           //
//                                                    //
//   Please report bugs to:                           //
//   Stefan Ion                                       //
//   office@jsindustries.ro                           //
//                                                    //
//   Please contact use for your suggestions or       //
//   projects proposal                                //
////////////////////////////////////////////////////////

interface

uses
   Windows, Classes, Graphics, Controls, Buttons, SysUtils, Extctrls;

type
  TJSILampButtonThread= class(TThread)

   public
      iSpeed: integer;
      FNotify: TNotifyEvent;
      constructor Create(DoNotify: TNotifyEvent);
      procedure Execute; override;
      procedure DoEvent;
   end;

  TJSILampButton = class(TCustomControl)
  private
    FCaption         : TCaption;
    FSpeed           : integer;
    FThread          : TJSILampButtonThread;
    FTrueColor       : TColor;
    FFalseColor      : TColor;
    FState           : Boolean;
    FFlash           : Boolean;
    FButtonState     : TButtonState;
    FDown            : Boolean;
    Lamp              : TRect;
    bFlag            : boolean;
    DoClickArea: Boolean;

  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Click; override;

  protected
    procedure SetTrueColor(Value:TColor);
    procedure SetFalseColor(Value:TColor);
    procedure SetState(Value:Boolean);
    procedure SetFlash(Value:Boolean);
    procedure SetCaption(Value: TCaption);
    procedure SetDown(Value:Boolean);
    procedure Paint; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Redraw(AOwner:TObject);
    procedure DoChange(Sender: TObject);
    procedure PreThread;

  published
    Property State:Boolean read fState write SetState default false;
    Property Flash:Boolean read fFlash write SetFlash default false;
    property TrueColor:TColor read fTrueColor write SetTrueColor;
    property FalseColor:TColor read fFalseColor write SetFalseColor;
    property Speed: integer read FSpeed write FSpeed;
    property Caption: TCaption read FCaption write SetCaption;
    Property Down:Boolean read fDown write SetDown;

    property Hint;
    property ShowHint;
    property Color;
    Property Enabled;
    property Font;
    property onClick;
    property onMouseMove;
    property onMouseDown;
    Property onMouseUp;
    property onEnter;
    property onExit;
  end;

procedure Register;

implementation

procedure Register;
begin
   RegisterComponents('JSIComponents', [TJSILampButton]);
end;

{##############################################################################}

constructor TJSILampButtonThread.Create(DoNotify: TNotifyEvent);
begin
   inherited Create(true);
   FreeOnTerminate := true;
   FNotify := DoNotify;
end;

procedure TJSILampButtonThread.Execute;
begin
//Lamp flash timer
   while not Terminated do
   begin
      SleepEx(iSpeed, false);
      Synchronize(DoEvent);
   end;
end;

procedure TJSILampButtonThread.DoEvent;
begin
   FNotify(Self);
end;

{##############################################################################}

constructor TJSILampButton.Create(aOwner: TComponent);
begin
   inherited Create(aOwner);
   Down:=false;//indica cum ramane: jos sau sus
   State:=False;//indica starea curenta: on sau off
   Flash:=False;
   TrueColor:=clRed;
   FalseColor:=clGray;
   Caption:=Name;
//   Font.onChange:=Redraw;
   DragMode:=dmManual;
   Speed := 500;
   Height := 40;
   Width := 90;
   FThread := TJSILampButtonThread.Create(DoChange);
   Repaint;
end;

destructor TJSILampButton.Destroy;
begin
   FThread.Terminate;
   inherited;
end;

procedure TJSILampButton.SetTrueColor(Value:TColor);
begin
   if fTrueColor <> Value then
      begin
         fTrueColor:=Value;
         Repaint;
      end;
end;

procedure TJSILampButton.SetFalseColor(Value:TColor);
begin
   if fFalseColor <> Value then
      begin
         fFalseColor:=Value;
         Repaint;
      end;
end;
procedure TJSILampButton.Redraw(AOwner:Tobject);
begin
  Paint;
end;

procedure TJSILampButton.SetState(Value:Boolean);
begin
   if fState <> Value then
      begin
         fState:=Value;
         if fState then//daca este on si jos atunci traseaza jos
            if fDown then
               fButtonState := bsDown
            else
               fButtonState := bsUp;
         PreThread;
         Repaint;
      end;
end;

procedure TJSILampButton.SetDown(Value:Boolean);
begin
   if fDown <> Value then
      begin
         fDown:=Value;
         fState:=fDown;//state urmareste down si nu invers
         if fDown then
            fButtonState := bsDown
         else
            fButtonState := bsUp;
         PreThread;
         Repaint;
      end;
end;

procedure TJSILampButton.SetFlash(Value:Boolean);
begin
   if fFlash <> Value then
      begin
         fFlash:=Value;
         PreThread;
         Repaint;
      end;
end;

procedure TJSILampButton.SetCaption(Value: TCaption);
begin
  if Value='' then FCaption:=Name;
  if AnsiCompareStr(Value, FCaption) <> 0 then FCaption := Value;
  Repaint;
end;

procedure TJSILampButton.PreThread;
begin
//click event
  FThread.iSpeed := FSpeed;
  if Flash then
    if fState then
      FThread.Resume //resume Lamp flash
    else
      FThread.Suspend; //suspend Lamp flash
end;

procedure TJSILampButton.Click;
begin
//click event
//  if DoClickArea then begin
    fState:= not fState;//schimba flagul de stare
    if fDown then
     if not fState then
        fButtonState := bsUp
     else
        fButtonState := bsDown;

    PreThread;
    Repaint;

//  end;
  inherited Click;
end;

procedure TJSILampButton.Paint;
var
  Margin,Margin1,ARect,Area, CaptArea:TRect;
begin
     inherited Paint;
     if Caption ='' then Caption:=Name;
     if not Enabled and not (csDesigning in ComponentState) then
        if fButtonState = bsDisabled then fButtonState := bsUp;
//draw button

     with Canvas do begin
       Brush.Color:=clBtnFace;//culoare limite si buton
       with Pen do begin
            Style:= psSolid;
            Mode:=pmCopy;
       end;
       Margin:=Rect(0,0,Width,Height);
       FillRect(Margin);//
       Frame3D(Canvas, Margin, cl3DLight,  cl3DDkShadow,2);//traseaza limitele butonului (partea fiza)
       Margin1:=Rect(8,0,width-8,height);
       Frame3D(Canvas, Margin1, cl3DDkShadow, cl3DLight,2);//trareaza zona buttonului

       Area:=DrawButtonFace(Canvas,Rect(10,0,Width-10,Height),2,bsNew,false,fButtonState in [bsDown],false);
//       FillRect(Area);//

//Lamp area
       Lamp:=Rect(Area.left+2, Area.top+2, Area.Right-2, Area.bottom-2);//zona lampa
       Frame3D(Canvas, Lamp, cl3DDkShadow, cl3DLight,1);
       Pen.Color:= Self.Color;

//first color
       Font.Assign(Self.Font);
       if fState then begin
             Brush.Color:=fTrueColor;
             Font.Color:=cl3DLight;
       end else begin
             Brush.Color:=fFalseColor;
             Font.Color:=cl3DDkShadow;
//             Font.Color:=fEnabledColor;
       end;
       FillRect(Lamp);

//draw caption text
       CaptArea:=Rect(Lamp.left+1, Lamp.Top+3, Lamp.Right-1, Lamp.Bottom-3);
       if Caption <> '' then
          DrawText(handle,pchar(Caption),length(Caption),CaptArea,DT_Center or DT_WordBreak);

     end;

end;

procedure TJSILampButton.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  DoClickArea := (X >= 10) and (X < ClientWidth-10) and (Y >= 0) and (Y <= ClientHeight);
//  if DoClickArea then
    if (Button = mbLeft) and Enabled then begin
       fButtonState := bsDown;
       Repaint;
    end;
 inherited MouseDown(Button, Shift, X, Y);
end;

procedure TJSILampButton.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewState: TButtonState;
begin
   DoClickArea := (X >= 10) and (X < ClientWidth-10) and (Y >= 0) and (Y <= ClientHeight);
{
   if DoClickArea then
    if (shift =[ssLeft]) then begin
       NewState := bsdown
     else
       NewState := bsUp;
    if NewState <> fButtonState then
    begin
      fButtonState := NewState;
      Repaint;
    end;
  end;
}

  inherited MouseMove(Shift, X, Y);
end;

procedure TJSILampButton.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  DoClickArea := (X >= 10) and (X < ClientWidth-10) and (Y >= 0) and (Y <= ClientHeight);
//  if DoClickArea then
     if not fDown then begin
       fButtonState := bsUp;
       Repaint;
     end;
  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TJSILampButton.DoChange(Sender: TObject);
var CaptArea:TRect;
begin
//   if DoClickArea then
   with canvas do  begin

       Pen.Color:= Self.Color;
//first color
       Font.Assign(Self.Font);
       if fState then begin
             Brush.Color:=fTrueColor;
             Font.Color:=cl3DLight;
       end else begin
             Brush.Color:=fFalseColor;
             Font.Color:=cl3DDkShadow;
       end;
       FillRect(Lamp);

//draw caption text
       CaptArea:=Rect(Lamp.left+1, Lamp.Top+3, Lamp.Right-1, Lamp.Bottom-3);
       if Caption <> '' then
          DrawText(handle,pchar(Caption),length(Caption),CaptArea,DT_Center or DT_WordBreak);

   end;
end;

end.
