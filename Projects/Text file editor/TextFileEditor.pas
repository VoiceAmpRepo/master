unit TextFileEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Label1: TLabel;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}



procedure TForm1.Button1Click(Sender: TObject);
var
  InTextFile: TextFile;
  OutTextFile: TextFile;
  buffer: string;
  s: string;
  i: Integer;
  cnt : Integer;
begin
  memo1.Lines.Clear;
  i := 0;
  cnt := 0;
  AssignFile(InTextFile, '609_8kHz.txt');
  Reset(InTextFile);
  while not ( (EOF(InTextFile)) or (cnt>strtoint(edit1.text))) do
  begin
   cnt := cnt+1;
   ReadLn(InTextFile, buffer);
   if i<7 then
   begin
     i := i+1;
     s := s + buffer + ',' + #9;
   end
   else
   begin
     memo1.Lines.add(s);
     i := 0;
     s := '';
   end;
  end;
  Label1.Caption := IntToStr(cnt);
  CloseFile(InTextFile) ;

end;

end.
