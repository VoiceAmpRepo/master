unit Feedback;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  Tfrm_Feedback = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Feedback: Tfrm_Feedback;

implementation

{$R *.dfm}

{==========================================================================
 Procedure: Button1Click
 Inputs:    None
 Ouputs:    None
 About:     Close the window
 ==========================================================================}
procedure Tfrm_Feedback.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
