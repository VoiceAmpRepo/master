object frm_Message: Tfrm_Message
  Left = 427
  Top = 213
  BorderStyle = bsNone
  Caption = 'frm_Message'
  ClientHeight = 96
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 256
    Height = 96
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 0
    object pnl_Heading: TPanel
      Left = 2
      Top = 2
      Width = 252
      Height = 31
      Align = alTop
      BevelOuter = bvNone
      Caption = 'pnl_Heading'
      Color = clCream
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object pnl_Main: TPanel
      Left = 2
      Top = 33
      Width = 252
      Height = 61
      Align = alClient
      BevelOuter = bvNone
      Caption = 'pnl_Main'
      Color = clCream
      TabOrder = 1
    end
  end
end
