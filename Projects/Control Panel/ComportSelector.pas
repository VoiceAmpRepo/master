unit ComportSelector;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CPortCtl, Registry;

type
  TComportSelection = class(TForm)
    ComComboBox1: TComComboBox;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ComportSelection: TComportSelection;

implementation

procedure TComportSelection.Button1Click(Sender: TObject);
 var
 reg:  TRegistry;
 I:Integer;
begin
   Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('HARDWARE\DEVICEMAP\SERIALCOMM', false) then
    begin
      Reg.GetValueNames(ComComboBox1.Items);
      for I := 0 to ComComboBox1.Items.Count - 1 do
          ComComboBox1.Items[i] := Reg.ReadString(ComComboBox1.Items[i]);
    end;
//    ComComboBox1.Sorted := true;
  finally
    Reg.Free;
  end;
end;

{$R *.dfm}

end.
