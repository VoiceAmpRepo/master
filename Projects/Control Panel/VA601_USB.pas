{==========================================================================
 Project:   VA601 FT232BM USB DLL driver interface test
 Author:    Andr� Hoek
 Copyright: Ukwakha Consulting cc

 Revision history:
  Rev         Date          Comments
  ---         ----          --------
  A001        28 Oct 2005   Initial Release - from scratch - previous lost! f#@$$
  A002                      Added 'connected' status - verify for specific VA601
  A003                      Tx implemented - note 300ms Tx delay!

  A004        19 Nov 2005   Intermittend port failure fixed.
                            Procedure is as follows:
                            Pre-load default settings into variables (e.g. FT_Current_Baud)
                            OPEN the port
                            Send variables to port (e.g. Set_USB_Device_BaudRate)

  A005        20 Nov 2005   Modified so that device auto re-connects when
                            removed and plugged in again.
                            (via DevicePresent flag on timer event)

  A006        20 Nov 2005   Audio 'glitch' during data transfer fixed:
                            Baud rate increased to 115200
                            Tx interrupt on DSP changed to level 7
                            Busy flags added on both processes

  A007        21 Nov 2005   Added new slider and knob components:
                            Abacus components expired!

 *** BUG: Usb does not re-connect properly when unplugged while app open***

  A008        22 Nov 2005   Added Save/Open ini files
                            Saves three setting BANKS in .ini file
                            Automatically loads settings when file opens

  A009        22 Nov 2005   Added 'CheckPrevious.pas'
                            Only allows one instance of software to run
                            Prevents multiple apps acessing same USB handle

  A010        24 Nov 2005   Save dialogue extension issue fixed
                            Bank update command implemented:
                            Sends the currently selected bank values
                            to the VA601 EEPROM

  A011        02 Des 2005   Added Audio socket switching

  A012        05 Des 2005   EEPROM update:
                            Device description: VA601 Stutter Inhibitor
                            Display serial number when connected

  1.00        05 Des 2005   Initial release

  1.01        07 Des 2005   Changed all gain sliders to show 0-100% scale
                            Noise gate scale changed to 0-100%

  1.02        08 Des 2005   USB device name changed to "VoiceAmp 601"

  *** BUG: On Selecting the same bank in the combobox twice,
                slider values are incorrect  ***

  1.03        25 Jan 2006   Slider update bug fixed:
                            Bank update routines did not account for
                            inverse offsett of sliders.
                            Initial values were loaded incorrectly

  1.04        26 Jan 2006   'MODE' changed to 'PROGRAM'
                            Production release version: v4.0A

  1.05        16 Feb 2006   Added application version information
                            MahFileVer unit added

  --- Standard Windows Application version format used from now on ---

  4.1.0.0     17 Feb 2006   Added hints for all controls
                            Added hint enable/disable in Help menu

  TODO:
  [*] 'Get Settings' function lost during power down - re do
  [*] 'Get usage' lost during power down - re do
  [*] 'Save settings' prompt at app close or program change
  [*] Update ini file to save unit serial number
  [*] Update ini file to save unit usage counter
  [*] Update ini file to save firmware revision
  [*] Update ini file to save date

  4.1.0.1     06 Mar 2006   -> Added 'Split' function to parse strings
                            -> Added MinutesToDaysHoursMinutes function
                            -> Added 'Get Usage' function and Edit menu
                            -> Added 'Get Settings' function
                            -> Added information dialog (Feedback) function
                            -> USB Write timeout changed from 300 to 100ms

  4.1.0.2     08 Mar 2006   -> Added Delay function
                            -> Added 'Get Unit Data' interface
                               Custom box to read Manf date, rev and usage
                            -> Updated feedback function to allow timer disable
                            -> Added catch for 'Save settings' on exit and
                               'Save Program' to unit
                            -> .ini file save updated to save unit information
                            -> 'Send Settings' confirmation window added

  13 Mar 2006:
  TODO:
  [*] During 'Send Settings', replace the word 'BANK' with 'PROGRAM'
  [*] '*.ini' file, change 'BANK' to 'PROGRAM'
  [*] Do not display last digit in revision number (Main app window)
  [*] When changing from PROGRAM2 to 3, timer is too fast
  [*] When saving a file, change prompt to: 'Updating data'
  [*] When a Program wasn't sent to device, prompt: 'Previous program not sent to device'

  4.1.0.3     13 Mar 2006   -> Added 'Wait' function
                               Uses system Time to accurately count out seconds

  4.2.0.0     16 Nov 2006   -> Updated for FTDI CDM driver.
                               Encapsulated in FT232Comms.pas
 ==========================================================================}

unit VA601_USB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, janTracker, ComCtrls, 
  PDJRotoLabel,  Menus, JSILed,
  JSIButtonText, JSIButton, ExTrackBar, ToolWin, ActnMan, ActnCtrls,
  ActnMenus, ImgList, ActnList, StdActns, XPStyleActnCtrls;
  // mKnob, Knob, XiTrackBar, 

type
  TfrmMain = class(TForm)
    tmr_Connected: TTimer;
    tmrComDelay: TTimer;
    Panel1: TPanel;
    PDJRotoLabel1: TPDJRotoLabel;
    StatusBar1: TStatusBar;
    ComboBox1: TComboBox;
    Label8: TLabel;
    Led1: TJSILed;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Button1: TButton;
    Button2: TButton;
    GroupBox1: TGroupBox;
    tb_Delay: TExTrackBar;
    tb_Pitch: TExTrackBar;
    tb_Ambient: TExTrackBar;
    tb_Effect: TExTrackBar;
    tb_Master: TExTrackBar;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    pnlDelay: TPanel;
    pnlPitch: TPanel;
    pnlAmbient: TPanel;
    pnlEffect: TPanel;
    pnlMaster: TPanel;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label1: TLabel;
    pnlThreshold: TPanel;
    pnlRelease: TPanel;
    tb_Threshold: TExTrackBar;
    tb_Release: TExTrackBar;
    OpenDialog1: TOpenDialog;
    ActionManager1: TActionManager;
    FileExit1: TFileExit;
    act_About: TAction;
    ImageList1: TImageList;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    ActionMainMenuBar1: TActionMainMenuBar;
    SaveDialog1: TSaveDialog;
    RadioGroup1: TRadioGroup;
    pnl_Serial: TPanel;
    Label2: TLabel;
    act_Hints: TAction;
    act_GetDeviceData: TAction;
    Button3: TButton;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure tmr_ConnectedTimer(Sender: TObject);
    procedure tmrComDelayTimer(Sender: TObject);
    procedure tb_DelayChange(Sender: TObject);
    procedure tb_PitchChange(Sender: TObject);
    procedure tb_AmbientChange(Sender: TObject);
    procedure tb_EffectChange(Sender: TObject);
    procedure tb_MasterChange(Sender: TObject);
    procedure tb_ThresholdChange(Sender: TObject);
    procedure tb_ReleaseChange(Sender: TObject);
    procedure ComboBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ComboBox1Select(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure act_AboutExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure act_HintsExecute(Sender: TObject);
    procedure act_GetDeviceDataExecute(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);


  private
    { Private declarations }
    function  Split(Chaine: string; Casseur: string; Retour: TStrings): integer;
    function  MinutesToDaysHoursMinutes(AMinutes: Integer): string;
    procedure Feedback(head: string; msg: string; time: Integer; UseTmr: Boolean);    procedure Delay(dwMilliseconds: Longint);
    procedure Display_Defaults();

    procedure addTicks();              // Manually paint ticks for large values
    procedure showValues();
    procedure saveSettings();
    procedure getSettings();
    procedure updateBank1();
    procedure updateBank2();
    procedure updateBank3();
    procedure ControlsEnable(state: Boolean);
    procedure loadHints();
    procedure Wait(seconds: Integer);

  public
    { Public declarations }
     Version: String;
  end;

var
  frmMain: TfrmMain;
  const Colors: array[0..2] of TColor = (clGreen, $000289D2, clRed);


implementation

uses D2XXUnit, INIfiles, About, MahFileVer, StrUtils, Message, Feedback,
     FT232Comms, DateUtils;

var
 DevicePresent : Boolean;                   // To check availability
 DeviceBusy : Boolean;                      // Flag for comms busy
 Device_Description : String = 'VoiceAmp 601';
 CurrentBank: Integer;                      // Current effects bank
 curFileName: String;                       // Current filename

 // Save flags
 ProgramChanged: Boolean;                   // Flag for saving Programs
 FileChanged: Boolean;
 StartUp: Boolean;                          // Disable warnings during startup

 // Settings storage
 curDelay_Bank1: Integer;       // BANK 1
 curPitch_Bank1: Integer;
 curAmbient_Bank1: Integer;
 curEffect_Bank1: Integer;
 curMaster_Bank1: Integer;
 curThreshold_Bank1: Integer;
 curRelease_Bank1: Integer;
 curSocket_Bank1: Integer;

 curDelay_Bank2: Integer;       // BANK 2
 curPitch_Bank2: Integer;
 curAmbient_Bank2: Integer;
 curEffect_Bank2: Integer;
 curMaster_Bank2: Integer;
 curThreshold_Bank2: Integer;
 curRelease_Bank2: Integer;
 curSocket_Bank2: Integer;

 curDelay_Bank3: Integer;       // BANK 3
 curPitch_Bank3: Integer;
 curAmbient_Bank3: Integer;
 curEffect_Bank3: Integer;
 curMaster_Bank3: Integer;
 curThreshold_Bank3: Integer;
 curRelease_Bank3: Integer;
 curSocket_Bank3: Integer;

 // Current effects values
 curDelay: Integer;
 curPitch: Integer;
 curAmbient: Integer;
 curEffect: Integer;
 curMaster: Integer;
 curThreshold: Integer;
 curRelease: Integer;
 curSocket: Integer;

 // Unit data
 curSerNum: String;     // Unit serial number
 curRev:  String;       // Unit firmware revision
 curRevDate: String;    // Unit firmware revision date
 curUsage: String;      // Unit usage counter

{$R *.dfm}



{==========================================================================
 Procedure: loadHints
 Inputs:    None
 Ouputs:    None
 About:     Load help hint text for controls
 ==========================================================================}
procedure TfrmMain.loadHints;
begin
  application.HintPause := 250;       // 250ms before hints are shown
  application.HintHidePause := 10000;  // Show hints for 10sec

  with tb_Delay do
  begin
    Hint := 'Adjust the feedback delay' + #13#10 +
            'between 0-250 milliseconds';
    ShowHint := True;
  end;
  with tb_Pitch do
  begin
    Hint := 'Adjust the pitch up or down' + #13#10 +
            'This shifts normal speech up'+ #13#10 +
            '     or down in Hertz';
    ShowHint := True;
  end;
  with tb_Ambient do
  begin
    Hint := ' Ambient volume level allows' + #13#10 +
            'the occlusion of the earpiece'+ #13#10 +
            '          to be offset'+ #13#10 +
            '  (unmodified sound level)';
    ShowHint := True;
  end;
  with tb_Effect do
  begin
    Hint := ' Effect is the volume level' + #13#10 +
            ' of the modified sound i.e.'+ #13#10 +
            'the level of the delayed and '+ #13#10 +
            '  frequency shifted speech'+ #13#10 +
            '       (Effect volume)';
    ShowHint := True;
  end;
  with tb_Master do
  begin
    Hint := 'Master is the overall volume control' + #13#10 +
            'and represents the same setting as'+ #13#10 +
            '  the device keypad + and - keys';
    ShowHint := True;
  end;
  with tb_Threshold do
  begin
    Hint := '   Threshold sets the speech' + #13#10 +
            'trigger level for the background'+ #13#10 +
            '        noise suppression';
    ShowHint := True;
  end;
  with tb_Release do
  begin
    Hint := ' Release is the time the noise' + #13#10 +
            'suppression system is kept active'+ #13#10 +
            '   after speech has stopped';
    ShowHint := True;
  end;
  with RadioGroup1 do
  begin
    Hint := 'This radio button selects' + #13#10 +
            '    the upper or lower'+ #13#10 +
            '       audio socket';
    ShowHint := True;
  end;
  with ComboBox1 do
  begin
    Hint := 'This program selector' + #13#10 +
            ' allows one of three'+ #13#10 +
            'individual programs'+ #13#10 +
            '   to be created';
    ShowHint := True;
  end;
  with Button2 do
  begin
    Hint := 'Send Settings sends all the settings' + #13#10 +
            '  on the screen to the VA601 unit'+ #13#10 +
            ' Load defaults resets the settings';
    ShowHint := True;
  end;
  with Button3 do
  begin
    Hint := '  Loads current VA601 device settings' + #13#10 +
            'from the device and displays the values'+ #13#10 +
            '        on the screen controls';
    ShowHint := True;
  end;
  with Led1 do
  begin
    Hint := 'Connection Status';
    ShowHint := True;
  end;
  with pnl_Serial do
  begin
    Hint := ' Serial no of device is' + #13#10 +
            'displayed when connected';
    ShowHint := True;
  end;
  with pnlDelay do
  begin
    Hint := 'Delay level';
    ShowHint := True;
  end;
  with pnlPitch do
  begin
    Hint := 'Pitch level';
    ShowHint := True;
  end;
  with pnlAmbient do
  begin
    Hint := 'Ambient level';
    ShowHint := True;
  end;
  with pnlEffect do
  begin
    Hint := 'Effect level';
    ShowHint := True;
  end;
  with pnlMaster do
  begin
    Hint := 'Master level';
    ShowHint := True;
  end;
  with pnlThreshold do
  begin
    Hint := 'Threshold level';
    ShowHint := True;
  end;
  with pnlRelease do
  begin
    Hint := 'Release level';
    ShowHint := True;
  end;
end;



{==========================================================================
 Procedure: Display_Defaults
 Inputs:    None
 Ouputs:    None
 About:     Display default values
 ==========================================================================}
procedure TfrmMain.Display_Defaults;
begin

 curDelay_Bank1 := 50;
 curPitch_Bank1 := 250;
 curAmbient_Bank1 := 10900;
 curEffect_Bank1 := 10900;
 curMaster_Bank1 := 21800;
 curThreshold_Bank1 := 0;
 curRelease_Bank1 := 1667;
 curSocket_Bank1 := 0;

 curDelay_Bank2 := 90;
 curPitch_Bank2 := 530;
 curAmbient_Bank2 := 10900;
 curEffect_Bank2 := 10900;
 curMaster_Bank2 := 21800;
 curThreshold_Bank2 := 1200;
 curRelease_Bank2 := 1667;
 curSocket_Bank2 := 0;

 curDelay_Bank3 := 120;
 curPitch_Bank3 := 650;
 curAmbient_Bank3 := 8278;
 curEffect_Bank3 := 16350;
 curMaster_Bank3 := 24525;
 curThreshold_Bank3 := 2400;
 curRelease_Bank3 := 5000;
 curSocket_Bank3 := 0;

 StatusBar1.Panels.Items[2].Text := 'Defaults Loaded';
 ComboBox1.OnSelect(Self);
end;



{==========================================================================
 Procedure: updateBank3
 Inputs:    None
 Ouputs:    None
 About:     Update BANK 3 controls
 ==========================================================================}
procedure TfrmMain.updateBank3;
begin
  tb_Delay.Position := 236-curDelay_Bank3;
  tb_Pitch.Position := 1500-curPitch_Bank3;
  tb_Ambient.Position := 32700-curAmbient_Bank3;
  tb_Effect.Position := 32700-curEffect_Bank3;
  tb_Master.Position := 32700-curMaster_Bank3;
  tb_Threshold.Position := 6000-curThreshold_Bank3;
  tb_Release.Position := 10000-curRelease_Bank3;
  RadioGroup1.ItemIndex := curSocket_Bank3;
end;



{==========================================================================
 Procedure: updateBank2
 Inputs:    None
 Ouputs:    None
 About:     Update BANK 2 controls
 ==========================================================================}
procedure TfrmMain.updateBank2;
begin
  tb_Delay.Position := 236-curDelay_Bank2;
  tb_Pitch.Position := 1500-curPitch_Bank2;
  tb_Ambient.Position := 32700-curAmbient_Bank2;
  tb_Effect.Position := 32700-curEffect_Bank2;
  tb_Master.Position := 32700-curMaster_Bank2;
  tb_Threshold.Position := 6000-curThreshold_Bank2;
  tb_Release.Position := 10000-curRelease_Bank2;
  RadioGroup1.ItemIndex := curSocket_Bank2;
end;



{==========================================================================
 Procedure: updateBank1
 Inputs:    None
 Ouputs:    None
 About:     Update BANK 1 controls
 ==========================================================================}
procedure TfrmMain.updateBank1;
begin
  tb_Delay.Position := 236-curDelay_Bank1;
  tb_Pitch.Position := 1500-curPitch_Bank1;
  tb_Ambient.Position := 32700-curAmbient_Bank1;
  tb_Effect.Position := 32700-curEffect_Bank1;
  tb_Master.Position := 32700-curMaster_Bank1;
  tb_Threshold.Position := 6000-curThreshold_Bank1;
  tb_Release.Position := 10000-curRelease_Bank1;
  RadioGroup1.ItemIndex := curSocket_Bank1;
end;



{==========================================================================
 Procedure: getSettings
 Inputs:    None
 Ouputs:    None
 About:     Get current bank slider values from .ini file
 ==========================================================================}
procedure TfrmMain.getSettings;
var
  IniFile: TIniFile;
  i: integer;
begin
  OpenDialog1.InitialDir :=  Application.GetNamePath;

  OpenDialog1.Filter := 'VoiceAmp settings (*.ini)|*.ini';

  if OpenDialog1.Execute then
  begin
    IniFile := TIniFile.Create(OpenDialog1.FileName);
    CurFileName := OpenDialog1.FileName;
    
    // BANK 1
    curDelay_Bank1 := IniFile.ReadInteger('PROGRAM1','Delay',i);
    curPitch_Bank1 := IniFile.ReadInteger('PROGRAM1','Pitch',i);
    curAmbient_Bank1 := IniFile.ReadInteger('PROGRAM1','Ambient',i);
    curEffect_Bank1 := IniFile.ReadInteger('PROGRAM1','Effect',i);
    curMaster_Bank1 := IniFile.ReadInteger('PROGRAM1','Master',i);
    curThreshold_Bank1 := IniFile.ReadInteger('PROGRAM1','Threshold',i);
    curRelease_Bank1 := IniFile.ReadInteger('PROGRAM1','Release',i);
    curSocket_Bank1 := IniFile.ReadInteger('PROGRAM1','Socket',i);;

    // BANK 2
    curDelay_Bank2 := IniFile.ReadInteger('PROGRAM2','Delay',i);
    curPitch_Bank2 := IniFile.ReadInteger('PROGRAM2','Pitch',i);
    curAmbient_Bank2 := IniFile.ReadInteger('PROGRAM2','Ambient',i);
    curEffect_Bank2 := IniFile.ReadInteger('PROGRAM2','Effect',i);
    curMaster_Bank2 := IniFile.ReadInteger('PROGRAM2','Master',i);
    curThreshold_Bank2 := IniFile.ReadInteger('PROGRAM2','Threshold',i);
    curRelease_Bank2 := IniFile.ReadInteger('PROGRAM2','Release',i);
    curSocket_Bank2 := IniFile.ReadInteger('PROGRAM2','Socket',i);;

    // BANK 3
    curDelay_Bank3 := IniFile.ReadInteger('PROGRAM3','Delay',i);
    curPitch_Bank3 := IniFile.ReadInteger('PROGRAM3','Pitch',i);
    curAmbient_Bank3 := IniFile.ReadInteger('PROGRAM3','Ambient',i);
    curEffect_Bank3 := IniFile.ReadInteger('PROGRAM3','Effect',i);
    curMaster_Bank3 := IniFile.ReadInteger('PROGRAM3','Master',i);
    curThreshold_Bank3 := IniFile.ReadInteger('PROGRAM3','Threshold',i);
    curRelease_Bank3 := IniFile.ReadInteger('PROGRAM3','Release',i);
    curSocket_Bank3 := IniFile.ReadInteger('PROGRAM3','Socket',i);;

    case CurrentBank of
      1: updateBank1();
      2: updateBank2();
      3: updateBank3();
    else
    end;
    StatusBar1.Panels.Items[2].Text := IniFile.FileName;
    IniFile.Free;
    showValues(); // Update controls and variable
  end;
end;



{==========================================================================
 Procedure: saveSettings
 Inputs:    None
 Ouputs:    None
 About:     Save current bank slider values in .ini file
 ==========================================================================}
procedure TfrmMain.saveSettings;
var
  IniFile: TIniFile;
  Action: TCloseAction;
  s, Rx: String;
  strData: TStrings;    // Stores parsed values
  mins: Integer;        // Minute counter storage
  ovfl: Integer;        // Overflow counter storage
  total: Integer;       // Total calculated usage in minutes
  i,j:Integer;
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values
  timeStr: String;      // Time string
  Rev: String;          // Revision string storage
  Dat: String;          // Release Date string storage
  SavDat: String;       // Date of save
begin
  // Retrieve unit data (SerNum, Revision, Revision date, Usage)
  with frm_Message do
  begin
    Show;
    pnl_Heading.Caption := 'Getting unit data';
    pnl_Main.Caption := 'Updating data, please wait...';
  end;

  //--- Save serial number ---//
  curSerNum := pnl_Serial.Caption;

  //--- Save current date ---//
  SavDat := DateToStr(Date);

  if DevicePresent then
  begin
    //--- Get Revision data--- //
    s := 'AT+GETRV=';
    if DevicePresent then
      write_value(s,Device_Description);
    if FT_In_Buffer <> '' then
    begin
      Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
      j:=StrLen(PChar(Rx));         // Save string lenght
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETRV' then
        curRev := vals
      else
        curRev := 'Not available';
     end;
  end;

  Delay(500); // Wait for comms to complete

  if DevicePresent then
  begin
    //--- Get Revision date--- //
    s := 'AT+GETDT=';
    if DevicePresent then
      write_value(s,Device_Description);
    if FT_In_Buffer <> '' then
    begin
      Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
      j:=StrLen(PChar(Rx));         // Save string lenght
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETDT' then
      begin
        curRevDate := vals;
     end
      else
        curRevDate := 'Not available';
     end;
  end;

  Delay(500); // Wait for comms to complete

  if DevicePresent then
  begin
    {--- Get usage data---}
    s := 'AT+TIMER=';
    if DevicePresent then
      write_value(s,Device_Description);
    if FT_In_Buffer <> '' then
    begin
      Rx := Trim(FT_In_Buffer);      // Lose NL/CR etc
      j:=StrLen(PChar(Rx));          // Save string lenght
      i := AnsiPos('=',Rx);          // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);        // Save only AT cmd
      vals := RightStr(Rx,j-i);      // Save only values
      if Cmd = 'AT+TIMER' then
      begin
        strData := TStringList.Create;
        Split(vals,',',strData);
        mins := StrToInt(strData[0]);
        ovfl := StrToInt(strData[1]);
        Total := (ovfl*65533) + mins;
        TimeStr := MinutesToDaysHoursMinutes(Total);
        curUsage := TimeStr;
      end
      else
        curUsage := 'Not available';
    end;
  end;

  // Done
  frm_Message.Visible := False;

  // Wait for data retrieval to complete
  while frm_Message.Visible do
    Application.ProcessMessages;

  // Show save dialog
  SaveDialog1.InitialDir := GetCurrentDir;
  SaveDialog1.Filter := 'VoiceAmp settings (*.ini)|*.ini';
  SaveDialog1.DefaultExt := '*.ini';

  if SaveDialog1.Execute then
  begin
    IniFile := TIniFile.Create(SaveDialog1.FileName);
    curFileName := SaveDialog1.FileName;

    // Unit data
    IniFile.WriteString('DEVICE','Serial',curSerNum);
    IniFile.WriteString('DEVICE','Revision',curRev);
    IniFile.WriteString('DEVICE','RevDate',curRevDate);
    IniFile.WriteString('DEVICE','Usage',curUsage);
    IniFile.WriteString('DEVICE','SaveDate',SavDat);
    
    // BANK1 data
    IniFile.WriteInteger('PROGRAM1','Delay',curDelay_Bank1);
    IniFile.WriteInteger('PROGRAM1','Pitch',curPitch_Bank1);
    IniFile.WriteInteger('PROGRAM1','Ambient',curAmbient_Bank1);
    IniFile.WriteInteger('PROGRAM1','Effect',curEffect_Bank1);
    IniFile.WriteInteger('PROGRAM1','Master',curMaster_Bank1);
    IniFile.WriteInteger('PROGRAM1','Threshold',curThreshold_Bank1);
    IniFile.WriteInteger('PROGRAM1','Release',curRelease_Bank1);
    IniFile.WriteInteger('PROGRAM1','Socket',curSocket_Bank1);
    // BANK2 data
    IniFile.WriteInteger('PROGRAM2','Delay',curDelay_Bank2);
    IniFile.WriteInteger('PROGRAM2','Pitch',curPitch_Bank2);
    IniFile.WriteInteger('PROGRAM2','Ambient',curAmbient_Bank2);
    IniFile.WriteInteger('PROGRAM2','Effect',curEffect_Bank2);
    IniFile.WriteInteger('PROGRAM2','Master',curMaster_Bank2);
    IniFile.WriteInteger('PROGRAM2','Threshold',curThreshold_Bank2);
    IniFile.WriteInteger('PROGRAM2','Release',curRelease_Bank2);
    IniFile.WriteInteger('PROGRAM2','Socket',curSocket_Bank2);
    // BANK3 data
    IniFile.WriteInteger('PROGRAM3','Delay',curDelay_Bank3);
    IniFile.WriteInteger('PROGRAM3','Pitch',curPitch_Bank3);
    IniFile.WriteInteger('PROGRAM3','Ambient',curAmbient_Bank3);
    IniFile.WriteInteger('PROGRAM3','Effect',curEffect_Bank3);
    IniFile.WriteInteger('PROGRAM3','Master',curMaster_Bank3);
    IniFile.WriteInteger('PROGRAM3','Threshold',curThreshold_Bank3);
    IniFile.WriteInteger('PROGRAM3','Release',curRelease_Bank3);
    IniFile.WriteInteger('PROGRAM3','Socket',curSocket_Bank3);

    StatusBar1.Panels.Items[2].Text := IniFile.FileName;
    IniFile.Free;
    Action := caNone;         // Do not exit on cancel
  end;
end;



{==========================================================================
 Procedure: showValues
 Inputs:    None
 Ouputs:    None
 About:     Show current slider values in panels
 ==========================================================================}
procedure TfrmMain.showValues;
begin
  tb_DelayChange(Self);
  tb_PitchChange(Self);
  tb_AmbientChange(Self);
  tb_EffectChange(Self);
  tb_MasterChange(Self);
  tb_ThresholdChange(Self);
  tb_ReleaseChange(Self);
  ComboBox1Select(Self);
  RadioGroup1Click(Self);
end;



{==========================================================================
 Procedure: addTicks
 Inputs:    None
 Ouputs:    None
 About:     Paint tick marks on large value TrackBars
 ==========================================================================}
procedure TfrmMain.addTicks();
var
  i,j,k: integer;
begin
  // Threshold bar
  j := tb_Threshold.Max div 10;
  k := j;
  for i := 1 to 10 do
  begin
    tb_Threshold.SetTick(j);
    j := j+k;
  end;

  // Master bar
  j := tb_Master.Max div 10;
  k := j;
  for i := 1 to 10 do
  begin
    tb_Master.SetTick(j);
    j := j+k;
  end;

  // Effect bar
  j := tb_Effect.Max div 10;
  k := j;
  for i := 1 to 10 do
  begin
    tb_Effect.SetTick(j);
    j := j+k;
  end;

  // Ambient bar
  j := tb_Ambient.Max div 10;
  k := j;
  for i := 1 to 10 do
  begin
    tb_Ambient.SetTick(j);
    j := j+k;
  end;
end;



{==========================================================================
 Procedure: FormShow
 Inputs:    None
 Ouputs:    None
 About:     Check for available devices on form show event
 ==========================================================================}
procedure TfrmMain.FormShow(Sender: TObject);
var
  i : Integer;
  oVer : TFileVersion;
begin
  // --- Clear Status Flags --- //
  StartUp := True;
  DevicePresent := False;
  DeviceBusy := False;
  ProgramChanged := False;
  FileChanged := False;

  oVer := TFileVersion.Create;
  Version := IntToStr(oVer.VerMajor) + '.' +
             IntToStr(oVer.VerMinor) + '.' +
             IntToStr(oVer.VerRelease);
  oVer.Destroy;

  Caption := Caption + ' ' + Version; // Current software version

  addTicks();                // Add tick marks to sliders
  showValues();              // Show panel values

  Combobox1.ItemIndex := 0;  // Set Combo index to BANK 1
  ComboBox1Select( Self );   // Update display
  loadHints();               // Load help hints
  GroupBox1.SetFocus;


  FT_Enable_Error_Report := False; // Error reporting = on
  DevicePresent := False;          // Reset flag
  LoadSettings();		               // Pre-Load USB settings

  Display_Defaults();
  StartUp := False;
end;



{==========================================================================
 Procedure: tmr_ConnectedTimer
 Inputs:    None
 Ouputs:    None
 About:     Check if device is connected on timer event
 ==========================================================================}
procedure TfrmMain.tmr_ConnectedTimer(Sender: TObject);
var
  PortStatus : FT_Result;
  NoOfDevs: Integer;
begin
  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;
  NoOfDevs := FT_Device_Count;
  if NoOfDevs > 0 then
  begin
    PortStatus := GetFTDeviceDescription(0);
    if PortStatus <> FT_OK then exit;
    if FT_Device_String = Device_Description then
    begin
      DevicePresent := True;
      StatusBar1.Panels.Items[0].Text := 'Connected';
      Led1.State := True;
      ControlsEnable(True);

      // ---- Test!!
      if GetFTDeviceSerialNo(0) = FT_OK then
        pnl_Serial.Caption := FT_Device_String;
      // ----

    end;
  end
  else
  begin
    StatusBar1.Panels.Items[0].Text := 'NOT Connected!';
    pnl_Serial.Caption := 'No device present';
    Led1.State := False;
    DevicePresent := False;
    ControlsEnable(False);
  end;
end;



{==========================================================================
 Procedure: tmrComDelayTimer
 Inputs:    None
 Ouputs:    None
 About:     Comms Tx delay timer
 ==========================================================================}
procedure TfrmMain.tmrComDelayTimer(Sender: TObject);
begin
    DeviceBusy := False;
    tmrComDelay.Enabled := False;
end;



{==========================================================================
 Procedure: tb_DelayChange
 Inputs:    None
 Ouputs:    None
 About:     Send Delay value
 ==========================================================================}
procedure TfrmMain.tb_DelayChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin

  val := 236-tb_Delay.Position;

  case CurrentBank of
   1: curDelay_Bank1 := val;
   2: curDelay_Bank2 := val;
   3: curDelay_Bank3 := val;
  end;

  s := 'AT+DLYST=' +  IntToStr(val);
  pnlDelay.Caption := IntToStr(val) + 'ms';
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: tb_PitchChange
 Inputs:    None
 Ouputs:    None
 About:     Send pitch value
 ==========================================================================}
procedure TfrmMain.tb_PitchChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 1500-tb_Pitch.Position;
  case CurrentBank of
   1: curPitch_Bank1 := val;
   2: curPitch_Bank2 := val;
   3: curPitch_Bank3 := val;
  end;
  s := 'AT+SFTST=' +  IntToStr(val);
  pnlPitch.Caption := IntToStr(val) + 'Hz';
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: tb_AmbientChange
 Inputs:    None
 Ouputs:    None
 About:     Send ambient gain value
 ==========================================================================}
procedure TfrmMain.tb_AmbientChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 32700-tb_Ambient.Position;
  case CurrentBank of
   1: curAmbient_Bank1 := val;
   2: curAmbient_Bank2 := val;
   3: curAmbient_Bank3 := val;
  end;
  s := 'AT+DRYST=' +  IntToStr(val);
  val := round((val/32700)*100);
  pnlAmbient.Caption := IntToStr(val) + '%';
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: tb_EffectChange
 Inputs:    None
 Ouputs:    None
 About:     Send effect gain value
 ==========================================================================}
procedure TfrmMain.tb_EffectChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 32700-tb_Effect.Position;
  case CurrentBank of
   1: curEffect_Bank1 := val;
   2: curEffect_Bank2 := val;
   3: curEffect_Bank3 := val;
  end;
  s := 'AT+WETST=' +  IntToStr(val);
  val := round((val/32700)*100);
  pnlEffect.Caption := IntToStr(val) + '%';
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: tb_MasterChange
 Inputs:    None
 Ouputs:    None
 About:     Send master gain value
 ==========================================================================}
procedure TfrmMain.tb_MasterChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 32700-tb_Master.Position;
  case CurrentBank of
   1: curMaster_Bank1 := val;
   2: curMaster_Bank2 := val;
   3: curMaster_Bank3 := val;
  end;
  s := 'AT+MSTST=' +  IntToStr(val);
  val := round((val/32700)*100);
  pnlMaster.Caption := IntToStr(val) + '%';
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: tb_ThresholdChange
 Inputs:    None
 Ouputs:    None
 About:     Send noise gate threshold value
 ==========================================================================}
procedure TfrmMain.tb_ThresholdChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 6000 - tb_Threshold.Position;
  case CurrentBank of
   1: curThreshold_Bank1 := val;
   2: curThreshold_Bank2 := val;
   3: curThreshold_Bank3 := val;
  end;
  s := 'AT+NGTST=' +  IntToStr(val);
  val := round((val/6000)*100);
  pnlThreshold.Caption := IntToStr(val) + '%';
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: tb_ReleaseChange
 Inputs:    None
 Ouputs:    None
 About:     Send noise gate release value
 ==========================================================================}
procedure TfrmMain.tb_ReleaseChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 10001 - tb_Release.Position;
  case CurrentBank of
   1: curRelease_Bank1 := val;
   2: curRelease_Bank2 := val;
   3: curRelease_Bank3 := val;
  end;
  s := 'AT+NGRST=' +  IntToStr(val);
  val := round((val/10001)*100);
  pnlRelease.Caption := IntToStr(val) + '%';
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: ComboBox1DrawItem
 Inputs:    None
 Ouputs:    None
 About:     Dynamically load colour banks into combo
 ==========================================================================}
procedure TfrmMain.ComboBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
 with (Control as TComboBox).Canvas do
 begin
  Font.Color := Colors[index];
  FillRect(Rect);
  TextOut(Rect.Left,
          Rect.Top,
          'PROGRAM ' + IntToStr(index+1));
 end;
end;



{==========================================================================
 Procedure: ComboBox1Select
 Inputs:    None
 Ouputs:    None
 About:     Select active settings BANK
 ==========================================================================}
procedure TfrmMain.ComboBox1Select(Sender: TObject);
var i: Integer;
begin

  i := Combobox1.ItemIndex;
  case i of
   0:
     begin
       GroupBox1.Font.Color := Colors[i];
       GroupBox2.Font.Color := Colors[i];
       PDJRotoLabel1.Font.Color := Colors[i];
       RadioGroup1.Font.Color := Colors[i];
       PDJRotoLabel1.Caption := 'PROGRAM 1';
       StatusBar1.Panels.Items[1].Text := 'PROGRAM: 1';
       CurrentBank := 1;
       updateBank1;   // Update control values to current
     end;
   1:
     begin
       GroupBox1.Font.Color := Colors[i];
       GroupBox2.Font.Color := Colors[i];
       PDJRotoLabel1.Font.Color := Colors[i];
       RadioGroup1.Font.Color := Colors[i];
       PDJRotoLabel1.Caption := 'PROGRAM 2';
       StatusBar1.Panels.Items[1].Text := 'PROGRAM: 2';
       CurrentBank := 2;
       updateBank2;   // Update control values to current
     end;
   2:
     begin
       GroupBox1.Font.Color := Colors[i];
       GroupBox2.Font.Color := Colors[i];
       PDJRotoLabel1.Font.Color := Colors[i];
       RadioGroup1.Font.Color := Colors[i];
       PDJRotoLabel1.Caption := 'PROGRAM 3';
       StatusBar1.Panels.Items[1].Text := 'PROGRAM: 3';
       CurrentBank := 3;
       updateBank3;   // Update control values to current
     end;
  else
     begin
       GroupBox1.Font.Color := Colors[0];
       GroupBox2.Font.Color := Colors[0];
       PDJRotoLabel1.Font.Color := Colors[0];
       RadioGroup1.Font.Color := Colors[i];
       PDJRotoLabel1.Caption := 'PROGRAM 1';
       StatusBar1.Panels.Items[1].Text := 'PROGRAM: 1';
       CurrentBank := 1;
       updateBank1;   // Update control values to current
     end;
  end;
end;



{==========================================================================
 Procedure: Action1Execute
 Inputs:    None
 Ouputs:    None
 About:     Open settings file
 ==========================================================================}
procedure TfrmMain.Action1Execute(Sender: TObject);
begin
  StartUp := True;  // Disable prompts during file load
  getSettings();
  StartUp := False;
end;



{==========================================================================
 Procedure: Action2Execute
 Inputs:    None
 Ouputs:    None
 About:     Open settings file
 ==========================================================================}
procedure TfrmMain.Action2Execute(Sender: TObject);
begin
  saveSettings();
end;

{==========================================================================
 Procedure: Action3Execute
 Inputs:    None
 Ouputs:    None
 About:     Open Comms Selection panel
 ==========================================================================}
procedure TfrmMain.Action3Execute(Sender: TObject);
begin
//  saveSettings();
end;


{==========================================================================
 Procedure: act_HintsExecute
 Inputs:    None
 Ouputs:    None
 About:     Enable/Disable Hints
 ==========================================================================}
procedure TfrmMain.act_HintsExecute(Sender: TObject);
begin
  if  act_Hints.Checked = False then
  begin
    act_Hints.Checked := True;
    Application.ShowHint := True;
  end else
  if act_Hints.Checked then
  begin
    act_Hints.Checked := False;
    Application.ShowHint := False;
  end;
end;



{==========================================================================
 Procedure: ControlsEnable
 Inputs:    state
 Ouputs:    None
 About:     Enable/Disable controls
 ==========================================================================}
procedure TfrmMain.ControlsEnable(state: Boolean);
begin
  tb_Delay.Enabled := state;
  tb_Pitch.Enabled := state;
  tb_Ambient.Enabled := state;
  tb_Effect.Enabled := state;
  tb_Master.Enabled := state;
  tb_Threshold.Enabled := state;
  tb_Release.Enabled := state;
  ComboBox1.Enabled := state;
  Button1.Enabled := state;
  Button2.Enabled := state;
  Button3.Enabled := state;
  RadioGroup1.Enabled := state;
  act_GetDeviceData.Enabled := state;
end;



{==========================================================================
 Procedure: Button2Click
 Inputs:    None
 Ouputs:    None
 About:     Send the curent bank settings to the VA601 unit
 ==========================================================================}
procedure TfrmMain.Button2Click(Sender: TObject);
var
 s: String;
 Rx, bnk: String;
 strData: TStrings;    // Stores parsed values
 i,j:Integer;
 cmd: String;          // Stores received AT command
 vals: String;         // Stores received values

begin

  //--- Setup feedback window ---//
  with frm_Message do
  begin
    Show;
    pnl_Heading.Caption := 'Sending device data';
    pnl_Main.Caption := 'Sending data to device, please wait...';
  end;
  Wait(2);

  case CurrentBank of
   //--- SEND SETTINGS FOR PROGRAM 1 ---//
   1:
   begin
   if DevicePresent then
   begin
     //--- Get Revision data--- //
     s := 'AT+BNKST=' +
     IntToStr(CurrentBank) +  ',' +
     IntToStr(curEffect_Bank1) +  ',' +
     IntToStr(curAmbient_Bank1) +  ',' +
     IntToStr(curMaster_Bank1) +  ',' +
     IntToStr(curDelay_Bank1) +  ',' +
     IntToStr(curPitch_Bank1) +  ',' +
     IntToStr(curThreshold_Bank1) +  ',' +
     IntToStr(curRelease_Bank1) + ',' +
     IntToStr(curSocket_Bank1);

     if DevicePresent then
       write_value(s,Device_Description);
     begin
       Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
       j:=StrLen(PChar(Rx));         // Save string lenght
       i := AnsiPos('=',Rx);         // Look for '=' sign
       Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
       vals := RightStr(Rx,j-i);     // Save only values
       if Cmd = 'AT+BNKST' then
       begin
         with frm_Message do
         begin
           bnk := vals;
           pnl_Main.Caption := 'Program ' + bnk + ' successfully updated!';
           Wait(2);
           Close;
         end;
       end
       else
       begin
         with frm_Message do
         begin
           bnk := vals;
           pnl_Main.Caption := 'Update failed.';
           Wait(2);
           Close;
         end;
       end;
      end;
    end;
   end;
   //--- SEND SETTINGS FOR PROGRAM 2 ---//
   2:
   begin
   if DevicePresent then
   begin
     //--- Get Revision data--- //
     s := 'AT+BNKST=' +
     IntToStr(CurrentBank) +  ',' +
     IntToStr(curEffect_Bank2) +  ',' +
     IntToStr(curAmbient_Bank2) +  ',' +
     IntToStr(curMaster_Bank2) +  ',' +
     IntToStr(curDelay_Bank2) +  ',' +
     IntToStr(curPitch_Bank2) +  ',' +
     IntToStr(curThreshold_Bank2) +  ',' +
     IntToStr(curRelease_Bank2) + ',' +
     IntToStr(curSocket_Bank2);
     if DevicePresent then
       write_value(s,Device_Description);
     begin
       Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
       j:=StrLen(PChar(Rx));         // Save string lenght
       i := AnsiPos('=',Rx);         // Look for '=' sign
       Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
       vals := RightStr(Rx,j-i);     // Save only values
       if Cmd = 'AT+BNKST' then
       begin
         with frm_Message do
         begin
           bnk := vals;
           pnl_Main.Caption := 'Program ' + bnk + ' successfully updated!';
           Wait(2);
           Close;
         end;
       end
       else
       begin
         with frm_Message do
         begin
           bnk := vals;
           pnl_Main.Caption := 'Update failed.';
           Wait(2);
           Close;
         end;
       end;
      end;
    end;
   end;
   //--- SEND SETTINGS FOR PROGRAM 3 ---//
   3:
   begin
   if DevicePresent then
   begin
     //--- Get Revision data--- //
     s := 'AT+BNKST=' +
     IntToStr(CurrentBank) +  ',' +
     IntToStr(curEffect_Bank3) +  ',' +
     IntToStr(curAmbient_Bank3) +  ',' +
     IntToStr(curMaster_Bank3) +  ',' +
     IntToStr(curDelay_Bank3) +  ',' +
     IntToStr(curPitch_Bank3) +  ',' +
     IntToStr(curThreshold_Bank3) +  ',' +
     IntToStr(curRelease_Bank3) + ',' +
     IntToStr(curSocket_Bank3);
     if DevicePresent then
       write_value(s,Device_Description);
     begin
       Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
       j:=StrLen(PChar(Rx));         // Save string lenght
       i := AnsiPos('=',Rx);         // Look for '=' sign
       Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
       vals := RightStr(Rx,j-i);     // Save only values
       if Cmd = 'AT+BNKST' then
       begin
         with frm_Message do
         begin
           bnk := vals;
           pnl_Main.Caption := 'Program ' + bnk + ' successfully updated!';
           Wait(2);
           Close;
         end;
       end
       else
       begin
         with frm_Message do
         begin
           bnk := vals;
           pnl_Main.Caption := 'Update failed.';
           Wait(2);
           Close;
         end;
       end;
      end;
    end;
   end;
  end; 
end;




{==========================================================================
 Procedure: RadioGroup1Click
 Inputs:    None
 Ouputs:    None
 About:     Send the selected audio socket setting
 ==========================================================================}
procedure TfrmMain.RadioGroup1Click(Sender: TObject);
var
 s: String;
begin
  case CurrentBank of
   1: curSocket_Bank1 := RadioGroup1.ItemIndex;
   2: curSocket_Bank2 := RadioGroup1.ItemIndex;
   3: curSocket_Bank3 := RadioGroup1.ItemIndex;
  end;
  s := 'AT+STSCK=' + IntToStr(RadioGroup1.ItemIndex);
  if DevicePresent then
    write_value(s,Device_Description);
end;



{==========================================================================
 Procedure: act_AboutExecute
 Inputs:    None
 Ouputs:    None
 About:     Show about form
 ==========================================================================}
procedure TfrmMain.act_AboutExecute(Sender: TObject);
begin
  frmAbout.Show;
end;



{==========================================================================
 Procedure: Display_Defaults
 Inputs:    None
 Ouputs:    None
 About:     Display default values
 ==========================================================================}
procedure TfrmMain.Button1Click(Sender: TObject);
begin
  Display_Defaults();
end;



{==========================================================================
 Procedure: act_GetUsageExecute
 Inputs:    None
 Ouputs:    None
 About:     Get the unit usage
 ==========================================================================}
procedure TfrmMain.act_GetDeviceDataExecute(Sender: TObject);
var
  s, Rx: String;
  strData: TStrings;    // Stores parsed values
  mins: Integer;        // Minute counter storage
  ovfl: Integer;        // Overflow counter storage
  total: Integer;       // Total calculated usage in minutes
  i,j:Integer;
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values
  timeStr: String;      // Time string
  Rev: String;          // Revision string storage
  Dat: String;          // Release Date string storage

begin
  // --- Show feedback form ---//
  with frm_Feedback do
  begin
    Show;                       // Show form
    Button1.Enabled := False;   // Disable OK button
    with Memo1.Lines do
    begin
      Clear;                                        // Clear display
      Caption := 'Device settings for ' + pnl_Serial.Caption;
      Add('Checking device for information...');
      Add('');
      Add('Serial number: ' + pnl_Serial.Caption);
      Add('');
    end;
  end;

  if DevicePresent then
  begin
    //--- Get Revision data--- //
    s := 'AT+GETRV=';
    if DevicePresent then
      write_value(s,Device_Description);

    if FT_In_Buffer <> '' then
    begin
      Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
      j:=StrLen(PChar(Rx));         // Save string lenght
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETRV' then
      begin
        Rev := vals;
        frm_Feedback.Memo1.Lines.Add('Firmware revision: ' + vals );
        frm_Feedback.Memo1.Lines.Add('');
      end
      else
      begin
        frm_Feedback.Memo1.Lines.Add('Firmware revision not available.');
        frm_Feedback.Memo1.Lines.Add('');
      end;
     end;
  end;

  Delay(500); // Wait for comms to complete

  if DevicePresent then
  begin
    //--- Get Revision data--- //
    s := 'AT+GETDT=';
    if DevicePresent then
      write_value(s,Device_Description);

    if FT_In_Buffer <> '' then
    begin
      Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
      j:=StrLen(PChar(Rx));         // Save string lenght
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETDT' then
      begin
        Rev := vals;
        frm_Feedback.Memo1.Lines.Add('Firmware release date: ' + vals );
        frm_Feedback.Memo1.Lines.Add('');
      end
      else
      begin
        frm_Feedback.Memo1.Lines.Add('Firmware release date not available.');
        frm_Feedback.Memo1.Lines.Add('');
      end;
     end;
  end;

  Delay(500); // Wait for comms to complete

  if DevicePresent then
  begin
    {--- Get usage data---}
    s := 'AT+TIMER=';
    if DevicePresent then
      write_value(s,Device_Description);
    if FT_In_Buffer <> '' then
    begin
      Rx := Trim(FT_In_Buffer);      // Lose NL/CR etc
      j:=StrLen(PChar(Rx));          // Save string lenght
      i := AnsiPos('=',Rx);          // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);        // Save only AT cmd
      vals := RightStr(Rx,j-i);      // Save only values
      if Cmd = 'AT+TIMER' then
      begin
        strData := TStringList.Create;
        Split(vals,',',strData);
        mins := StrToInt(strData[0]);
        ovfl := StrToInt(strData[1]);
        Total := (ovfl*65533) + mins;
        TimeStr := MinutesToDaysHoursMinutes(Total);

        frm_Feedback.Memo1.Lines.Add('Usage counter: ' + TimeStr );
        frm_Feedback.Memo1.Lines.Add('');
      end
      else
      begin
        frm_Feedback.Memo1.Lines.Add('Usage counter data not available.');
        frm_Feedback.Memo1.Lines.Add('');
      end;
    end;
  end;

  frm_Feedback.Memo1.Lines.Add('Done.');
  frm_Feedback.Button1.Enabled := True;  // Enable feedback form ENABLE button
end;



{==========================================================================
 Function:  Split
 Inputs:    Chaine, Casseur, Retour
 Ouputs:    None
 About:     Parse delimeted string, stores results in TStrings
 ==========================================================================}
function TfrmMain.Split(Chaine: string; Casseur: string; Retour: TStrings): integer;
var
  Start: integer;
  LeftString: string;
  TempString: string;
begin
  TempString := Chaine;                                 //used to check if the last char from
                                                        // "Chaine" is the LineBreaker
  Delete(TempString,1,length(TempString)-1);            // keep the last Char
  if TempString <> Casseur then Chaine := Chaine + Casseur;
                                                        // if the last char is not "Casseur" then put it at the end
  repeat
    Start := Pos(Casseur,Chaine);                       // find the "casseur"
    LeftString := Chaine;                               // Create tempory handler
    Delete(LeftString,Start,Length(LeftString));        // keep the LeftToken
    Delete(Chaine,1,Start);                             // keep the rest of "Chaine"
    Retour.Add (LeftString);                            // Add the new Parse to "Retour"
    until Pos(Casseur,Chaine) = 0;                      // Reapet until end
end;



{==========================================================================
 Function:  MinutesToDaysHoursMinutes
 Inputs:    Minutes
 Ouputs:    String
 About:     Convert minutes into days, hours, minutes
 ==========================================================================}
function TfrmMain.MinutesToDaysHoursMinutes(AMinutes: Integer): string;
const
  HOURSPERDAY = 24;
var
  Days: Integer;
  Hours: Integer;
  Minutes: Integer;
begin
  if (AMinutes > 0) then
  begin
    Hours   := AMinutes div 60;
    Minutes := AMinutes mod 60;
    Days    := Hours div HOURSPERDAY;
    Hours   := Hours mod HOURSPERDAY;
  end
  else
  begin
    Hours   := 0;
    Minutes := 0;
    Days    := 0;
  end;
  Result := Format('%.2d Days %.2d Hrs %.2d Minutes', [Days, Hours, Minutes]);
end;



{==========================================================================
 Function:  Button3Click
 Inputs:    None
 Ouputs:    None
 About:     Get current settings from device and display on screen controls
 ==========================================================================}
procedure TfrmMain.Button3Click(Sender: TObject);
var
  s, Rx: String;
  strData: TStrings;    // Stores parsed values
  i,j,k:Integer;        // Counters
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values
begin
  StartUp:= True;
  //--- Setup feedback window ---//
  with frm_Message do
  begin
    Show;
    pnl_Heading.Caption := 'Getting unit data';
    pnl_Main.Caption := 'Getting data, please wait...';
  end;

  if DevicePresent then
  begin
    s := 'AT+GETST=';
    if DevicePresent then
      write_value(s,Device_Description);

    if FT_In_Buffer <> '' then
    begin
      Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
      j:=StrLen(PChar(Rx));         // Save string lenght
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETST' then
      begin
        strData := TStringList.Create;
        Split(vals,',',strData);         // Parse value string

        {-- Extract current bank --}
        k := StrToInt(strData[0]);       // Extract current bank value
        CurrentBank := k;                // Save bank

        {-- Extract BANK1 values --}
        k := StrToInt(strData[1]);       // Extract Effect1 gain
        curEffect_Bank1 := k;            // Save
        k := StrToInt(strData[2]);       // Extract Ambient1 gain
        curAmbient_Bank1 := k;           // Save
        k := StrToInt(strData[3]);       // Extract Master1 gain
        curMaster_Bank1 := k;            // Save
        k := StrToInt(strData[4]);       // Extract Delay1
        curDelay_Bank1 := k;             // Save
        k := StrToInt(strData[5]);       // Extract Shift1
        curPitch_Bank1 := k;             // Save
        k := StrToInt(strData[6]);       // Extract Threshold1
        curThreshold_Bank1 := k;         // Save
        k := StrToInt(strData[7]);       // Extract Release1
        curRelease_Bank1 := k;           // Save
        k := StrToInt(strData[8]);       // Extract Socket1
        curSocket_Bank1 := k;            // Save

        {-- Extract BANK2 values --}
        k := StrToInt(strData[9]);       // Extract Effect2 gain
        curEffect_Bank2 := k;            // Save
        k := StrToInt(strData[10]);      // Extract Ambient2 gain
        curAmbient_Bank2 := k;           // Save
        k := StrToInt(strData[11]);      // Extract Master2 gain
        curMaster_Bank2 := k;            // Save
        k := StrToInt(strData[12]);      // Extract Delay2
        curDelay_Bank2 := k;             // Save
        k := StrToInt(strData[13]);      // Extract Shift2
        curPitch_Bank2 := k;             // Save
        k := StrToInt(strData[14]);      // Extract Threshold2
        curThreshold_Bank2 := k;         // Save
        k := StrToInt(strData[15]);      // Extract Release2
        curRelease_Bank2 := k;           // Save
        k := StrToInt(strData[16]);      // Extract Socket2
        curSocket_Bank2 := k;            // Save

        {-- Extract BANK3 values --}
        k := StrToInt(strData[17]);      // Extract Effect3 gain
        curEffect_Bank3 := k;            // Save
        k := StrToInt(strData[18]);      // Extract Ambient3 gain
        curAmbient_Bank3 := k;           // Save
        k := StrToInt(strData[19]);      // Extract Master3 gain
        curMaster_Bank3 := k;            // Save
        k := StrToInt(strData[20]);      // Extract Delay3
        curDelay_Bank3 := k;             // Save
        k := StrToInt(strData[21]);      // Extract Shift3
        curPitch_Bank3 := k;             // Save
        k := StrToInt(strData[22]);      // Extract Threshold3
        curThreshold_Bank3 := k;         // Save
        k := StrToInt(strData[23]);      // Extract Release3
        curRelease_Bank3 := k;           // Save
        k := StrToInt(strData[24]);      // Extract Socket3
        curSocket_Bank3 := k;            // Save

        {-- Update the controls --}
        ComboBox1.ItemIndex := CurrentBank-1;
        ComboBox1.OnSelect(Self);
        with frm_Message do
        begin
          pnl_Heading.Caption := 'Getting unit data';
          pnl_Main.Caption := 'Device settings received!';
          Wait(2);
          Close;
        end;
      end
      else
      begin
        with frm_Message do
        begin
          pnl_Heading.Caption := 'Getting unit data';
          pnl_Main.Caption := 'Command not supported.';
          Wait(2);
          Close;
        end;
      end;
    end;
  end;
  StartUp:= False;
end;



{==========================================================================
 Function:  Feedback
 Inputs:    head, msg, time
 Ouputs:    None
 About:     Give user feedback message(msg) for specified time(time)
 ==========================================================================}
procedure TfrmMain.Feedback(head: string; msg: string; time: Integer; UseTmr: Boolean);
begin
  with frm_Message do
  begin
    pnl_Heading.Caption := head;
    pnl_Main.Caption := msg;
    Show;
    Wait(time);
    Close;
  end;
end;



{==========================================================================
 Function:  Delay
 Inputs:    dwMilliseconds
 Ouputs:    None
 About:     Delay in milliseconds
 ==========================================================================}
procedure TfrmMain.Delay(dwMilliseconds: Longint);
var
  iStart, iStop: DWORD;
begin
  iStart := GetTickCount;
  repeat
    iStop := GetTickCount;
    Application.ProcessMessages;
  until (iStop - iStart) >= dwMilliseconds;
end;



{==========================================================================
 Function:  FormClose
 Inputs:    None
 Ouputs:    None
 About:     Prompt for save on form close event
 ==========================================================================}
procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Selected: Integer;
begin
  if FileChanged then
  begin
    Selected := messageDlg( 'Would you like to save the changes made to '
                + CurFileName ,mtConfirmation, mbYesNoCancel, 0);
    if Selected = mrYes then
      saveSettings();           // Save settings to file
    if Selected = mrCancel then
      Action := caNone;         // Do not exit on cancel
  end;
end;

{==========================================================================
 Function:  Wait
 Inputs:    seconds
 Ouputs:    None
 About:     Let the system idle for specified amount of seconds (uses time)
 ==========================================================================}
procedure TfrmMain.Wait(seconds: Integer);
var
  iStart, iStop : TDateTime;
begin
  iStart := Time;
  repeat
    iStop := Time;
    Application.ProcessMessages;
  until (SecondsBetween(iStop,iStart)) >= seconds;
end;




end.
{============================= End of file ================================}



