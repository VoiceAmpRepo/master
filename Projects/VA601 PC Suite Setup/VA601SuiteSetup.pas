unit VA601SuiteSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, URLLabel;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    URLLabel1: TURLLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    blt_1: TImage;
    blt_2: TImage;
    blt_3: TImage;
    blt_4: TImage;
    blt_5: TImage;
    Label10: TLabel;
    Label11: TLabel;
    Image8: TImage;
    Label12: TLabel;
    Label13: TLabel;
    Image9: TImage;
    Label14: TLabel;
    Label15: TLabel;
    Image10: TImage;
    blt_6: TImage;
    procedure Label2MouseEnter(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label3MouseEnter(Sender: TObject);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label4MouseEnter(Sender: TObject);
    procedure Label4MouseLeave(Sender: TObject);
    procedure Label5MouseEnter(Sender: TObject);
    procedure Label5MouseLeave(Sender: TObject);
    procedure Label5Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label10MouseEnter(Sender: TObject);
    procedure Label10MouseLeave(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label14MouseEnter(Sender: TObject);
    procedure Label14MouseLeave(Sender: TObject);
    procedure Label14Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses ShellApi;

{$R *.dfm}


{==========================================================================
 Procedure: Mouse Over events
 Inputs:    None
 Ouputs:    None
 About:     Make fancy buttons using mouse over events
 ==========================================================================}
procedure TForm1.Label2MouseEnter(Sender: TObject);
begin
  Label2.Font.Style := [fsBold];
  Label2.Font.Color := clGreen;
  blt_1.Visible := True;
end;
procedure TForm1.Label2MouseLeave(Sender: TObject);
begin
  Label2.font.Style := [];
  Label2.Font.Color := clBlack;
  blt_1.Visible := False;
end;
procedure TForm1.Label3MouseEnter(Sender: TObject);
begin
  Label3.Font.Style := [fsBold];
  Label3.Font.Color := clGreen;
  blt_2.Visible := True;
end;
procedure TForm1.Label3MouseLeave(Sender: TObject);
begin
  Label3.Font.Style := [];
  Label3.Font.Color := clBlack;
  blt_2.Visible := False;
end;
procedure TForm1.Label4MouseEnter(Sender: TObject);
begin
  Label4.Font.Style := [fsBold];
  Label4.Font.Color := clGreen;
  blt_3.Visible := True;
end;
procedure TForm1.Label4MouseLeave(Sender: TObject);
begin
  Label4.Font.Style := [];
  Label4.Font.Color := clBlack;
  blt_3.Visible := False;
end;
procedure TForm1.Label5MouseEnter(Sender: TObject);
begin
  Label5.Font.Style := [fsBold];
  Label5.Font.Color := clGreen;
  blt_4.Visible := True;
end;
procedure TForm1.Label5MouseLeave(Sender: TObject);
begin
  Label5.Font.Style := [];
  Label5.Font.Color := clBlack;
  blt_4.Visible := False;
end;
procedure TForm1.Label14MouseEnter(Sender: TObject);
begin
  Label14.Font.Style := [fsBold];
  Label14.Font.Color := clGreen;
  blt_6.Visible := True;
end;
procedure TForm1.Label14MouseLeave(Sender: TObject);
begin
  Label14.Font.Style := [];
  Label14.Font.Color := clBlack;
  blt_6.Visible := False;
end;
procedure TForm1.Label10MouseEnter(Sender: TObject);
begin
  Label10.Font.Style := [fsBold];
  Label10.Font.Color := clGreen;
  blt_5.Visible := True;
end;
procedure TForm1.Label10MouseLeave(Sender: TObject);
begin
  Label10.Font.Style := [];
  Label10.Font.Color := clBlack;
  blt_5.Visible := False;
end;



{==========================================================================
 Procedure: Label5Click
 Inputs:    None
 Ouputs:    None
 About:     Exit Application
 ==========================================================================}
procedure TForm1.Label5Click(Sender: TObject);
begin
  Application.Terminate;
end;



{==========================================================================
 Procedure: Label2Click
 Inputs:    None
 Ouputs:    None
 About:     Launch the CalWizard installer
 ==========================================================================}
procedure TForm1.Label2Click(Sender: TObject);
var
  appPath: String;
  s: String;
begin

  appPath := ExtractFilePath(Application.ExeName);  	// Get the application path

  s := appPath + 'Drivers\XP\DPInst.exe';            	// Pre-install driver
  ShellExecute(Handle, 'open', PChar(s) ,nil,nil,SW_SHOWNORMAL) ;

  s := appPath + 'Installs\setupcal.exe';            	// Run cal wiz installer
  ShellExecute(Handle, 'open', PChar(s) ,nil,nil,SW_SHOWNORMAL) ;

end;



{==========================================================================
 Procedure: Label2Click
 Inputs:    None
 Ouputs:    None
 About:     Launch the Control Panel installer
 ==========================================================================}
procedure TForm1.Label3Click(Sender: TObject);
var
  appPath: String;
  s: String;
begin

  appPath := ExtractFilePath(Application.ExeName);  	// Get the application path

  s := appPath + 'Drivers\XP\DPInst.exe';            	// Pre-install driver
  ShellExecute(Handle, 'open', PChar(s) ,nil,nil,SW_SHOWNORMAL);

  s := appPath + 'Installs\setuppnl.exe';            	// Run cal wiz installer
  ShellExecute(Handle, 'open', PChar(s) ,nil,nil,SW_SHOWNORMAL) ;
end;



{==========================================================================
 Procedure: Label2Click
 Inputs:    None
 Ouputs:    None
 About:     Launch the D2XX CDM driver installer
 ==========================================================================}
procedure TForm1.Label4Click(Sender: TObject);
var
  appPath: String;
  s: String;
begin
  appPath := ExtractFilePath(Application.ExeName);  	// Get the application path

  s := appPath + 'Drivers\XP feedback\DPInst.exe';      // Pre-install driver
  ShellExecute(Handle, 'open', PChar(s) ,nil,nil,SW_SHOWNORMAL);
end;


{==========================================================================
 Procedure: Label10Click
 Inputs:    None
 Ouputs:    None
 About:     Launch the Morning Live video clip
 ==========================================================================}
procedure TForm1.Label10Click(Sender: TObject);
var
  appPath: String;
  s: String;
begin
  appPath := ExtractFilePath(Application.ExeName);  	// Get the application path

  s := appPath + 'Media\';
  ShellExecute(Handle, 'open', PChar(s) ,nil,nil,SW_SHOWNORMAL);
end;



{==========================================================================
 Procedure: Label14Click
 Inputs:    None
 Ouputs:    None
 About:     Open the documentation folder
 ==========================================================================}
procedure TForm1.Label14Click(Sender: TObject);
var
  appPath: String;
  s: String;
begin
  appPath := ExtractFilePath(Application.ExeName);  	// Get the application path

  s := appPath + 'Documentation\';
  ShellExecute(Handle, 'open', PChar(s) ,nil,nil,SW_SHOWNORMAL);
end;

end.
