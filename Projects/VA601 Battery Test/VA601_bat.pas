unit VA601_bat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, CPort;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Memo1: TMemo;
    ComPort1: TComPort;
    procedure Timer1Timer(Sender: TObject);
    procedure ComPort1RxChar(Sender: TObject; Count: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    {Global variables}
    loop_busy: Boolean;             // Rx string complete flag
    RxString: String;               // Receive String

  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Memo1.Lines.SaveToFile('test.txt');
end;

procedure TForm1.ComPort1RxChar(Sender: TObject; Count: Integer);
var
 LastRxTick : longint;                              // For delay calculations
 Rx: String;                                        // Received buffer

 begin
  loop_busy := True;                                // Busy processing buffer

  LastRxTick := GetTickCount;                       // Time mark
  RxString := '';                                   // Reset receive string

  while loop_busy  do                               // Process buffer
  begin
    ComPort1.ReadStr(Rx,Count);                     // Get Rx data
    RxString := RxString+Rx;                        // Build receive string
    if ( (GetTickCount - LastRxTick) > 100) then    // Time elapsed?
    begin
      loop_busy := False;
      Memo1.Lines.Add(TimeToStr(Now) + #9 +  RxString);                    // Send string for parsing
    end;
  end;
end;

end.
