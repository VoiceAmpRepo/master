unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sSkinProvider, sSkinManager, ExtCtrls, sPanel,
  Buttons, sBitBtn, sGauge, Menus, jpeg, OleCtrls, SHDocVw,
  ComCtrls, frame_1, sButton, sLabel, TrCtrls;

type
  TfrmMain = class(TForm)
    sSkinManager1: TsSkinManager;
    sSkinProvider1: TsSkinProvider;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sPanel4: TsPanel;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    PageControl1: TPageControl;
    ts_MainMenu: TTabSheet;
    ts_Delay: TTabSheet;
    sLabel2: TsLabel;
    sPanel11: TsPanel;
    ts_Comms: TTabSheet;
    sButton1: TsButton;
    RichEdit1: TRichEdit;
    sLabel1: TsLabel;
    Image3: TImage;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    Image2: TImage;
    sButton3: TsButton;
    sButton4: TsButton;
    sButton5: TsButton;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    Image1: TImage;
    btnBack1: TsBitBtn;
    btnNext1: TsBitBtn;
    Image4: TImage;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    ts_Shift: TTabSheet;
    sLabel10: TsLabel;
    Image5: TImage;
    sBitBtn3: TsBitBtn;
    sBitBtn4: TsBitBtn;
    ts_ShiftUp: TTabSheet;
    sBitBtn5: TsBitBtn;
    sBitBtn6: TsBitBtn;
    sLabel12: TsLabel;
    ts_NoiseGate: TTabSheet;
    sLabel14: TsLabel;
    sBitBtn7: TsBitBtn;
    sBitBtn8: TsBitBtn;
    ts_Setup: TTabSheet;
    RichEdit2: TRichEdit;
    sLabel15: TsLabel;
    Image8: TImage;
    btn_Prepare: TsButton;
    lbl_Prepare_Feedback1: TsLabel;
    lbl_Prepare_Feedback2: TsLabel;
    tmr_TimeOut: TTimer;
    tmr_Connected: TTimer;
    rb_ShortDly: TTrRadioButton;
    rb_MediumDly: TTrRadioButton;
    rb_LongDly: TTrRadioButton;
    sButton2: TsButton;
    lbl_DelayIntro: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    re_CommFeedback: TRichEdit;
    sPanel15: TsPanel;
    TrRadioButton1: TTrRadioButton;
    TrRadioButton2: TTrRadioButton;
    TrRadioButton3: TTrRadioButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    sPanel12: TsPanel;
    TrRadioButton4: TTrRadioButton;
    TrRadioButton5: TTrRadioButton;
    TrRadioButton6: TTrRadioButton;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Image6: TImage;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    sPanel13: TsPanel;
    TrRadioButton7: TTrRadioButton;
    TrRadioButton8: TTrRadioButton;
    TrRadioButton9: TTrRadioButton;
    Label16: TLabel;
    Image7: TImage;
    sLabel11: TsLabel;
    ledOFF: TImage;
    ledON: TImage;
    procedure Exit1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure ts_CommsEnter(Sender: TObject);
    procedure btnNext1Click(Sender: TObject);
    procedure ts_DelayEnter(Sender: TObject);
    procedure btn_SendDelayClick(Sender: TObject);
    procedure btn_SendDwnShiftClick(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure ts_ShiftEnter(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure ts_ShiftUpEnter(Sender: TObject);
    procedure btn_SendUpShftClick(Sender: TObject);
    procedure ts_NoiseGateEnter(Sender: TObject);
    procedure btn_SendGateClick(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure ts_SetupEnter(Sender: TObject);
    procedure btn_PrepareClick(Sender: TObject);
    procedure tmr_TimeOutTimer(Sender: TObject);
    procedure tmr_ConnectedTimer(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sBitBtn8Click(Sender: TObject);

  private
    { Private declarations }
    procedure LoadSettings();
    procedure SetSettings();
    procedure sendUSBstring(s: String);
    procedure Delay(dwMilliseconds: Longint);

  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses D2XXUnit;

var

  DevicePresent : Boolean;                   // To check availability
  DeviceBusy : Boolean;                      // Flag for comms busy
  Device_Description : String = 'VoiceAmp 601';
  TimeOut: Boolean;                           // Comm timeout
  Count: Integer = 0;			      // General purpose counter

  //====== Simple state machine for page items ======//
  Mode: Integer;                    // Current mode
  Step: Integer;                    // Current step
  const MainMenu: Integer = 0;      // Main Menu active
  const Information: Integer = 1;   // Information pages active
  const Assesment: Integer = 2;     // Assesment pages active
  const Calibration: Integer = 3;   // Calibration pages active

  const USB_IS_OPEN: Integer = 1;
  const USB_NOT_PRESENT: Integer = 2;
  const USB_NEW_OPENED: Integer = 3;
{$R *.dfm}

procedure TfrmMain.Exit1Click(Sender: TObject);
begin
  Application.Terminate;
end;




{=============================================
  Procedure:  FormShow
  Purpose:    Setup stuff when starting
 =============================================}
procedure TfrmMain.FormShow(Sender: TObject);
begin
  FT_Enable_Error_Report := true; // Error reporting = on
  PageControl1.ActivePageIndex := 0;
  DevicePresent := False;         // Reset flag
  TimeOut := False;               // Reset comms flag
  LoadSettings();		  // Pre-Load USB settings
end;



{=============================================
  Procedure:  sButton4Click
  Purpose:    Assesment button
 =============================================}
procedure TfrmMain.sButton4Click(Sender: TObject);
begin
  PageControl1.ActivePage := ts_Comms;    // Show the fist step - comms setup
end;



{=============================================
  Procedure:  sButton4Click
  Purpose:    Home button
 =============================================}
procedure TfrmMain.Image3Click(Sender: TObject);
begin
  sPanel1.Visible := False;
  sPanel2.Visible := False;
  sPanel3.Visible := False;
  sPanel4.Visible := False;
  sPanel5.Visible := False;
  btnBack1.Visible := False;
  btnNext1.Visible := False;

  PageControl1.ActivePageIndex := 0;
end;



{==========================================================================
 Procedure: LoadSettings
 Inputs:    None
 Ouputs:    None
 About:     Pre-load the default comms settings into variables
 ==========================================================================}
procedure TfrmMain.LoadSettings();
begin
  FT_Current_Baud := FT_BAUD_115200;
  FT_Current_DataBits := FT_DATA_BITS_8;
  FT_Current_StopBits := FT_STOP_BITS_1;
  FT_Current_Parity := FT_PARITY_NONE;
  FT_Current_FlowControl := FT_FLOW_NONE;
end;



{==========================================================================
 Procedure: SetSettings
 Inputs:    None
 Ouputs:    None
 About:     Apply pre-loaded settings to current D2XX port
 ==========================================================================}
procedure TfrmMain.SetSettings();
var
  PortStatus : FT_Result;
begin
  PortStatus := Get_USB_Device_QueueStatus;  // Get the current port status
  if PortStatus = FT_OK then               // Port open, all good to update
  begin
    Set_USB_Device_BaudRate;
    Set_USB_Device_DataCharacteristics;
    Set_USB_Device_FlowControl;
    Set_USB_Device_TimeOuts(300,100);
  end;
end;


{==========================================================================
 Procedure: sendUSBstring
 Inputs:    s: String (String to be sent to USB port)
 Ouputs:    None
 About:     Send a string to the USB port
 ==========================================================================}
procedure TfrmMain.sendUSBstring(s: String);
var
  PortStatus: FT_Result;
  TxString: string;
  k,j,i: Integer;           // To store string length
begin
  if DevicePresent then
    PortStatus := Get_USB_Device_QueueStatus;   // Get port status

  if (PortStatus = FT_OK) and DevicePresent then
  begin
    DeviceBusy := True;
    TxString := s + #13#10;
    k := strlen(PChar(TxString));

    // Load string into out buffer
    for i:=0 to k do
      FT_Out_Buffer[i-1] := TxString[i];

    Application.ProcessMessages;
    j := Write_USB_Device_Buffer(k);
  end;
end;


{==========================================================================
 Procedure: sButton1Click
 Inputs:    None
 Ouputs:    None
 About:     First step in assesment - check comms
 ==========================================================================}
procedure TfrmMain.sButton1Click(Sender: TObject);
var
  status: Integer;
  s: String;
  Rx:String;
begin
  re_CommFeedback.Clear;
  re_CommFeedback.SelAttributes.Style := [];
  re_CommFeedback.SelAttributes.Color := clBlack;

  re_CommFeedback.SelAttributes.Color := clBlack;     // Set text to black
  re_CommFeedback.Lines.Add('Looking for connected devices...'+#13+#10);

  Delay(500);	// Wait a bit

  if DevicePresent then
  begin
    re_CommFeedback.SelAttributes.Style := [];
    re_CommFeedback.SelAttributes.Color := clBlue;     // Set text to blue
    re_CommFeedback.Lines.Add('Found device, connecting...');
    re_CommFeedback.SelAttributes.Style := [];
    re_CommFeedback.SelAttributes.Color := clBlue;     // Set text to blue

    if GetFTDeviceSerialNo(0) = FT_OK then
      re_CommFeedback.Lines.Add('Serial number: ' + FT_Device_String+#13+#10)
    else
      re_CommFeedback.Lines.Add('Unable to read Serial number. '+#13+#10);


    re_CommFeedback.SelAttributes.Style := [];
    re_CommFeedback.SelAttributes.Color := clBlue;     // Set text to blue
    re_CommFeedback.Lines.Add('Sending test command:');
    Delay(200);	// Wait a bit

    FT_In_Buffer := '';   				// Clear the USB buffer

    sendUSBstring('AT+BATVS=?'+ #13#10);  		// Request battery voltage

    if Read_USB_Device_Buffer(8) > 0 then
      Rx := Trim(FT_In_Buffer);     			// Read USB data

    if Rx = 'AT+BATVS' then
    begin
      re_CommFeedback.SelAttributes.Style := [fsBold];
      re_CommFeedback.SelAttributes.Color := clBlue;
      re_CommFeedback.Lines.Add('Communication OK!');
    end
    else begin
      re_CommFeedback.SelAttributes.Style := [fsBold];
      re_CommFeedback.SelAttributes.Color := clRed;
      re_CommFeedback.Lines.Add('Unable to connect to device!');
   end;


    btnNext1.Enabled := True;
  end
  else
  begin
    re_CommFeedback.Lines.Add('');
    re_CommFeedback.Lines.Add('Please check that the device is correctly plugged in and the driver is installed.');
  end;
{
  case status of
  2:
    begin
      lbl_Feedback1.Font.Color := clRed;
      lbl_Feedback1.Caption := 'No VA601 devices found.';
      lbl_Feedback2.Caption := 'Make sure that the unit is plugged in and the drivers are correctly installed.';
    end;
  3:
    begin
      DevicePresent := True;
      s := FT_Device_String;                // Save the serial number
      lbl_Feedback1.Caption := 'VA601 serial number ' + s + ' is connected.';
      FT_In_Buffer := '';   // Clear the USB buffer
      sendUSBstring('AT+BATVS=?'+ #13#10);  // Request battery voltage
      if Read_USB_Device_Buffer(8) > 0 then
        Rx := Trim(FT_In_Buffer);     // Read USB data
      if Rx = 'AT+BATVS' then
      begin
        lbl_Feedback2.Font.Color := clGreen;
        lbl_Feedback2.Caption := 'Communication successfull!';
        lbl_Feedback3.Caption := 'Press "Next" to continue';
        btnNext1.Enabled := True;
      end
      else
      begin
        DevicePresent := True;
        lbl_Feedback2.Font.Color := clRed;
        lbl_Feedback2.Caption := 'Communication failed!';
        lbl_Feedback3.Caption := 'Make sure the USB driver is correctly installed.';
     end;
    end;
  end;
}

end;



{==========================================================================
 Procedure: ts_CommsEnter
 Inputs:    None
 Ouputs:    None
 About:     Setup controls for the Comms test screen
 ==========================================================================}
procedure TfrmMain.ts_CommsEnter(Sender: TObject);
begin
  Mode := Assesment;          // Put into assesment mode

  sPanel1.Visible := True;    // Show the progress panel up to 5 bars
  sPanel2.Visible := True;
  sPanel3.Visible := True;
  sPanel4.Visible := True;
  sPanel5.Visible := True;
  sPanel6.Visible := True;

  sPanel1.Font.Style := [fsBold];               // Show current step
  sPanel1.Font.Size := 12;
  sPanel1.SkinData.SkinSection := 'BAR';

  btnBack1.Visible := True;      // Show buttons
  btnNext1.Visible := True;

  re_CommFeedback.Lines.Clear;	 // Clear the feedback box
  
end;



{==========================================================================
 Procedure: btnNext1Click
 Inputs:    None
 Ouputs:    None
 About:     Comms page "Next" button handler
 ==========================================================================}
procedure TfrmMain.btnNext1Click(Sender: TObject);
begin
  PageControl1.ActivePage := ts_Setup;    // Show the setup screen
end;



{==========================================================================
 Procedure: ts_DelayEnter
 Inputs:    None
 Ouputs:    None
 About:     Setup controls for the DAF test screen
 ==========================================================================}
procedure TfrmMain.ts_DelayEnter(Sender: TObject);
begin
  rb_ShortDly.Transparent := True;	// Make radion buttons transparant
  rb_MediumDly.Transparent := True;
  rb_LongDly.Transparent := True;

  sPanel3.Font.Style := [fsBold];               // Show current step
  sPanel3.Font.Size := 12;
  sPanel3.SkinData.SkinSection := 'BAR';
end;



{==========================================================================
 Procedure: btn_SendDelayClick
 Inputs:    None
 Ouputs:    None
 About:     Delay update button handler
 ==========================================================================}
procedure TfrmMain.btn_SendDelayClick(Sender: TObject);
begin
  sendUSBstring('AT+DRYST=0'+ #13#10);      // Mute clean channel
  Delay(100);
  sendUSBstring('AT+WETST=15000'+ #13#10);  // Set effect channel gain
  Delay(100);
  sendUSBstring('AT+MSTST=15000'+ #13#10);  // Set master channel gain
  Delay(100);
  sendUSBstring('AT+NGTST=0'+ #13#10);      // Turn off noise gate threshold
  Delay(100);
  sendUSBstring('AT+SFTST=0'+ #13#10);      // Turn off pitch shift
  Delay(100);
end;



{==========================================================================
 Function:  Delay
 Inputs:    dwMilliseconds
 Ouputs:    None
 About:     Delay in milliseconds
 ==========================================================================}
procedure TfrmMain.Delay(dwMilliseconds: Longint);
var
  iStart, iStop: DWORD;
begin
  iStart := GetTickCount;
  repeat
    iStop := GetTickCount;
    Application.ProcessMessages;
  until (iStop - iStart) >= dwMilliseconds;
end;



{==========================================================================
 Function:  btn_SendDwnShiftClick
 Inputs:    None
 Ouputs:    None
 About:     Downward frequency shift send button
 ==========================================================================}
procedure TfrmMain.btn_SendDwnShiftClick(Sender: TObject);
begin
  sendUSBstring('AT+DRYST=0'+ #13#10);      // Mute clean channel
  Delay(100);
  sendUSBstring('AT+WETST=15000'+ #13#10);  // Set effect channel gain
  Delay(100);
  sendUSBstring('AT+MSTST=15000'+ #13#10);  // Set master channel gain
  Delay(100);
  sendUSBstring('AT+NGTST=0'+ #13#10);      // Turn off noise gate threshold
  Delay(100);
  sendUSBstring('AT+DLYST=1'+ #13#10);      // Set delay to 0msec
  Delay(100);
end;



{==========================================================================
 Function:  sBitBtn2Click
 Inputs:    None
 Ouputs:    None
 About:     Show FSF downward page
 ==========================================================================}
procedure TfrmMain.sBitBtn2Click(Sender: TObject);
begin
  PageControl1.ActivePage := ts_Shift;    // Show the delay screen
end;



{==========================================================================
 Function:  ts_ShiftEnter
 Inputs:    None
 Ouputs:    None
 About:     Setup the downward shift page
 ==========================================================================}
procedure TfrmMain.ts_ShiftEnter(Sender: TObject);
begin
  sPanel4.Font.Style := [fsBold];               // Show current step
  sPanel4.Font.Size := 12;
  sPanel4.SkinData.SkinSection := 'BAR';
end;


{==========================================================================
 Function:  sBitBtn4Click
 Inputs:    None
 Ouputs:    None
 About:     Show FSF upward page
 ==========================================================================}
procedure TfrmMain.sBitBtn4Click(Sender: TObject);
begin
  PageControl1.ActivePage := ts_ShiftUp;    // Show the delay screen
end;



{==========================================================================
 Function:  ts_ShiftUpEnter
 Inputs:    None
 Ouputs:    None
 About:     Setup the upward shift page
 ==========================================================================}
procedure TfrmMain.ts_ShiftUpEnter(Sender: TObject);
begin
  sPanel5.Font.Style := [fsBold];               // Show current step
  sPanel5.SkinData.SkinSection := 'BAR';
  sPanel5.Font.Size := 12;
end;


{==========================================================================
 Function:  btn_SendUpShftClick
 Inputs:    None
 Ouputs:    None
 About:     Upward frequency shift send button
 ==========================================================================}
procedure TfrmMain.btn_SendUpShftClick(Sender: TObject);
begin
  sendUSBstring('AT+DRYST=0'+ #13#10);      // Mute clean channel
  Delay(100);
  sendUSBstring('AT+WETST=30000'+ #13#10);  // Set effect channel gain
  Delay(100);
  sendUSBstring('AT+MSTST=15000'+ #13#10);  // Set master channel gain
  Delay(100);
  sendUSBstring('AT+NGTST=0'+ #13#10);      // Turn off noise gate threshold
  Delay(100);
  sendUSBstring('AT+DLYST=1'+ #13#10);      // Set delay to 0msec
  Delay(100);
end;



{==========================================================================
 Function:  ts_NoiseGateEnter
 Inputs:    None
 Ouputs:    None
 About:     Setup the noise gate page
 ==========================================================================}
procedure TfrmMain.ts_NoiseGateEnter(Sender: TObject);
begin
  sPanel6.Font.Style := [fsBold];               // Show current step
  sPanel6.SkinData.SkinSection := 'BAR';
  sPanel6.Font.Size := 12;
end;



{==========================================================================
 Function:  btn_SendGateClick
 Inputs:    None
 Ouputs:    None
 About:     Upward frequency shift send button
 ==========================================================================}
procedure TfrmMain.btn_SendGateClick(Sender: TObject);
begin
  sendUSBstring('AT+DRYST=0'+ #13#10);      // Mute clean channel
  Delay(100);
  sendUSBstring('AT+WETST=30000'+ #13#10);  // Set effect channel gain
  Delay(100);
  sendUSBstring('AT+MSTST=15000'+ #13#10);  // Set master channel gain
  Delay(100);
  sendUSBstring('AT+DLYST=50'+ #13#10);      // Set delay to 50msec
  Delay(100);
  sendUSBstring('AT+SFTST=400'+ #13#10);      // Shift 680Hz up
  Delay(100);
end;



{==========================================================================
 Function:  sBitBtn5Click
 Inputs:    None
 Ouputs:    None
 About:     Show the noisegate screen
 ==========================================================================}
procedure TfrmMain.sBitBtn5Click(Sender: TObject);
begin
  PageControl1.ActivePage := ts_NoiseGate;    // Show the NoiseGate screen
end;



{==========================================================================
 Function:  ts_SetupEnter
 Inputs:    None
 Ouputs:    None
 About:     Setup the preparation screen
 ==========================================================================}
procedure TfrmMain.ts_SetupEnter(Sender: TObject);
begin
  lbl_Prepare_Feedback1.Caption := '';
  lbl_Prepare_Feedback2.Caption := '';

  sPanel2.Font.Style := [fsBold];               // Show current step
  sPanel2.SkinData.SkinSection := 'BAR';
  sPanel2.Font.Size := 12;
end;



{==========================================================================
 Function:  btn_PrepareClick
 Inputs:    None
 Ouputs:    None
 About:     Prepare the VA601 for assesment
 ==========================================================================}
procedure TfrmMain.btn_PrepareClick(Sender: TObject);
var
  Rx:String;
begin

 FT_In_Buffer := '';                    // Clear the USB buffer
 sendUSBstring('AT+DRYST=0'+ #13#10);   // Mute clean channel
 while ( Read_USB_Device_Buffer(10) < 10 ) do
   Application.ProcessMessages;         // Wait for response
 Rx := Trim(FT_In_Buffer);              // Read USB data


 sendUSBstring('AT+WETST=30000'+ #13#10);  // Set effect channel gain
 while ( Read_USB_Device_Buffer(14) < 14 ) do
   Application.ProcessMessages;         // Wait for response
   Rx := Trim(FT_In_Buffer);            // Read USB data


 sendUSBstring('AT+MSTST=0'+ #13#10);   // Mute master
 while ( Read_USB_Device_Buffer(10) < 10 ) do
   Application.ProcessMessages;         // Wait for response
   Rx := Trim(FT_In_Buffer);     // Read USB data


 lbl_Prepare_Feedback1.Caption := 'Done';
end;



{==========================================================================
 Function:  tmr_TimeOutTimer
 Inputs:    None
 Ouputs:    None
 About:     Set TimeOut flag on timer event
 ==========================================================================}
procedure TfrmMain.tmr_TimeOutTimer(Sender: TObject);
begin
  TimeOut := True;
  tmr_TimeOut.Enabled := False;
end;




{
procedure TfrmMain.tmr_ConnectedTimer(Sender: TObject);
var
  PortStatus : FT_Result;
begin
  if GetFTDeviceCount = FT_OK then
  begin

    if (FT_Device_Count > 0) and DevicePresent then
    begin
      lbl_Connected.Caption := 'Connected';
    end

    else if (FT_Device_Count > 0) and (not DevicePresent) then
    begin
      LoadSettings;
      Delay(500);
      lbl_Connected.Caption := 'Connected';

      if Open_USB_Device_By_Device_Description(Device_Description) = FT_OK then
      begin
        SetSettings;
        DevicePresent := True;
      end;
    end
    else begin
      lbl_Connected.Caption := 'Not Connected';
      DevicePresent := False;
    end;

 end;
end;
}
{==========================================================================
 Procedure: tmr_ConnectedTimer
 Inputs:    None
 Ouputs:    None
 About:     Check for device availability on timer event
 ==========================================================================}
procedure TfrmMain.tmr_ConnectedTimer(Sender: TObject);
var
  PortStatus: FT_Result;
  Devices: Integer;
  s: String;
begin

  // Count devices
  GetFTDeviceCount;

  if ( FT_Device_Count > 0 ) and ( not DevicePresent ) then
  begin
    // Check for a handle and close it
    if FT_HANDLE <> 0 then
    begin
      if Close_USB_Device = FT_OK then
      begin
        FT_HANDLE := 0;
      end;
    end;
    if GetFTDeviceDescription(0) = FT_OK then
    begin
      if Open_USB_Device_By_Device_Description(Device_Description) = FT_OK then
      begin
        ledON.Visible := True;
    	DevicePresent := True;
      end;
    end;
  end
  else if ( FT_Device_Count = 0 ) then
  begin
    ledON.Visible := False;
    DevicePresent := False;
  end;



{
  if ( DevicePresent ) then
  begin
    if Get_USB_Device_QueueStatus = FT_OK then
    begin
        ledON.Visible := True;
    	DevicePresent := True;
    end
    else
    begin
        ledON.Visible := False;
    	DevicePresent := False;
    end;
  end
  else
  begin
    GetFTDeviceCount;
    if (FT_Device_Count > 0) then
    begin
      if Open_USB_Device_By_Device_Description(Device_Description) = FT_OK then
      begin
        ledON.Visible := True;
    	DevicePresent := True;
      end;
    end;
  end;
}
  {
  if ( not DevicePresent ) then
  begin
    GetFTDeviceCount;
    if (FT_Device_Count > 0) then
    begin
      if Open_USB_Device_By_Device_Description(Device_Description) = FT_OK then
      begin
        ledON.Visible := True;
    	DevicePresent := True;
      end;
    end
    else
    begin
      ledON.Visible := False;
      DevicePresent := False;
    end;
  end;
  }
  {
  GetFTDeviceCount;

  if (FT_Device_Count > 0) and (not DevicePresent) then
  begin
    //GetFTDeviceDescription(0);
    Open_USB_Device_By_Device_Description(Device_Description);
    //PortStatus := Open_USB_Device();
    //GetFTDeviceDescription(0);
    DevicePresent := True;
    //showMessage(FT_Device_String);
    ledON.Visible := True;
  end;
  if ( FT_Device_Count=0 ) then
  begin
    if FT_HANDLE <> 0 then
    begin
      Close_USB_Device;
      FT_HANDLE := 0;
    end;
    ledON.Visible := False;
    DevicePresent := False;
  end;
  }
end;



{==========================================================================
 Procedure: tmr_ConnectedTimer
 Inputs:    None
 Ouputs:    None
 About:     Check for device availability on timer event
 ==========================================================================}
procedure TfrmMain.sButton2Click(Sender: TObject);
begin
  PageControl1.ActivePage := ts_Delay;    // Show the setup screen
end;



{==========================================================================
 Procedure: sBitBtn8Click
 Inputs:    None
 Ouputs:    None
 About:     Show Done screen
 ==========================================================================}
procedure TfrmMain.sBitBtn8Click(Sender: TObject);
begin
  PageControl1.ActivePage := ts_MainMenu;    // Return to main menu
end;


end.

