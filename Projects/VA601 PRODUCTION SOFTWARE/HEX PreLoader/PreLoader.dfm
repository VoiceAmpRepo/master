object Form1: TForm1
  Left = 730
  Top = 365
  Width = 352
  Height = 154
  Caption = 'VoiceAmp HEX Pre-Loader'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 248
    Top = 81
    Width = 68
    Height = 13
    Caption = 'Device Ready'
  end
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 226
    Height = 20
    Caption = 'HEX Pre-Loader Application'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 101
    Width = 344
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 150
      end>
  end
  object Led1: TJSILed
    Left = 320
    Top = 80
    Width = 12
    Height = 15
    Position = poTop
    TrueColor = clLime
    FalseColor = clGray
  end
  object Run: TButton
    Left = 8
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Run'
    Enabled = False
    TabOrder = 2
    OnClick = RunClick
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 288
    Top = 40
  end
end
