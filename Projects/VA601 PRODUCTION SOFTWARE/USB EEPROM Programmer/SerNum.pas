unit SerNum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask;

type
  TForm1 = class(TForm)
    SerNum: TMaskEdit;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  s: String;
  SerLen: Integer;
begin
 s := SerNum.Text;

 if Length(Trim(s)) < 11 then
   MessageDlg('Incorrect Serial Number Format!',mtError, [mbOK], 0)
 else
   ShowMessage(SerNum.Text);
end;

end.
