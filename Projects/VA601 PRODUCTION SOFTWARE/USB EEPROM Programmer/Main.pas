{==========================================================================
 Project:   PS_VA601_USB_EEPROM
 About:     VA601i Production USB EEPROM Programmer
 Author:    Andr� Hoek
 Copyright: VoiceAmp (Pty) Ltd

 Notes:
 ->> Exclude USB Programmer from Device List!!

 Revision history:
  Rev         Date          Comments
  ---         ----          --------
              25 Oct 2007   First Release
 ==========================================================================}

unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  ExtCtrls, StdCtrls, StdActns, ActnList, ToolWin, ActnMan,
  ActnCtrls, ActnMenus, XPStyleActnCtrls, Menus, Mask, ComCtrls;



type
  Tfrm_Main = class(TForm)
    ActionManager1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    FileExit1: TFileExit;
    HelpAbout: TAction;
    Enter: TAction;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label10: TLabel;
    edt_EE_PDesc: TEdit;
    edt_EE_MnfID: TEdit;
    edt_EE_PID: TEdit;
    edt_EE_VID: TEdit;
    edt_EE_VCP: TEdit;
    edt_EE_ShowSerNr: TEdit;
    edt_EE_SerNr: TEdit;
    Button1: TButton;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    SerNum: TMaskEdit;
    Button4: TButton;
    Button5: TButton;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    edt_EmployNr: TMaskEdit;
    Button2: TButton;
    Button3: TButton;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    Button6: TButton;
    Button7: TButton;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    Edit1: TEdit;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    edt_EE_VID_Int: TEdit;
    edt_EE_PID_Int: TEdit;
    Button12: TButton;
    Button8: TButton;
    Label5: TLabel;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HelpAboutExecute(Sender: TObject);
    procedure EnterExecute(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure GroupBox5Enter(Sender: TObject);
    procedure GroupBox2Enter(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);

  private
    { Private declarations }
    procedure LoadIni();
    procedure WriteLog();
    procedure ProgramEE();

  public
    { Public declarations }
    DevicePresent : Boolean;                   // To check availability

  end;

var
  frm_Main: Tfrm_Main;
  ProgramOK: Boolean;

  // EEPROM Value Storage
  VendorID: Integer;
  ProductID: Integer;
  Manufacturer: String;
  ProductDesc: String;
  ShowVCP: Integer;
  SerialHide: Integer;
  SerialNumber: String;

  EmployeeNumber: String;

implementation

uses
D2XXUnit, MahFileVer, About, INIfiles;

{$R *.dfm}



{==========================================================================
 Procedure: Button1Click
 Inputs:    None
 Ouputs:    None
 About:     Test Routine
 =========================================================================}
procedure Tfrm_Main.LoadIni();
var
  IniFile: TIniFile;
  i: integer;
  path: String;
begin
  i:=0;
  GetDir(0, path);
  IniFile := TIniFile.Create(path+'/Settings.ini');

  VendorID := IniFile.ReadInteger('EEPROM','VendorID',i);
  ProductID := IniFile.ReadInteger('EEPROM','ProductID',i);
  Manufacturer := IniFile.ReadString('EEPROM','Manufacturer','');
  ProductDesc := IniFile.ReadString('EEPROM','ProductDesc','');
  ShowVCP := IniFile.ReadInteger('EEPROM','ShowVCP',i);
  SerialHide := IniFile.ReadInteger('EEPROM','SerialHide',i);
  SerialNumber := IniFile.ReadString('EEPROM','SerialNumber','');

end;




{==========================================================================
 Procedure: Button1Click
 Inputs:    None
 Ouputs:    None
 About:     Test Routine
 ==========================================================================}
procedure Tfrm_Main.Button1Click(Sender: TObject);
var
  PortStatus : FT_Result;
  DevCnt: Integer;
  i: Integer;
begin
  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;

  DevCnt := FT_Device_Count;

  if DevCnt > 0 then
  begin
    for i:=0 to (DevCnt-1) do
    begin
      PortStatus := GetFTDeviceDescription(i);
      if PortStatus <> FT_OK then exit;
      if FT_Device_String = 'ASIX PRESTO' then
      DevCnt := DevCnt-1;
      if DevCnt > 0 then
      begin
        PortStatus := Open_USB_Device_By_Device_Description(FT_Device_String);//Open_USB_Device;
        if PortStatus <> FT_OK then exit;

        PortStatus := USB_FT_R_EE_Read;
        if PortStatus <> FT_OK then exit;

        edt_EE_VID.Text := IntToHex((EEDataBuffer.VendorId),4);
        edt_EE_VID_Int.Text := IntToStr(EEDataBuffer.VendorId);
        edt_EE_PID.Text := IntToHex((EEDataBuffer.ProductID),4);
        edt_EE_PID_Int.Text := IntToStr(EEDataBuffer.ProductID);
        edt_EE_MnfID.Text := EEDataBuffer.Manufacturer;
        edt_EE_PDesc.Text := EEDataBuffer.Description;
        edt_EE_VCP.Text := IntToStr(EEDataBuffer.RIsVCP);
        edt_EE_ShowSerNr.Text := IntToStr(EEDataBuffer.SerNumEnableR);
        edt_EE_SerNr.Text := EEDataBuffer.SerialNumber;
        PortStatus := Close_USB_Device;
        if PortStatus <> FT_OK then exit;
      end
      else
        MessageDlg('No Devices available.',mtError, [mbOK], 0);
    end;
  end
  else
   MessageDlg('No Devices available.',mtError, [mbOK], 0);
end;



{==========================================================================
 Procedure: FormShow
 Inputs:    None
 Ouputs:    None
 About:     Write form caption
 ==========================================================================}
procedure Tfrm_Main.FormShow(Sender: TObject);
var
  oVer : TFileVersion;
  ver : String;
begin
  oVer := TFileVersion.Create;
  ver := oVer.AsString;
  oVer.Destroy;
  frm_Main.Caption := UpperCase(Application.Title) + ' ' + ver;

  PageControl1.ActivePageIndex := 0;

  edt_EmployNr.SetFocus;
//  FT_Enable_Error_Report := False; // Error reporting = on

  LoadIni();    // Load EEPROM values from file
end;



{==========================================================================
 Procedure: HelpAboutExecute
 Inputs:    None
 Ouputs:    None
 About:     Show about box
 ==========================================================================}
procedure Tfrm_Main.HelpAboutExecute(Sender: TObject);
begin
  frm_About.Show;
end;



{==========================================================================
 Procedure: EnterExecute
 Inputs:    None
 Ouputs:    None
 About:     Program serial numbers on Enter press
 ==========================================================================}
procedure Tfrm_Main.EnterExecute(Sender: TObject);
var
  buttonSelected : integer;
  programNext: integer;
  IncNum: Integer;
  s: String;
begin
  if not ProgramOK then
  begin
    buttonSelected := MessageDlg('Are you sure you want to program a sequence starting at ' + SerialNumber +' ?',mtConfirmation, mbOKCancel, 0);
    if buttonSelected = mrOK     then ProgramOK := True;
    if buttonSelected = mrCancel then ProgramOK := False;
  end;
  if ProgramOK then
  begin
    ProgramEE;
    beep;
    IncNum := StrToInt(Copy(SerialNumber,8,4));  // Save sequential part of #
    programNext := MessageDlg(SerialNumber + ' correctly programmed!'
                                            + #13#10
                                            + 'Plug in new unit and press OK to continue'
                                            + #13#10
                                            + 'or Cancel to exit.',mtInformation, mbOKCancel, 0);
    if programNext = mrOK then
    begin
      WriteLog;
      IncNum := IncNum+1;                 // Increment serial number
      s := Copy(SerialNumber,0,7);
      SerialNumber := s + Format('%.4d',[IncNum]);
      Edit1.Text := SerialNumber;
    end;
    if programNext = mrCancel then
    begin
       PageControl1.ActivePageIndex := 1;
    end;
  end;
end;



{==========================================================================
 Procedure: Button4Click
 Inputs:    None
 Ouputs:    None
 About:     Program serial numbers on Enter press
 ==========================================================================}
procedure Tfrm_Main.Button4Click(Sender: TObject);
var
  s: String;
begin
 s := SerNum.Text;
 if Length(Trim(s)) < 11 then
   MessageDlg('Incorrect Serial Number Format!',mtError, [mbOK], 0)
 else
 begin
   SerialNumber := Trim(s);
   PageControl1.ActivePageIndex := 3;
 end;
end;



{==========================================================================
 Procedure: Button2Click
 Inputs:    None
 Ouputs:    None
 About:     Employee Number Save
 ==========================================================================}
procedure Tfrm_Main.Button2Click(Sender: TObject);
begin
  if Length(Trim(edt_EmployNr.Text)) < 2 then
   MessageDlg('Incorrect Employee Number Format!'+
                #13+#10 + 'Please Enter a number longer than 2 digits.',mtError, [mbOK], 0)
 else
 begin
   EmployeeNumber := Trim(edt_EmployNr.Text);   // Save Employee Number
   PageControl1.ActivePageIndex := 1;
 end;
end;



{==========================================================================
 Procedure: Button6Click
 Inputs:    None
 Ouputs:    None
 About:     Goto Serial Number Programmer
 ==========================================================================}
procedure Tfrm_Main.Button6Click(Sender: TObject);
begin
   PageControl1.ActivePageIndex := 2;
end;



{==========================================================================
 Procedure: Button7Click
 Inputs:    None
 Ouputs:    None
 About:     Goto EEPROM Reader
 ==========================================================================}
procedure Tfrm_Main.Button7Click(Sender: TObject);
begin
   PageControl1.ActivePageIndex := 4;
end;



{==========================================================================
 Procedure: Button10Click
 Inputs:    None
 Ouputs:    None
 About:     Goto Menu
 ==========================================================================}
procedure Tfrm_Main.Button10Click(Sender: TObject);
begin
   PageControl1.ActivePageIndex := 1;
end;



{==========================================================================
 Procedure: Button11Click
 Inputs:    None
 Ouputs:    None
 About:     Goto Menu
 ==========================================================================}
procedure Tfrm_Main.Button11Click(Sender: TObject);
begin
   PageControl1.ActivePageIndex := 1;
end;



{==========================================================================
 Procedure: GroupBox5Enter
 Inputs:    None
 Ouputs:    None
 About:     Show selected serial number
 ==========================================================================}
procedure Tfrm_Main.GroupBox5Enter(Sender: TObject);
begin
  Edit1.Text := SerialNumber;
end;

procedure Tfrm_Main.GroupBox2Enter(Sender: TObject);
begin
  edt_EE_VID.Text := '';
  edt_EE_VID_Int.Text := '';
  edt_EE_PID.Text := '';
  edt_EE_PID_Int.Text := '';
  edt_EE_MnfID.Text := '';
  edt_EE_PDesc.Text := '';
  edt_EE_VCP.Text := '';
  edt_EE_ShowSerNr.Text := '';
  edt_EE_SerNr.Text := '';
  Button1.SetFocus;
end;



 {==========================================================================
 Procedure: Button8Click
 Inputs:    None
 Ouputs:    None
 About:     Go back to previous screen
 ==========================================================================}
procedure Tfrm_Main.Button8Click(Sender: TObject);
begin
    ShowVCP := 1;
    SerialHide := 1;
    SerialNumber := 'TEST_HEX';

    ProgramEE();
    showMessage('Done!');
end;



{==========================================================================
 Procedure: Button9Click
 Inputs:    None
 Ouputs:    None
 About:     Goto Menu
 ==========================================================================}
procedure Tfrm_Main.Button9Click(Sender: TObject);
begin
   PageControl1.ActivePageIndex := 1;
end;



{==========================================================================
 Procedure: WriteLog
 Inputs:    None
 Ouputs:    None
 About:     Save data to log file
 ==========================================================================}
procedure Tfrm_Main.WriteLog();
var
  F: TextFile;
  path: String;
  s: String;
begin
    s:= SerialNumber + ',' +
        EmployeeNumber + ',' +
        IntToHex((VendorId),4) + ',' +
        IntToHex((ProductID),4) + ',' +
        Manufacturer + ',' +
        ProductDesc + ',' +
        IntToStr(ShowVCP) + ',' +
        IntToStr(SerialHide) + ',' +
        DateTimeToStr(now);
    GetDir(0, path);                    // Get the working directory
    AssignFile(F, path + '\log.txt');   // Open text file
    if FileExists(path + '\log.txt') then
      Append(F)
    else
      Rewrite(F);
    WriteLn(F, s);
    CloseFile(F);
end;



{==========================================================================
 Procedure: ProgramEE
 Inputs:    None
 Ouputs:    None
 About:     Program device EEPROM
 ==========================================================================}
procedure Tfrm_Main.ProgramEE();
var
  PortStatus : FT_Result;
  DevCnt: Integer;
  i: Integer;
begin
  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;

  DevCnt := FT_Device_Count;

  if DevCnt > 0 then
  begin
    for i:=0 to (DevCnt-1) do
    begin
      PortStatus := GetFTDeviceDescription(i);
      if PortStatus <> FT_OK then exit;
      if FT_Device_String = 'ASIX PRESTO' then   // Make sure it's not the programmer!
        DevCnt := DevCnt-1;
      if DevCnt > 0 then
      begin


        // =========== Load EE variables ===========================
        EEDataBuffer.Version := 2;  // 0 for AM/BM, 1 for C, 2 for R
        EEDataBuffer.Signature1 := 0;
        EEDataBuffer.Signature2 := 4294967295;
        EEDataBuffer.Version := 2;  // 0 for AM/BM, 1 for C, 2 for R
        EEDataBuffer.VendorId := 1027;
        EEDataBuffer.ProductID := 24577;
        EEDataBuffer.Manufacturer := 'VoiceAmp';
        EEDataBuffer.ManufacturerID := 'FT';
        EEDataBuffer.Description := 'VoiceAmp 601';
        EEDataBuffer.SerialNumber := 'VA_TEST_HEX';
        EEDataBuffer.MaxPower := 100;
        EEDataBuffer.PnP := 1;
        EEDataBuffer.SelfPowered := 0;
        EEDataBuffer.RemoteWakeup := 0;
        EEDataBuffer.Rev4 := 0;
        EEDataBuffer.IsoIn := 0;
        EEDataBuffer.IsoOut := 0;
        EEDataBuffer.PullDownEnable := 0;
        EEDataBuffer.SerNumEnable := 0;
        EEDataBuffer.USBVersionEnable := 0;
        EEDataBuffer.USBVersion := 272;
        EEDataBuffer.Rev5 := 0;
        EEDataBuffer.IsoInA := 0;
        EEDataBuffer.IsoInB := 0;
        EEDataBuffer.IsoOutA := 0;
        EEDataBuffer.IsoOutB := 0;
        EEDataBuffer.PullDownEnable5 := 0;
        EEDataBuffer.SerNumEnable5 := 0;
        EEDataBuffer.USBVersionEnable5 := 0;
        EEDataBuffer.USBVersion5 := 0;
        EEDataBuffer.AIsHighCurrent := 0;
        EEDataBuffer.BIsHighCurrent := 0;
        EEDataBuffer.IFAIsFifo := 0;
        EEDataBuffer.IFAIsFifoTar := 0;
        EEDataBuffer.IFAIsFastSer := 0;
        EEDataBuffer.AIsVCP := 0;
        EEDataBuffer.IFBIsFifo := 0;
        EEDataBuffer.IFBIsFifoTar := 0;
        EEDataBuffer.IFBIsFastSer := 0;
        EEDataBuffer.BIsVCP := 0;
        EEDataBuffer.UseExtOsc := 0;
        EEDataBuffer.HighDriveIOs := 0;
        EEDataBuffer.EndpointSize := 64;
        EEDataBuffer.PullDownEnableR := 0;
        EEDataBuffer.SerNumEnableR := 1;
        EEDataBuffer.InvertTXD := 0;
        EEDataBuffer.InvertRXD := 0;
        EEDataBuffer.InvertRTS := 0;
        EEDataBuffer.InvertCTS := 0;
        EEDataBuffer.InvertDTR := 0;
        EEDataBuffer.InvertDSR := 0;
        EEDataBuffer.InvertDCD := 0;
        EEDataBuffer.InvertRI := 0;
        EEDataBuffer.Cbus0 := 15;
        EEDataBuffer.Cbus1 := 15;
        EEDataBuffer.Cbus2 := 15;
        EEDataBuffer.Cbus3 := 15;
        EEDataBuffer.Cbus4 := 15;
        EEDataBuffer.RIsVCP := 0;
       // =========== Load EE variables ===========================




        PortStatus := Open_USB_Device_By_Device_Description(FT_Device_String);
        if PortStatus <> FT_OK then exit;

        //PortStatus := Get_USB_Device_QueueStatus;  // Get the current port status
        //if PortStatus = FT_OK then               // Port open, all good to update
        //begin
          PortStatus := USB_FT_EE_Program;
          if PortStatus <> FT_OK then      // ERROR here invalid parameter??
          begin
           PortStatus := Close_USB_Device;
           exit;
          end;
          PortStatus := Close_USB_Device;
          if PortStatus <> FT_OK then exit;
        end
        else
        begin
          PortStatus := Close_USB_Device;
          MessageDlg('Port not ready!',mtError, [mbOK], 0);
        end ;


      //end
      //else
        MessageDlg('No Devices available.',mtError, [mbOK], 0);
    end;
  end
  else
   MessageDlg('No Devices available.',mtError, [mbOK], 0);
end;

procedure Tfrm_Main.Timer1Timer(Sender: TObject);
var
  PortStatus : FT_Result;
  NoOfDevs: Integer;
  Device_Description: String;
  Device_Description1: String;
begin

  Device_Description := 'USB <-> Serial Cable';
  Device_Description1 := 'VoiceAmp 601';

  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;
  NoOfDevs := FT_Device_Count;
  if NoOfDevs > 0 then
  begin
    PortStatus := GetFTDeviceDescription(0);
    if PortStatus <> FT_OK then exit;
    if (FT_Device_String = Device_Description) or (FT_Device_String = Device_Description1) then
    begin
      StatusBar1.Panels.Items[0].Text := 'Status: CONNECTED';
      // ---- Test!!
      if GetFTDeviceSerialNo(0) = FT_OK then
        StatusBar1.Panels.Items[1].Text := FT_Device_String;
      // ----

    end;
  end
  else
  begin
    StatusBar1.Panels.Items[0].Text := 'Status: NOT CONNECTED';
    StatusBar1.Panels.Items[1].Text := '';
  end;
end;

end.
