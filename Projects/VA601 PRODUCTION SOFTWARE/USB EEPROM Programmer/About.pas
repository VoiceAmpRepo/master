unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UrlLabel;

type
  Tfrm_About = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Image1: TImage;
    Label1: TLabel;
    UrlLabel1: TUrlLabel;
    pnl_Revision: TPanel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_About: Tfrm_About;

implementation

uses MahFileVer;

{$R *.dfm}



{==========================================================================
 Procedure: Button1Click
 Inputs:    None
 Ouputs:    None
 About:     Close Window
 ==========================================================================}
procedure Tfrm_About.Button1Click(Sender: TObject);
begin
  Close;
end;



{==========================================================================
 Procedure: FormShow
 Inputs:    None
 Ouputs:    None
 About:     Extract current application revision and display
 ==========================================================================}
procedure Tfrm_About.FormShow(Sender: TObject);
var
  oVer : TFileVersion;
  ver : String;
begin
  oVer := TFileVersion.Create;
  ver := oVer.AsString;
  oVer.Destroy;
  pnl_Revision.Caption := ver;
end;

end.
