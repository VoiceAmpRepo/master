object frm_Main: Tfrm_Main
  Left = 463
  Top = 247
  BorderStyle = bsSingle
  Caption = 'VoiceAmp'
  ClientHeight = 357
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 200
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label5'
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 426
    Height = 24
    UseSystemFont = False
    ActionManager = ActionManager1
    Caption = 'ActionMainMenuBar1'
    ColorMap.HighlightColor = 15660791
    ColorMap.BtnSelectedColor = clBtnFace
    ColorMap.UnusedColor = 15660791
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Spacing = 0
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 24
    Width = 426
    Height = 314
    ActivePage = TabSheet2
    Align = alClient
    Style = tsFlatButtons
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      TabVisible = False
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 418
        Height = 304
        Align = alClient
        Caption = 'Employee Number'
        TabOrder = 0
        object Label1: TLabel
          Left = 120
          Top = 80
          Width = 173
          Height = 13
          Caption = 'Enter Employee Number to continue:'
        end
        object edt_EmployNr: TMaskEdit
          Left = 192
          Top = 136
          Width = 33
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BorderStyle = bsNone
          EditMask = '0000;1;_'
          MaxLength = 4
          TabOrder = 0
          Text = '    '
        end
        object Button2: TButton
          Left = 8
          Top = 272
          Width = 75
          Height = 25
          Caption = 'Ok'
          TabOrder = 1
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 96
          Top = 272
          Width = 75
          Height = 25
          Caption = 'Cancel'
          TabOrder = 2
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      TabVisible = False
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 418
        Height = 304
        Align = alClient
        Caption = 'Menu'
        TabOrder = 0
        object Label3: TLabel
          Left = 32
          Top = 56
          Width = 80
          Height = 13
          Caption = 'Select an action:'
        end
        object Button6: TButton
          Left = 32
          Top = 112
          Width = 137
          Height = 25
          Caption = 'Serial Number Programmer'
          TabOrder = 0
          OnClick = Button6Click
        end
        object Button7: TButton
          Left = 32
          Top = 144
          Width = 137
          Height = 25
          Caption = 'USB EEPROM Reader'
          TabOrder = 1
          OnClick = Button7Click
        end
        object Button8: TButton
          Left = 32
          Top = 80
          Width = 137
          Height = 25
          Caption = 'EEPROM Prepare'
          TabOrder = 2
          OnClick = Button8Click
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      TabVisible = False
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 418
        Height = 303
        Align = alClient
        Caption = 'Serial Number Batch Programmer'
        TabOrder = 0
        object Label2: TLabel
          Left = 136
          Top = 80
          Width = 119
          Height = 13
          Caption = 'Enter First Serial Number:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object SerNum: TMaskEdit
          Left = 160
          Top = 132
          Width = 73
          Height = 21
          BevelInner = bvLowered
          BevelOuter = bvRaised
          BevelKind = bkFlat
          BorderStyle = bsNone
          EditMask = '\6\0\1\Z\A\0\70000;1;_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 11
          ParentFont = False
          TabOrder = 0
          Text = '601ZA07    '
        end
        object Button4: TButton
          Left = 8
          Top = 272
          Width = 75
          Height = 25
          Caption = 'Ok'
          TabOrder = 1
          OnClick = Button4Click
        end
        object Button5: TButton
          Left = 96
          Top = 272
          Width = 75
          Height = 25
          Caption = 'Cancel'
          TabOrder = 2
        end
        object Button10: TButton
          Left = 336
          Top = 272
          Width = 75
          Height = 25
          Caption = 'Menu'
          TabOrder = 3
          OnClick = Button10Click
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      TabVisible = False
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 418
        Height = 304
        Align = alClient
        Caption = 'Serial Number Batch Programmer'
        TabOrder = 0
        OnEnter = GroupBox5Enter
        object Label4: TLabel
          Left = 24
          Top = 48
          Width = 182
          Height = 13
          Caption = 'Next Serial Number to be programmed:'
        end
        object Edit1: TEdit
          Left = 24
          Top = 72
          Width = 177
          Height = 37
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          Text = 'Edit1'
        end
        object Button9: TButton
          Left = 24
          Top = 264
          Width = 75
          Height = 25
          Caption = 'Menu'
          TabOrder = 1
          OnClick = Button9Click
        end
        object Button12: TButton
          Left = 24
          Top = 120
          Width = 89
          Height = 65
          Action = Enter
          Caption = 'Program (F5)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'TabSheet5'
      ImageIndex = 4
      TabVisible = False
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 418
        Height = 303
        Align = alClient
        Caption = 'Device USB EEPROM Reader'
        TabOrder = 0
        OnEnter = GroupBox2Enter
        object Label6: TLabel
          Left = 16
          Top = 28
          Width = 48
          Height = 13
          Caption = 'Vendor ID'
        end
        object Label7: TLabel
          Left = 16
          Top = 60
          Width = 51
          Height = 13
          Caption = 'Product ID'
        end
        object Label8: TLabel
          Left = 16
          Top = 92
          Width = 63
          Height = 13
          Caption = 'Manufacturer'
        end
        object Label9: TLabel
          Left = 16
          Top = 124
          Width = 93
          Height = 13
          Caption = 'Product Description'
        end
        object Label12: TLabel
          Left = 16
          Top = 156
          Width = 51
          Height = 13
          Caption = 'Show VCP'
        end
        object Label14: TLabel
          Left = 16
          Top = 188
          Width = 62
          Height = 13
          Caption = 'SerialNr Hide'
        end
        object Label10: TLabel
          Left = 16
          Top = 220
          Width = 66
          Height = 13
          Caption = 'Serial Number'
        end
        object edt_EE_PDesc: TEdit
          Left = 144
          Top = 120
          Width = 121
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
        object edt_EE_MnfID: TEdit
          Left = 144
          Top = 88
          Width = 121
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
        object edt_EE_PID: TEdit
          Left = 144
          Top = 56
          Width = 41
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
        end
        object edt_EE_VID: TEdit
          Left = 144
          Top = 24
          Width = 41
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
        end
        object edt_EE_VCP: TEdit
          Left = 144
          Top = 152
          Width = 17
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
        end
        object edt_EE_ShowSerNr: TEdit
          Left = 144
          Top = 184
          Width = 17
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
        end
        object edt_EE_SerNr: TEdit
          Left = 144
          Top = 216
          Width = 121
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
        end
        object Button1: TButton
          Left = 8
          Top = 272
          Width = 75
          Height = 25
          Caption = 'Read '
          TabOrder = 7
          OnClick = Button1Click
        end
        object Button11: TButton
          Left = 336
          Top = 272
          Width = 75
          Height = 25
          Caption = 'Menu'
          TabOrder = 8
          OnClick = Button11Click
        end
        object edt_EE_VID_Int: TEdit
          Left = 192
          Top = 24
          Width = 41
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
        end
        object edt_EE_PID_Int: TEdit
          Left = 192
          Top = 56
          Width = 41
          Height = 21
          BevelInner = bvLowered
          BevelKind = bkFlat
          BevelOuter = bvRaised
          BorderStyle = bsNone
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 338
    Width = 426
    Height = 19
    Panels = <
      item
        Text = 'Status:'
        Width = 150
      end
      item
        Width = 100
      end>
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Items = <
              item
                Action = FileExit1
                ImageIndex = 43
              end>
            Caption = '&File'
          end
          item
            Items = <
              item
                Action = HelpAbout
              end>
            Caption = '&Help'
          end>
        ActionBar = ActionMainMenuBar1
      end>
    Left = 384
    Top = 8
    StyleName = 'XP Style'
    object FileExit1: TFileExit
      Category = 'File'
      Caption = 'E&xit'
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
    object HelpAbout: TAction
      Category = 'Help'
      Caption = 'About'
      OnExecute = HelpAboutExecute
    end
    object Enter: TAction
      Category = 'Help'
      Caption = 'Program'
      ShortCut = 116
      OnExecute = EnterExecute
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 380
    Top = 46
  end
end
