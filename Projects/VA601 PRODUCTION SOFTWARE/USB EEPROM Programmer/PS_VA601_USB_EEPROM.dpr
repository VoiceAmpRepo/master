program PS_VA601_USB_EEPROM;

uses
  Forms,
  Main in 'Main.pas' {frm_Main},
  D2XXUnit in 'D2XXUnit.pas',
  FT232Comms in 'FT232Comms.pas',
  MahFileVer in 'MahFileVer.pas',
  About in 'About.pas' {frm_About};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_Main, frm_Main);
  Application.CreateForm(Tfrm_About, frm_About);
  Application.Run;
end.
