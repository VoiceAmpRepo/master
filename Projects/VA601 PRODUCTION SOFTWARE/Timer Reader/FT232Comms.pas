unit FT232Comms;

interface

uses D2XXUNIT, SysUtils;

var
  DevicePresent : Boolean;                    // To check availability
  DeviceBusy : Boolean;                       // Flag for comms busy
  Device_Description : String = 'VoiceAmp 601';
  TimeOut: Boolean;                           // Comm timeout
  Count: Integer = 0;			                    // General purpose counter
  PortAIsOpen : boolean;                      // Flag checks if port is open
  USB_Rx_String: String;                      // Storage for receive buffer
  const USBBuffSize : integer = $4000;
  const RxSize: Integer = 256;                // Size of read buffer

  procedure LoadSettings();
  procedure SetSettings();
  //procedure sendUSBstring(s: String);
  function write_value(s: String; DName: String) : boolean;
  function read_value(Bytes: Integer; DName : String) : boolean;

implementation

{==========================================================================
 Procedure: LoadSettings
 Inputs:    None
 Ouputs:    None
 About:     Pre-load the default comms settings into variables
 ==========================================================================}
procedure LoadSettings();
begin
  FT_Current_Baud := FT_BAUD_115200;
  FT_Current_DataBits := FT_DATA_BITS_8;
  FT_Current_StopBits := FT_STOP_BITS_1;
  FT_Current_Parity := FT_PARITY_NONE;
  FT_Current_FlowControl := FT_FLOW_NONE;
end;



{==========================================================================
 Procedure: SetSettings
 Inputs:    None
 Ouputs:    None
 About:     Apply pre-loaded settings to current D2XX port
 ==========================================================================}
procedure SetSettings();
var
  PortStatus : FT_Result;
begin
  PortStatus := Get_USB_Device_QueueStatus;  // Get the current port status
  if PortStatus = FT_OK then               // Port open, all good to update
  begin
    Set_USB_Device_BaudRate;
    Set_USB_Device_DataCharacteristics;
    Set_USB_Device_FlowControl;
    Set_USB_Device_TimeOuts(300,100);
  end;
end;



{==========================================================================
 Procedure: sendUSBstring
 Inputs:    s: String (String to be sent to USB port)
 Ouputs:    None
 About:     Send a string to the USB port
 ==========================================================================}
{procedure sendUSBstring(s: String);
var
  PortStatus: FT_Result;
  TxString: string;
  k,j,i: Integer;           // To store string length
begin
  if DevicePresent then
    PortStatus := Get_USB_Device_QueueStatus;   // Get port status

  if (PortStatus = FT_OK) and DevicePresent then
  begin
    DeviceBusy := True;
    TxString := s + #13#10;
    k := strlen(PChar(TxString));

    // Load string into out buffer
    for i:=0 to k do
      FT_Out_Buffer[i-1] := TxString[i];
    //Application.ProcessMessages;
    j := Write_USB_Device_Buffer(k);
  end;
end;
}


{==========================================================================
 Procedure: OpenPort
 Inputs:    PortName: String
 Ouputs:    Boolean
 About:     Open a USB port
 ==========================================================================}
function OpenPort(PortName : string) : boolean;
Var res : FT_Result;
NoOfDevs,i,J : integer;
Name : String;
DualName : string;
done : boolean;
begin
PortAIsOpen := False;
OpenPort := False;
Name := '';
Dualname := PortName;
res := GetFTDeviceCount;
if res <> Ft_OK then exit;
NoOfDevs := FT_Device_Count;
j := 0;
if NoOfDevs > 0 then
  begin
    repeat
      repeat
      res := GetFTDeviceDescription(J);
      if (res <> Ft_OK) then  J := J + 1;
      until (res = Ft_OK) OR (J=NoOfDevs);
    if res <> Ft_OK then exit;
    done := false;
    i := 1;
    Name := FT_Device_String;
    J := J + 1;
    until (J = NoOfDevs) or (name = DualName);
  end;
if (name = DualName) then
  begin
  res := Open_USB_Device_By_Device_Description(name);
  if res <> Ft_OK then exit;
  OpenPort := true;
  res := Get_USB_Device_QueueStatus;
  if res <> Ft_OK then exit;
  PortAIsOpen := true;
  end
else
  begin
  OpenPort := false;
  end;
end;



{==========================================================================
 Procedure: ClosePort
 Inputs:    None
 Ouputs:    None
 About:     Close active USB port
 ==========================================================================}
procedure ClosePort;
Var res : FT_Result;
begin
if PortAIsOpen then
  res := Close_USB_Device;
PortAIsOpen := False;
end;



{==========================================================================
 Procedure: Init_Controller
 Inputs:    None
 Ouputs:    None
 About:     Initialise USB controller settings
 ==========================================================================}
function Init_Controller(DName : String) : boolean;
var passed : boolean;
res : FT_Result;
begin
Init_Controller := false;
passed := OpenPort(DName);
if passed then
  begin
    res := Set_USB_Device_BaudRate;
    res := Set_USB_Device_DataCharacteristics;
    res := Set_USB_Device_FlowControl;
    res := Set_USB_Device_TimeOuts(300,100);
    if (res = FT_OK) then Init_Controller := true;
  end;
end;



{==========================================================================
 Procedure: write_value
 Inputs:    None
 Ouputs:    None
 About:     REPLACE WITH WRITE_USB_STRING
 ==========================================================================}
function write_value(s: String; DName: String) : boolean;
var
  passed : boolean;
  tmpval : byte;
  res : FT_Result;
  i,j,k: Integer;
  TxString: String;

begin
  write_value := false;
  passed := Init_Controller(DName);
  USB_Rx_String := '';
  if passed then
  begin
    DeviceBusy := True;
    TxString := s +#13#10;
    k := strlen(PChar(TxString));
    for i:=0 to k do
      FT_Out_Buffer[i-1] := TxString[i];
    j := Write_USB_Device_Buffer(k);
    if (j = k) then write_value := true;

    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(RxSize) > 0 then  // Read response if present
      USB_Rx_String := Trim(FT_In_Buffer);     	// Read USB data
      
    closeport;
  end;
end;



{==========================================================================
 Procedure: read_value
 Inputs:    None
 Ouputs:    None
 About:     REPLACE WITH READ_USB_STRING
 ==========================================================================}
function read_value(Bytes: Integer; DName : String) : boolean;
var passed : boolean;
tmpval : byte;
res : FT_Result;
Rx:String;
begin
read_value := false;
passed := Init_Controller(DName);
if passed then
  begin
    if Read_USB_Device_Buffer(Bytes) > 0 then
    begin
      USB_Rx_String := Trim(FT_In_Buffer);     	// Read USB data
      read_value := True;
    end;
  closeport;
  end;
end;

end.
