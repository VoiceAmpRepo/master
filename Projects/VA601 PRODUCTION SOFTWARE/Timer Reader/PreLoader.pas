unit PreLoader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JSILed, ExtCtrls, ComCtrls, Mask, ImgList;

type
  TForm1 = class(TForm)
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    Led1: TJSILed;
    Label1: TLabel;
    Label2: TLabel;
    Run: TButton;
    ImageList1: TImageList;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TmrRead;
    procedure RunClick(Sender: TObject);

  private
    { Private declarations }
    function Split(Chaine: string; Casseur: string; Retour: TStrings): integer;
    function MinutesToDaysHoursMinutes(AMinutes: Integer): string;
    procedure WriteLog(s: String);

  public
    { Public declarations }
    DevicePresent : Boolean;                   // To check availability
    UnitSerial : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
uses
D2XXUnit, MahFileVer, FT232Comms, StrUtils;

{==========================================================================
 Function:  FormShow
 Inputs:    None
 Ouputs:    None
 About:     Initialise on FormShow event
 ==========================================================================}
procedure TForm1.FormShow(Sender: TObject);
var
  oVer : TFileVersion;
  ver : String;
begin
  oVer := TFileVersion.Create;
  ver := oVer.AsString;
  oVer.Destroy;
  Form1.Caption := UpperCase(Application.Title) + ' ' + ver;

  FT_Enable_Error_Report := False; // Turn off internal error reporting
  LoadSettings();		               // Pre-Load USB settings

end;



{==========================================================================
 Function:  Timer1Timer
 Inputs:    None
 Ouputs:    None
 About:     Check for available units on Timer event
 ==========================================================================}
procedure TForm1.Timer1Timer(Sender: TObject);
var
  PortStatus : FT_Result;
  NoOfDevs: Integer;
  Device_Description: String;
  Device_Description1: String;
  Device_Description2: String;
begin
  Device_Description := 'USB <-> Serial Cable';
  Device_Description1 := 'VoiceAmp 601';
  Device_Description2 := 'FT232R USB UART';

  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;
  NoOfDevs := FT_Device_Count;
  if NoOfDevs > 0 then
  begin
    PortStatus := GetFTDeviceDescription(0);
    if PortStatus <> FT_OK then exit;
    if (FT_Device_String = Device_Description) or
       (FT_Device_String = Device_Description1) or
       (FT_Device_String = Device_Description2 )then
    begin
      DevicePresent := True;
      Run.Enabled := True;
      Led1.State := True;
      StatusBar1.Panels.Items[0].Text := 'Status: CONNECTED';

      if GetFTDeviceSerialNo(0) = FT_OK then
      begin
        UnitSerial := FT_Device_String;
        StatusBar1.Panels.Items[1].Text := FT_Device_String;
      end;

    end
    else
    begin
      DevicePresent := False;
      Run.Enabled := False;
      Led1.State := False;
      StatusBar1.Panels.Items[0].Text := 'Status: NOT CONNECTED';
      StatusBar1.Panels.Items[1].Text := '';
    end;
  end
  else
  begin
    DevicePresent := False;
    Run.Enabled := False;
    Led1.State := False;
    StatusBar1.Panels.Items[0].Text := 'Status: NOT CONNECTED';
    StatusBar1.Panels.Items[1].Text := '';
  end;
end;



{==========================================================================
 Function:  TmrRead
 Inputs:    None
 Ouputs:    None
 About:     Read,save and clear timer value
 ==========================================================================}
procedure TForm1.TmrRead();
var
  PortStatus : FT_Result;
  DevCnt, DevCntOld: Integer;
  s, Rx: String;
  strData: TStrings;    // Stores parsed values
  i,j,k:Integer;
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values
  cntr: String;
  timeStr: String;      // Time string
  mins: Integer;        // Minute counter storage
  ovfl: Integer;        // Overflow counter storage
  Total: Integer;
  const DURATION = 720; // 13 HRS operating time
begin
  Run.Enabled := False;
  Timer1.Enabled := False;
  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;

  DevCnt := FT_Device_Count;

  if DevCnt > 0 then
  begin
    for i:=0 to (DevCnt-1) do
    begin
      PortStatus := GetFTDeviceDescription(i);
      if PortStatus <> FT_OK then exit;
      if FT_Device_String = 'ASIX PRESTO' then   // Make sure it's not the programmer!
        DevCnt := DevCnt-1;
      if DevCnt > 0 then
      begin
        //PortStatus := Open_USB_Device;
        PortStatus := Open_USB_Device_By_Device_Description(FT_Device_String);
        if PortStatus <> FT_OK then exit;

        PortStatus := Get_USB_Device_QueueStatus;  // Get the current port status
        if PortStatus = FT_OK then               // Port open, all good to update
        begin

          PortStatus := Close_USB_Device;
          if PortStatus <> FT_OK then exit;

          s := 'AT+TIMER=';
          if DevicePresent then
            write_value(s,'VoiceAmp 601');

          if FT_In_Buffer <> '' then
          begin
            Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
            j:=StrLen(PChar(Rx));         // Save string lenght
            k := AnsiPos('=',Rx);         // Look for '=' sign
            Cmd := LeftStr(Rx,k-1);       // Save only AT cmd
            vals := RightStr(Rx,j-k);     // Save only values

            if Cmd = 'AT+TIMER' then
            begin
              strData := TStringList.Create;
              Split(vals,',',strData);
              mins := StrToInt(strData[0]);
              ovfl := StrToInt(strData[1]);
              Total := (ovfl*65533) + mins;
              TimeStr := MinutesToDaysHoursMinutes(Total);
            end;
          end;

          if Total > DURATION then
            MessageDlg('Passed, ' + IntToStr(Total) + ' minutes.',mtInformation, [mbOK], 0)
          else
            MessageDlg('Failed, ' + IntToStr(Total) + ' minutes!',mtError, [mbOK], 0);

          WriteLog( DateTimeToStr(now) + ',' + UnitSerial + ',' + IntToStr(Total) );

          s := 'AT+TMRCL=0';                    // Clear the usage counter
          if DevicePresent then
            write_value(s,'VoiceAmp 601');

          beep;                                     // Say Goodbye and close


          Application.Terminate;
          Run.Enabled := False;
          exit; // Exit before flashing PRESTO!
        end
        else
        begin
          MessageDlg('Port not ready!',mtError, [mbOK], 0);
        end
      end
      else
        MessageDlg('No Devices available.',mtError, [mbOK], 0);
    end;
  end
  else
   MessageDlg('No Devices available.',mtError, [mbOK], 0);
end;



{==========================================================================
 Function:  RunClick
 Inputs:    None
 Ouputs:    None
 About:     Read the timer value
 ==========================================================================}
procedure TForm1.RunClick(Sender: TObject);
begin
  TmrRead;
end;



{==========================================================================
 Function:  MinutesToDaysHoursMinutes
 Inputs:    Minutes
 Ouputs:    String
 About:     Convert minutes into days, hours, minutes
 ==========================================================================}
function TForm1.MinutesToDaysHoursMinutes(AMinutes: Integer): string;
const
  HOURSPERDAY = 24;
var
  Days: Integer;
  Hours: Integer;
  Minutes: Integer;
begin
  if (AMinutes > 0) then
  begin
    Hours   := AMinutes div 60;
    Minutes := AMinutes mod 60;
    Days    := Hours div HOURSPERDAY;
    Hours   := Hours mod HOURSPERDAY;
  end
  else
  begin
    Hours   := 0;
    Minutes := 0;
    Days    := 0;
  end;
  Result := Format('%.2d Days %.2d Hrs %.2d Minutes', [Days, Hours, Minutes]);
end;



{==========================================================================
 Function:  Split
 Inputs:    Chaine, Casseur, Retour
 Ouputs:    None
 About:     Parse delimeted string, stores results in TStrings
 ==========================================================================}
function TForm1.Split(Chaine: string; Casseur: string; Retour: TStrings): integer;
var
  Start: integer;
  LeftString: string;
  TempString: string;
begin
  TempString := Chaine;                                 //used to check if the last char from
                                                        // "Chaine" is the LineBreaker
  Delete(TempString,1,length(TempString)-1);            // keep the last Char
  if TempString <> Casseur then Chaine := Chaine + Casseur;
                                                        // if the last char is not "Casseur" then put it at the end
  repeat
    Start := Pos(Casseur,Chaine);                       // find the "casseur"
    LeftString := Chaine;                               // Create tempory handler
    Delete(LeftString,Start,Length(LeftString));        // keep the LeftToken
    Delete(Chaine,1,Start);                             // keep the rest of "Chaine"
    Retour.Add (LeftString);                            // Add the new Parse to "Retour"
    until Pos(Casseur,Chaine) = 0;                      // Reapet until end
end;



{==========================================================================
 Procedure: WriteLog
 Inputs:    None
 Ouputs:    None
 About:     Save data to log file
 ==========================================================================}
procedure TForm1.WriteLog(s: String);
var
  F: TextFile;
  path: String;
begin
    GetDir(0, path);                    // Get the working directory
    AssignFile(F, path + '\log.txt');   // Open text file
    if FileExists(path + '\log.txt') then
      Append(F)
    else
      Rewrite(F);
    WriteLn(F, s);
    CloseFile(F);
end;

end.
