unit PreLoader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JSILed, ExtCtrls, ComCtrls, Mask, ImgList;

type
  TForm1 = class(TForm)
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    Led1: TJSILed;
    Label1: TLabel;
    Label2: TLabel;
    Run: TButton;
    SerNum: TMaskEdit;
    ImageList1: TImageList;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure RunClick(Sender: TObject);
    procedure EEWrite;

  private
    { Private declarations }
  public
    { Public declarations }
    DevicePresent : Boolean;                   // To check availability

  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
uses
D2XXUnit, MahFileVer, FT232Comms;


procedure TForm1.FormShow(Sender: TObject);
var
  oVer : TFileVersion;
  ver : String;
begin
  oVer := TFileVersion.Create;
  ver := oVer.AsString;
  oVer.Destroy;
  Form1.Caption := UpperCase(Application.Title) + ' ' + ver;

  FT_Enable_Error_Report := False; // Turn off internal error reporting
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  PortStatus : FT_Result;
  NoOfDevs: Integer;
  Device_Description: String;
  Device_Description1: String;
  Device_Description2: String;
begin
  Device_Description := 'USB <-> Serial Cable';
  Device_Description1 := 'VoiceAmp 601';
  Device_Description2 := 'FT232R USB UART';

  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;
  NoOfDevs := FT_Device_Count;
  if NoOfDevs > 0 then
  begin
    PortStatus := GetFTDeviceDescription(0);
    if PortStatus <> FT_OK then exit;
    if (FT_Device_String = Device_Description) or
       (FT_Device_String = Device_Description1) or
       (FT_Device_String = Device_Description2 )then
    begin
      DevicePresent := True;
      Run.Enabled := True;
      Led1.State := True;
      StatusBar1.Panels.Items[0].Text := 'Status: CONNECTED';
      // ---- Test!!
      if GetFTDeviceSerialNo(0) = FT_OK then
        StatusBar1.Panels.Items[1].Text := FT_Device_String;
      // ----

    end
    else
    begin
      DevicePresent := False;
      Run.Enabled := False;
      Led1.State := False;
      StatusBar1.Panels.Items[0].Text := 'Status: NOT CONNECTED';
      StatusBar1.Panels.Items[1].Text := '';
    end;
  end
  else
  begin
    DevicePresent := False;
    Run.Enabled := False;
    Led1.State := False;
    StatusBar1.Panels.Items[0].Text := 'Status: NOT CONNECTED';
    StatusBar1.Panels.Items[1].Text := '';
  end;
end;

procedure TForm1.RunClick(Sender: TObject);
var
 s: String;
begin
 s := SerNum.Text;
 if Length(Trim(s)) < 11 then
   MessageDlg('Incorrect Serial Number Format!',mtError, [mbOK], 0)
 else
 begin
  Timer1.Enabled := False;
  EEWrite;
  Timer1.Enabled := True;
 end;
end;

procedure TForm1.EEWrite();
var
  PortStatus : FT_Result;
  DevCnt, DevCntOld: Integer;
  i: Integer;
  s: String;
begin
  Run.Enabled := False;
  Timer1.Enabled := False;
  PortStatus := GetFTDeviceCount;
  if PortStatus <> FT_OK then exit;

  DevCnt := FT_Device_Count;

  if DevCnt > 0 then
  begin
    for i:=0 to (DevCnt-1) do
    begin
      PortStatus := GetFTDeviceDescription(i);
      if PortStatus <> FT_OK then exit;
      if FT_Device_String = 'ASIX PRESTO' then   // Make sure it's not the programmer!
        DevCnt := DevCnt-1;
      if DevCnt > 0 then
      begin
         // =========== Load EE variables ===========================
        EEDataBuffer.Version := 2;  // 0 for AM/BM, 1 for C, 2 for R
        EEDataBuffer.Signature1 := 0;
        EEDataBuffer.Signature2 := 4294967295;
        EEDataBuffer.Version := 2;  // 0 for AM/BM, 1 for C, 2 for R
        EEDataBuffer.VendorId := 1027;
        EEDataBuffer.ProductID := 24577;
        EEDataBuffer.Manufacturer := 'VoiceAmp';
        EEDataBuffer.ManufacturerID := 'FT';
        EEDataBuffer.Description := 'VoiceAmp 601';
        EEDataBuffer.SerialNumber := PChar(Trim(SerNum.Text));
        EEDataBuffer.MaxPower := 100;
        EEDataBuffer.PnP := 1;
        EEDataBuffer.SelfPowered := 0;
        EEDataBuffer.RemoteWakeup := 0;
        EEDataBuffer.Rev4 := 0;
        EEDataBuffer.IsoIn := 0;
        EEDataBuffer.IsoOut := 0;
        EEDataBuffer.PullDownEnable := 0;
        EEDataBuffer.SerNumEnable := 0;
        EEDataBuffer.USBVersionEnable := 0;
        EEDataBuffer.USBVersion := 272;
        EEDataBuffer.Rev5 := 0;
        EEDataBuffer.IsoInA := 0;
        EEDataBuffer.IsoInB := 0;
        EEDataBuffer.IsoOutA := 0;
        EEDataBuffer.IsoOutB := 0;
        EEDataBuffer.PullDownEnable5 := 0;
        EEDataBuffer.SerNumEnable5 := 0;
        EEDataBuffer.USBVersionEnable5 := 0;
        EEDataBuffer.USBVersion5 := 0;
        EEDataBuffer.AIsHighCurrent := 0;
        EEDataBuffer.BIsHighCurrent := 0;
        EEDataBuffer.IFAIsFifo := 0;
        EEDataBuffer.IFAIsFifoTar := 0;
        EEDataBuffer.IFAIsFastSer := 0;
        EEDataBuffer.AIsVCP := 0;
        EEDataBuffer.IFBIsFifo := 0;
        EEDataBuffer.IFBIsFifoTar := 0;
        EEDataBuffer.IFBIsFastSer := 0;
        EEDataBuffer.BIsVCP := 0;
        EEDataBuffer.UseExtOsc := 0;
        EEDataBuffer.HighDriveIOs := 0;
        EEDataBuffer.EndpointSize := 64;
        EEDataBuffer.PullDownEnableR := 0;
        EEDataBuffer.SerNumEnableR := 0;
        EEDataBuffer.InvertTXD := 0;
        EEDataBuffer.InvertRXD := 0;
        EEDataBuffer.InvertRTS := 0;
        EEDataBuffer.InvertCTS := 0;
        EEDataBuffer.InvertDTR := 0;
        EEDataBuffer.InvertDSR := 0;
        EEDataBuffer.InvertDCD := 0;
        EEDataBuffer.InvertRI := 0;
        EEDataBuffer.Cbus0 := 15;
        EEDataBuffer.Cbus1 := 15;
        EEDataBuffer.Cbus2 := 15;
        EEDataBuffer.Cbus3 := 15;
        EEDataBuffer.Cbus4 := 15;
        EEDataBuffer.RIsVCP := 0;
       // =========== Load EE variables ===========================

        //PortStatus := Open_USB_Device;
        PortStatus := Open_USB_Device_By_Device_Description(FT_Device_String);
        if PortStatus <> FT_OK then exit;

        PortStatus := Get_USB_Device_QueueStatus;  // Get the current port status
        if PortStatus = FT_OK then               // Port open, all good to update
        begin
          PortStatus := USB_FT_EE_Program;
          if PortStatus <> FT_OK then exit;     // ERROR here invalid parameter??

          PortStatus := Close_USB_Device;
          if PortStatus <> FT_OK then exit;

          beep;

          s := 'AT+TMRCL=0';                    // Clear the usage counter
          if DevicePresent then
            write_value(s,'VoiceAmp 601');

                                                // Say Goodbye and close
          MessageDlg('Serial number ' +  Trim(SerNum.Text)
                      + #13#10 + 'successfully programmed!.'
          ,mtInformation, [mbOK], 0);

          Application.Terminate;
          Run.Enabled := False;
          exit; // Exit before flashing PRESTO!
        end
        else
        begin
          MessageDlg('Port not ready!',mtError, [mbOK], 0);
        end
      end
      else
        MessageDlg('No Devices available.',mtError, [mbOK], 0);
    end;
  end
  else
   MessageDlg('No Devices available.',mtError, [mbOK], 0);
end;

end.
