program VAProdManager;

uses
  Forms,
  MAIN in 'Src\MAIN.PAS' {frmMain},
  Data in 'Src\Data.pas' {frmData},
  Login in 'Src\Login.pas' {frmLogin},
  Barcode in 'Src\Barcode.pas' {frmBarcode};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'VoiceAmp Production Manager';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmData, frmData);
  Application.Run;
end.
