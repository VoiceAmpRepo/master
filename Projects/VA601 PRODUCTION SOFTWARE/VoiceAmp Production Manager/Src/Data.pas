unit Data;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, CPort;

type
  TfrmData = class(TForm)
    DataSource1: TDataSource;
    cnt_MainDatabase: TADOConnection;
    ADOQuery1: TADOQuery;
    query_password: TADOQuery;
    ComPort1: TComPort;
    ComDataPacket1: TComDataPacket;
    procedure ComDataPacket1Packet(Sender: TObject; const Str: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmData: TfrmData;

implementation

uses MAIN;

{$R *.dfm}



{==========================================================================
 Procedure: ComDataPacket1Packet
 Inputs:    None
 Ouputs:    None
 About:     String received on comPort
 ==========================================================================}
procedure TfrmData.ComDataPacket1Packet(Sender: TObject;
  const Str: String);
begin
  if Str = 'TEST' then
  begin
    frmMain.BarcodeTest := True;
  end;
end;

end.
