{==========================================================================
 Project:   VA601 FT232BM USB DLL driver interface test
 Author:    Andr� Hoek
 Copyright: VoiceAmp Ltd

 Revision history:
  Rev         Date          Comments
  ---         ----          --------
  0.0.1.0     12 Jun 2006   Initial pre-production framework
 ==========================================================================}
unit MAIN;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, Menus,
  StdCtrls, Dialogs, Buttons, Messages, ExtCtrls, ComCtrls, StdActns,
  ActnList, ToolWin, ImgList, XPStyleActnCtrls, ActnMan, ActnCtrls,
  ActnMenus;

type
  TfrmMain = class(TForm)
    OpenDialog: TOpenDialog;
    ImageList1: TImageList;
    ActionMainMenuBar1: TActionMainMenuBar;
    ActionToolBar1: TActionToolBar;
    ActionManager1: TActionManager;
    FileExit2: TFileExit;
    Panel1: TPanel;
    Panel2: TPanel;
    TreeView1: TTreeView;
    WindowTileHorizontal2: TWindowTileHorizontal;
    WindowCascade2: TWindowCascade;
    WindowTileVertical2: TWindowTileVertical;
    WindowMinimizeAll2: TWindowMinimizeAll;
    WindowArrange1: TWindowArrange;
    act_LogIn: TAction;
    act_LogOut: TAction;
    act_SetupScan: TAction;
    StatusBar1: TStatusBar;
    tmr_Login: TTimer;
    actn_About: TAction;
    procedure FileExit1Execute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure act_LogInExecute(Sender: TObject);
    procedure tmr_LoginTimer(Sender: TObject);
    procedure act_SetupScanExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    LoggedIn: Boolean;                    // User Logged In?
    CurrentUsername: String;              // Current username
    BarcodeTest: Boolean;                 // Barcode Test PASS/FAIL Flag
  end;

var
  frmMain: TfrmMain;

implementation

uses Data, Login, Barcode;


{$R *.dfm}


{==========================================================================
 Procedure: FileExit1Execute
 Inputs:    None
 Ouputs:    None
 About:     Exit Application
 ==========================================================================}
procedure TfrmMain.FileExit1Execute(Sender: TObject);
begin
  Close;
end;



{==========================================================================
 Procedure: FormShow
 Inputs:    None
 Ouputs:    None
 About:     Form show event handler
 ==========================================================================}
procedure TfrmMain.FormShow(Sender: TObject);
begin
  LoggedIn := False;            // Reset user logged in flag
  CurrentUsername := '';        // Clear username
end;



{==========================================================================
 Procedure: act_LogInExecute
 Inputs:    None
 Ouputs:    None
 About:     Login action handler
 ==========================================================================}
procedure TfrmMain.act_LogInExecute(Sender: TObject);
var
  frmLogin: TfrmLogin;
begin
    // Create login form and show
    frmLogin := TfrmLogin.Create(Self);
    frmLogin.show;
end;



{==========================================================================
 Procedure: act_SetupScanExecute
 Inputs:    None
 Ouputs:    None
 About:     Barcode scanner setup action
 ==========================================================================}
procedure TfrmMain.act_SetupScanExecute(Sender: TObject);
var
  frmBarcode: TfrmBarcode;
begin
    // Create barcode form and show
    frmBarcode := TfrmBarcode.Create(Self);
    frmBarcode.show;
end;



{==========================================================================
 Procedure: tmr_LoginTimer
 Inputs:    None
 Ouputs:    None
 About:     Enable/Disable controls on user Login
 ==========================================================================}
procedure TfrmMain.tmr_LoginTimer(Sender: TObject);
begin
  act_SetupScan.Enabled := LoggedIn;
end;




end.
