object frmData: TfrmData
  Left = 368
  Top = 168
  Width = 254
  Height = 215
  Caption = 'frmData'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DataSource1: TDataSource
    Left = 40
    Top = 56
  end
  object cnt_MainDatabase: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Extended Properti' +
      'es="DSN=MS Access Database;DBQ=C:\Documents and Settings\sinkpla' +
      'at\My Documents\Delphi\VA601 PRODUCTION SOFTWARE\VoiceAmp Produc' +
      'tion Manager\Data\VAProd_100.mdb;DefaultDir=C:\Documents and Set' +
      'tings\sinkplaat\My Documents\Delphi\VA601 PRODUCTION SOFTWARE\Vo' +
      'iceAmp Production Manager\Data;DriverId=25;FIL=MS Access;MaxBuff' +
      'erSize=2048;PageTimeout=5;UID=admin;"'
    LoginPrompt = False
    Left = 8
    Top = 8
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 8
    Top = 56
  end
  object query_password: TADOQuery
    Connection = cnt_MainDatabase
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'User'
        DataType = ftString
        Precision = 1
        Size = 10
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT * FROM [tbl_UserName]'
      'WHERE [tbl_UserName].[UserName] = '#39'sinkplaat'#39)
    Left = 8
    Top = 88
  end
  object ComPort1: TComPort
    BaudRate = br9600
    Port = 'COM1'
    Parity.Bits = prNone
    StopBits = sbOneStopBit
    DataBits = dbEight
    Events = [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR, evError, evRLSD, evRx80Full]
    FlowControl.OutCTSFlow = False
    FlowControl.OutDSRFlow = False
    FlowControl.ControlDTR = dtrDisable
    FlowControl.ControlRTS = rtsDisable
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    Left = 192
    Top = 16
  end
  object ComDataPacket1: TComDataPacket
    ComPort = ComPort1
    OnPacket = ComDataPacket1Packet
    Left = 192
    Top = 48
  end
end
