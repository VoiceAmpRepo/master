unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmLogin = class(TForm)
    Label1: TLabel;
    edit_Username: TEdit;
    Label2: TLabel;
    edit_Password: TEdit;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    procedure btn_CancelClick(Sender: TObject);
    procedure btn_OkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

uses Data, MAIN;

{$R *.dfm}



{==========================================================================
 Procedure: btn_CancelClick
 Inputs:    None
 Ouputs:    None
 About:     Close on cancel
 ==========================================================================}
procedure TfrmLogin.btn_CancelClick(Sender: TObject);
begin
  Close;
end;



{==========================================================================
 Procedure: btn_OkClick
 Inputs:    None
 Ouputs:    None
 About:     Login User
 ==========================================================================}
procedure TfrmLogin.btn_OkClick(Sender: TObject);
var
  username: String;
  password: String;
begin
  username := Trim(edit_Username.Text);     // Cleanup username
  if StrLen(PChar(username))<1 then         // Parameter cannot be zero length
    username := ' ';
  // Connect to db
  with frmData do
  begin
    cnt_MainDatabase.Connected := True;
    with query_password do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'SELECT * FROM tbl_UserName ' +
                'WHERE UserName = :User';
      Parameters.ParamByName('User').Value := username;
      Open;
    end;
  end;
  // User does not exist
  if frmData.query_password.RecordCount < 1 then
  begin
    MessageDlg( 'Not a valid username!',mtError,[mbOK],0);
    frmMain.StatusBar1.Panels[0].Text := 'USER: None';
  end
  // User exists, check password
  else
  begin
    password := frmData.query_password.FieldByName('Password').AsString;
    if password = edit_Password.Text then
    begin
      frmMain.LoggedIn := True;                       // Set Login Flag
      frmData.query_password.Close;                   // Close Query
      frmData.cnt_MainDatabase.Connected := False;    // Close DB connection
      frmMain.StatusBar1.Panels[0].Text := 'USER: ' + username;
      Close;
    end
    else
      MessageDlg( 'Incorrect password!',mtError,[mbOK],0);
  end;
  // Disconnect to db
  frmData.cnt_MainDatabase.Connected := False;
end;



{==========================================================================
 Procedure: FormClose
 Inputs:    None
 Ouputs:    None
 About:     Free from memory on Close
 ==========================================================================}
procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Action := caFree;
end;

end.
