object frmBarcode: TfrmBarcode
  Left = 491
  Top = 282
  Width = 295
  Height = 154
  BorderIcons = [biSystemMenu]
  Caption = 'Barcode Scanner Setup'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object btn_Setup: TButton
    Left = 8
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Setup Port'
    TabOrder = 0
    OnClick = btn_SetupClick
  end
  object btn_Test: TButton
    Left = 88
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Test'
    TabOrder = 1
    OnClick = btn_TestClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 287
    Height = 81
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 2
    object lbl_Barcode: TLabel
      Left = 8
      Top = 8
      Width = 56
      Height = 13
      Caption = 'lbl_Barcode'
      Visible = False
    end
    object lbl_TestResult: TLabel
      Left = 8
      Top = 32
      Width = 116
      Height = 20
      Caption = 'lbl_TestResult'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object ProgressBar1: TProgressBar
      Left = 8
      Top = 56
      Width = 273
      Height = 17
      TabOrder = 0
    end
  end
  object btn_Done: TButton
    Left = 200
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Done'
    TabOrder = 3
    OnClick = btn_DoneClick
  end
  object tmr_Barcode: TTimer
    Enabled = False
    Interval = 250
    OnTimer = tmr_BarcodeTimer
    Left = 248
    Top = 8
  end
end
