unit Barcode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls;

type
  TfrmBarcode = class(TForm)
    btn_Setup: TButton;
    btn_Test: TButton;
    Panel1: TPanel;
    ProgressBar1: TProgressBar;
    btn_Done: TButton;
    tmr_Barcode: TTimer;
    lbl_Barcode: TLabel;
    lbl_TestResult: TLabel;
    procedure btn_DoneClick(Sender: TObject);
    procedure btn_SetupClick(Sender: TObject);
    procedure tmr_BarcodeTimer(Sender: TObject);
    procedure btn_TestClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBarcode: TfrmBarcode;

implementation

uses Data, MAIN;

{$R *.dfm}



{==========================================================================
 Procedure: btn_OkClick
 Inputs:    None
 Ouputs:    None
 About:     Close form when done
 ==========================================================================}
procedure TfrmBarcode.btn_DoneClick(Sender: TObject);
begin
  frmData.ComPort1.Close;
  Close;
end;



{==========================================================================
 Procedure: btn_Setup
 Inputs:    None
 Ouputs:    None
 About:     Setup barcode scanner
 ==========================================================================}
procedure TfrmBarcode.btn_SetupClick(Sender: TObject);
begin
  frmData.ComPort1.ShowSetupDialog;
end;



{==========================================================================
 Procedure: tmr_BarcodeTimer
 Inputs:    None
 Ouputs:    None
 About:     TimeOut the barcode test
 ==========================================================================}
procedure TfrmBarcode.tmr_BarcodeTimer(Sender: TObject);
begin
  if ( frmMain.BarcodeTest ) and ( ProgressBar1.Position < 100 ) then
  begin
    tmr_Barcode.Enabled := False;
    lbl_Barcode.Caption := 'Done.';
    lbl_TestResult.Font.Color := clGreen;
    lbl_TestResult.Caption := 'PASS!';
  end;
  if ProgressBar1.Position < 100 then
  begin
    ProgressBar1.Position :=  ProgressBar1.Position + 1;
  end
  else
  begin
    tmr_Barcode.Enabled := False;
    lbl_Barcode.Caption := 'Done.';
    lbl_TestResult.Font.Color := clRed;
    lbl_TestResult.Caption := 'FAIL!';
  end;
end;



{==========================================================================
 Procedure: btn_Test
 Inputs:    None
 Ouputs:    None
 About:     Run the barcode test
 ==========================================================================}
procedure TfrmBarcode.btn_TestClick(Sender: TObject);
begin
  lbl_Barcode.Visible := True;
  lbl_Barcode.Caption :=  'Scan the test barcode in the test instruction...';

  tmr_Barcode.Enabled := True;

  lbl_TestResult.Visible := True;
  lbl_TestResult.Caption := 'Waiting...';

  frmData.ComPort1.Open;
end;

                

{==========================================================================
 Procedure: FormClose
 Inputs:    None
 Ouputs:    None
 About:     Free from memory on Close
 ==========================================================================}
procedure TfrmBarcode.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action := caFree;
end;

end.
