object frmLogin: TfrmLogin
  Left = 774
  Top = 660
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Login'
  ClientHeight = 122
  ClientWidth = 263
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 48
    Height = 13
    Caption = 'Username'
  end
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object edit_Username: TEdit
    Left = 72
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object edit_Password: TEdit
    Left = 72
    Top = 48
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object btn_Ok: TButton
    Left = 48
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Ok'
    TabOrder = 2
    OnClick = btn_OkClick
  end
  object btn_Cancel: TButton
    Left = 136
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 3
    OnClick = btn_CancelClick
  end
end
