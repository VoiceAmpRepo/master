program VA601_Discharge;

uses
  Forms,
  Main in 'Main.pas' {frm_Main},
  D2XXUnit in 'D2XXUnit.pas',
  MahFileVer in 'MahFileVer.pas',
  About in 'About.pas' {frmAbout},
  Logger in 'Logger.pas' {frm_Logger};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'VA601 Battery Discharge Test';
  Application.CreateForm(Tfrm_Main, frm_Main);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.CreateForm(Tfrm_Logger, frm_Logger);
  Application.Run;
end.
