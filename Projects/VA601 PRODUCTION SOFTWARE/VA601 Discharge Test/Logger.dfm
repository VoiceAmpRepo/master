object frm_Logger: Tfrm_Logger
  Left = 370
  Top = 171
  Width = 553
  Height = 480
  Caption = 'Battery Data Logger vA00'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 545
    Height = 97
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 45
      Height = 13
      Caption = 'Filename:'
    end
    object Edit1: TEdit
      Left = 64
      Top = 12
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object Button1: TButton
      Left = 8
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 2
    end
    object JSILed1: TJSILed
      Left = 224
      Top = 8
      Width = 23
      Height = 27
      Position = poTop
      TrueColor = clRed
      FalseColor = clGray
      Caption = 'CHG'
    end
    object JSILed2: TJSILed
      Left = 256
      Top = 8
      Width = 22
      Height = 27
      Position = poTop
      TrueColor = clRed
      FalseColor = clGray
      Caption = 'PPR'
    end
    object JSILed3: TJSILed
      Left = 288
      Top = 8
      Width = 22
      Height = 27
      Position = poTop
      TrueColor = clRed
      FalseColor = clGray
      Caption = 'USB'
    end
    object JSILed4: TJSILed
      Left = 256
      Top = 40
      Width = 51
      Height = 27
      Position = poTop
      TrueColor = clLime
      FalseColor = clGray
      Caption = 'USB PWR'
    end
    object JSILed5: TJSILed
      Left = 320
      Top = 8
      Width = 22
      Height = 27
      Position = poTop
      TrueColor = clRed
      FalseColor = clGray
      Caption = 'CEN'
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 97
    Width = 241
    Height = 349
    Align = alLeft
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 1
  end
  object Memo2: TMemo
    Left = 241
    Top = 97
    Width = 304
    Height = 349
    Align = alClient
    TabOrder = 2
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10000
    Left = 192
    Top = 48
  end
end
