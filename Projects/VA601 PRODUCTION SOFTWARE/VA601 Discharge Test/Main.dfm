object frm_Main: Tfrm_Main
  Left = 283
  Top = 201
  Width = 650
  Height = 654
  Caption = 'VA601 Pre-production Development Interface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 601
    Width = 642
    Height = 19
    Panels = <
      item
        Width = 85
      end
      item
        Width = 70
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 401
    Height = 601
    Align = alLeft
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 311
      Height = 16
      Caption = 'VA601 Pre-production Development Interface'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 32
      Width = 74
      Height = 13
      Caption = 'Current Device:'
    end
    object Label3: TLabel
      Left = 8
      Top = 48
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object Label4: TLabel
      Left = 8
      Top = 64
      Width = 39
      Height = 13
      Caption = 'Voltage:'
    end
    object Label5: TLabel
      Left = 8
      Top = 80
      Width = 89
      Height = 13
      Caption = 'Software Revision:'
    end
    object Label6: TLabel
      Left = 8
      Top = 96
      Width = 80
      Height = 13
      Caption = 'Production Date:'
    end
    object lbl_Serial: TLabel
      Left = 120
      Top = 32
      Width = 80
      Height = 16
      Caption = 'lbl_Serial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
    end
    object lbl_Status: TLabel
      Left = 120
      Top = 48
      Width = 57
      Height = 13
      Caption = 'lbl_Status'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Voltage: TLabel
      Left = 120
      Top = 64
      Width = 64
      Height = 13
      Caption = 'lbl_Voltage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_SWRev: TLabel
      Left = 120
      Top = 80
      Width = 54
      Height = 13
      Caption = 'lbl_SWRev'
    end
    object lbl_ProdDate: TLabel
      Left = 120
      Top = 96
      Width = 61
      Height = 13
      Caption = 'lbl_ProdDate'
    end
    object Label7: TLabel
      Left = 8
      Top = 160
      Width = 65
      Height = 13
      Caption = 'Current Bank:'
    end
    object lbl_Bank: TLabel
      Left = 120
      Top = 160
      Width = 50
      Height = 13
      Caption = 'lbl_Bank'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 8
      Top = 112
      Width = 74
      Height = 13
      Caption = 'Usage Counter:'
    end
    object lbl_Usage: TLabel
      Left = 120
      Top = 112
      Width = 47
      Height = 13
      Caption = 'lbl_Usage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 8
      Top = 128
      Width = 63
      Height = 13
      Caption = 'USB voltage:'
    end
    object lbl_USBvolts: TLabel
      Left = 120
      Top = 128
      Width = 60
      Height = 13
      Caption = 'lbl_USBvolts'
    end
    object Label10: TLabel
      Left = 264
      Top = 384
      Width = 22
      Height = 13
      Caption = 'PPR'
    end
    object Label11: TLabel
      Left = 264
      Top = 400
      Width = 23
      Height = 13
      Caption = 'CHG'
    end
    object Label12: TLabel
      Left = 8
      Top = 552
      Width = 23
      Height = 13
      Caption = 'Time'
    end
    object Button1: TButton
      Left = 8
      Top = 320
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 320
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 168
      Top = 320
      Width = 75
      Height = 25
      Caption = 'Reset'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 248
      Top = 320
      Width = 75
      Height = 25
      Caption = 'Get Usage'
      TabOrder = 3
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 248
      Top = 352
      Width = 75
      Height = 25
      Caption = 'Clear timer'
      TabOrder = 4
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 8
      Top = 352
      Width = 75
      Height = 25
      Caption = 'Clear Memo'
      TabOrder = 5
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 8
      Top = 288
      Width = 75
      Height = 25
      Caption = 'Get EEPROM'
      TabOrder = 6
      OnClick = Button7Click
    end
    object Edit1: TEdit
      Left = 0
      Top = 520
      Width = 241
      Height = 21
      TabOrder = 7
      Text = 'AT+'
    end
    object Button8: TButton
      Left = 248
      Top = 520
      Width = 75
      Height = 25
      Caption = 'Send'
      TabOrder = 8
      OnClick = Button8Click
    end
    object StringGrid1: TStringGrid
      Left = 8
      Top = 176
      Width = 393
      Height = 105
      BorderStyle = bsNone
      ColCount = 9
      DefaultColWidth = 42
      RowCount = 4
      ScrollBars = ssNone
      TabOrder = 9
    end
    object Button9: TButton
      Left = 88
      Top = 352
      Width = 75
      Height = 25
      Caption = 'Get Revision'
      TabOrder = 10
      OnClick = Button9Click
    end
    object Button10: TButton
      Left = 168
      Top = 352
      Width = 75
      Height = 25
      Caption = 'Get Date'
      TabOrder = 11
      OnClick = Button10Click
    end
    object Button11: TButton
      Left = 8
      Top = 384
      Width = 75
      Height = 25
      Caption = 'USB pwr ON'
      TabOrder = 12
      OnClick = Button11Click
    end
    object Button12: TButton
      Left = 88
      Top = 384
      Width = 75
      Height = 25
      Caption = 'USB pwr OFF'
      TabOrder = 13
      OnClick = Button12Click
    end
    object Button13: TButton
      Left = 168
      Top = 384
      Width = 75
      Height = 25
      Caption = 'Get Vbat'
      TabOrder = 14
      OnClick = Button13Click
    end
    object JSILed2: TJSILed
      Left = 296
      Top = 400
      Width = 12
      Height = 15
      Position = poTop
      TrueColor = clLime
      FalseColor = clGray
    end
    object JSILed1: TJSILed
      Left = 296
      Top = 384
      Width = 12
      Height = 15
      Position = poTop
      TrueColor = clRed
      FalseColor = clGray
    end
    object Button14: TButton
      Left = 8
      Top = 416
      Width = 75
      Height = 25
      Caption = 'Get PPR'
      TabOrder = 17
      OnClick = Button14Click
    end
    object Button15: TButton
      Left = 88
      Top = 416
      Width = 75
      Height = 25
      Caption = 'Get CHG'
      TabOrder = 18
      OnClick = Button15Click
    end
    object Button16: TButton
      Left = 8
      Top = 448
      Width = 75
      Height = 25
      Caption = 'USB chg ON'
      TabOrder = 19
      OnClick = Button16Click
    end
    object Button17: TButton
      Left = 88
      Top = 448
      Width = 75
      Height = 25
      Caption = 'USB chg OFF'
      TabOrder = 20
      OnClick = Button17Click
    end
    object Button18: TButton
      Left = 8
      Top = 480
      Width = 75
      Height = 25
      Caption = 'CHG Enable'
      TabOrder = 21
      OnClick = Button18Click
    end
    object Button19: TButton
      Left = 88
      Top = 480
      Width = 75
      Height = 25
      Caption = 'CHG Disable'
      TabOrder = 22
      OnClick = Button19Click
    end
    object Button20: TButton
      Left = 248
      Top = 480
      Width = 75
      Height = 25
      Caption = 'Data Logger'
      TabOrder = 23
      OnClick = Button20Click
    end
    object Button21: TButton
      Left = 168
      Top = 480
      Width = 75
      Height = 25
      Caption = 'Charge Sum'
      TabOrder = 24
      OnClick = Button21Click
    end
    object Edit2: TEdit
      Left = 40
      Top = 552
      Width = 65
      Height = 21
      TabOrder = 25
    end
    object Button22: TButton
      Left = 248
      Top = 568
      Width = 75
      Height = 25
      Caption = 'Convert'
      TabOrder = 26
      OnClick = Button22Click
    end
    object Edit3: TEdit
      Left = 40
      Top = 576
      Width = 193
      Height = 21
      TabOrder = 27
    end
  end
  object Memo1: TMemo
    Left = 401
    Top = 0
    Width = 241
    Height = 601
    Align = alClient
    BevelInner = bvLowered
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clBtnFace
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    TabOrder = 2
  end
  object tmr_Connected: TTimer
    Interval = 50
    OnTimer = tmr_ConnectedTimer
    Left = 480
    Top = 288
  end
  object tmr_GetVolts: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmr_GetVoltsTimer
    Left = 480
    Top = 256
  end
end
