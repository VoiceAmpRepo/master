{==========================================================================
 Project:   VoiceAmp 601 general purpose interface
 Author:    Andr� Hoek
 Copyright: Ukwakha Consulting cc

 Revision history:
  Rev         Date          Comments
  ---         ----          --------
  1.00        20 Feb 2005   Initial Release
 ==========================================================================}
unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, ToolWin, ActnMan, ActnCtrls,
  ActnMenus, Grids, JSILed;

type
  Tfrm_Main = class(TForm)
    tmr_Connected: TTimer;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lbl_Serial: TLabel;
    lbl_Status: TLabel;
    lbl_Voltage: TLabel;
    lbl_SWRev: TLabel;
    lbl_ProdDate: TLabel;
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    tmr_GetVolts: TTimer;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Edit1: TEdit;
    Button8: TButton;
    StringGrid1: TStringGrid;
    Label7: TLabel;
    lbl_Bank: TLabel;
    Label8: TLabel;
    lbl_Usage: TLabel;
    Button9: TButton;
    Button10: TButton;
    Label9: TLabel;
    lbl_USBvolts: TLabel;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    JSILed2: TJSILed;
    Label10: TLabel;
    Label11: TLabel;
    JSILed1: TJSILed;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    Button17: TButton;
    Button18: TButton;
    Button19: TButton;
    Button20: TButton;
    Button21: TButton;
    Edit2: TEdit;
    Label12: TLabel;
    Button22: TButton;
    Edit3: TEdit;
    procedure tmr_ConnectedTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure tmr_GetVoltsTimer(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure Button19Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure Button21Click(Sender: TObject);
    procedure Button22Click(Sender: TObject);
  private
    { Private declarations }
    procedure LoadSettings();
    procedure SetSettings();
    procedure sendUSBstring(s: String);
    function Split(Chaine: string; Casseur: string; Retour: TStrings): integer;
    function MinutesToDaysHoursMinutes(AMinutes: Integer): string;
  public
    { Public declarations }

   DevicePresent : Boolean;                      // To check availability
   DeviceBusy : Boolean;                         // Flag for comms busy
  end;

var
  frm_Main: Tfrm_Main;

implementation

{$R *.dfm}

uses D2XXUnit, About, MahFileVer, StrUtils, Logger;

var

 Device_Description : String = 'VoiceAmp 601'; // USB descriptor



{==========================================================================
 Procedure: sendUSBstring
 Inputs:    s: String (String to be sent to USB port)
 Ouputs:    None
 About:     Send a string to the USB port
 ==========================================================================}
procedure Tfrm_Main.sendUSBstring(s: String);
var
  PortStatus: FT_Result;
  TxString: string;
  k,j,t,i: Integer;           // To store string length
begin

  if DevicePresent then
    PortStatus := Get_USB_Device_QueueStatus;   // Get port status

  if (PortStatus = FT_OK) and (not DeviceBusy) and DevicePresent then
  begin
    tmr_Connected.Enabled := False;             // Stop polling for connection

    DeviceBusy := True;
    TxString := s + #13#10;
    k := strlen(PChar(TxString));

    // Load string into out buffer
    for i:=0 to k do
      FT_Out_Buffer[i-1] := TxString[i];

    Application.ProcessMessages;
    j := Write_USB_Device_Buffer(k);

    tmr_Connected.Enabled := True;
    DeviceBusy := False;
  end;
end;



{==========================================================================
 Procedure: SetSettings
 Inputs:    None
 Ouputs:    None
 About:     Apply pre-loaded settings to current D2XX port
 ==========================================================================}
procedure Tfrm_Main.SetSettings();
var
  PortStatus : FT_Result;
begin
  PortStatus := Get_USB_Device_QueueStatus;  // Get the current port status
  if PortStatus = FT_OK then               // Port open, all good to update
  begin
    Set_USB_Device_BaudRate;
    Set_USB_Device_DataCharacteristics;
    Set_USB_Device_FlowControl;
    Set_USB_Device_TimeOuts(300,300);
  end;
end;



{==========================================================================
 Procedure: LoadSettings
 Inputs:    None
 Ouputs:    None
 About:     Pre-load the default comms settings into variable
 ==========================================================================}
procedure Tfrm_Main.LoadSettings();
begin
  FT_Current_Baud := FT_BAUD_115200;
  FT_Current_DataBits := FT_DATA_BITS_8;
  FT_Current_StopBits := FT_STOP_BITS_1;
  FT_Current_Parity := FT_PARITY_NONE;
  FT_Current_FlowControl := FT_FLOW_NONE;
end;



{==========================================================================
 Procedure: tmr_ConnectedTimer
 Inputs:    None
 Ouputs:    None
 About:     Check for device availability on timer event
 ==========================================================================}
procedure Tfrm_Main.tmr_ConnectedTimer(Sender: TObject);
var
  PortStatus : FT_Result;
begin
  GetFTDeviceCount;
  if (FT_Device_Count > 0) and DevicePresent then
  begin
    StatusBar1.Panels.Items[0].Text := 'Connected';
    lbl_Status.Caption := 'Connected';
  end
  else if (FT_Device_Count > 0) and (not DevicePresent) then
  begin
    LoadSettings;
    GetFTDeviceSerialNo (0);
    lbl_Serial.Caption := FT_Device_String;
    if Open_USB_Device_By_Device_Description(Device_Description) = FT_OK then
    begin
      SetSettings;
      DevicePresent := True;
    end;
  end
  else begin
    StatusBar1.Panels.Items[0].Text := 'NOT Connected!';
    lbl_Status.Caption := 'No status available';
    lbl_Serial.Caption := 'No device present';
    DevicePresent := False;
  end;
end;

{==========================================================================
 Procedure: FormShow
 Inputs:    None
 Ouputs:    None
 About:     Cleanup on form show event
 ==========================================================================}
procedure Tfrm_Main.FormShow(Sender: TObject);
var
  i:Integer;
begin
  lbl_Serial.Caption := '';
  lbl_Status.Caption := '';
  lbl_ProdDate.Caption := '';
  lbl_SWRev.Caption := '';
  lbl_Voltage.Caption := '';
  lbl_Bank.Caption := '';
  lbl_Usage.Caption := '';
  lbl_USBvolts.Caption := '';
  
  Memo1.Lines.Clear;

  { ==Setup grid== }
  for i:= 1 to 3 do
  begin
    StringGrid1.Rows[i].SetText(PChar(IntToStr(i)));
    StringGrid1.RowHeights[i] := 15;
  end;

  StringGrid1.ColWidths[0] := 15;

  StringGrid1.Cols[1].SetText('Effect');
  StringGrid1.ColWidths[1] := 42;
  StringGrid1.Cols[2].SetText('Ambient');
  StringGrid1.ColWidths[2] := 42;
  StringGrid1.Cols[3].SetText('Master');
  StringGrid1.ColWidths[3] := 42;
  StringGrid1.Cols[4].SetText('Delay');
  StringGrid1.ColWidths[4] := 42;
  StringGrid1.Cols[5].SetText('Shift');
  StringGrid1.ColWidths[5] := 42;
  StringGrid1.Cols[6].SetText('Thresh');
  StringGrid1.ColWidths[6] := 42;
  StringGrid1.Cols[7].SetText('Rlease');
  StringGrid1.ColWidths[7] := 42;
  StringGrid1.Cols[8].SetText('Socket');
  StringGrid1.ColWidths[8] := 42;

    FT_Enable_Error_Report := False; // Turn off internal error reporting

end;



{==========================================================================
 Procedure: Button1Click
 Inputs:    None
 Ouputs:    None
 About:     Start test timer
 ==========================================================================}
procedure Tfrm_Main.Button1Click(Sender: TObject);
begin
  tmr_GetVolts.Enabled := True;
end;



{==========================================================================
 Procedure: Button2Click
 Inputs:    None
 Ouputs:    None
 About:     Stop test timer
 ==========================================================================}
procedure Tfrm_Main.Button2Click(Sender: TObject);
begin
  tmr_GetVolts.Enabled := False;
end;



{==========================================================================
 Procedure: Button3Click
 Inputs:    None
 Ouputs:    None
 About:     Reset the device
 ==========================================================================}
procedure Tfrm_Main.Button3Click(Sender: TObject);
var
  s:String;
begin
  s := 'AT+RESET=';
  sendUSBstring(s);
end;



{==========================================================================
 Procedure: tmr_GetVoltsTimer
 Inputs:    None
 Ouputs:    None
 About:     Get battery voltage on timer event
 ==========================================================================}
procedure Tfrm_Main.tmr_GetVoltsTimer(Sender: TObject);
var
  s, Rx, volts, str_date, str_Time: String;
  i,j,k: Integer;
  d: Double;
begin
  if DevicePresent then
  begin
    FT_In_Buffer := '';           // Clear the receive buffer
    s := 'AT+BATVS=';
    sendUSBstring(s);
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);     // Save Rx string
      i := AnsiPos('=',Rx)-1;       // Look for '=' sign
      Rx := RightStr(Rx,i);         // Save only voltage
      str_Date := DateToStr(Date);
      str_Time := TimeToStr(Time);
      lbl_Voltage.Caption := Rx;
      Memo1.lines.add(str_Date + ' ' + str_Time + #9 +Rx);
      FT_In_Buffer := '';           // Clear the receive buffer
    end;
  end;
end;



{==========================================================================
 Procedure: Button4Click
 Inputs:    None
 Ouputs:    None
 About:     Get usage stats from unit
 ==========================================================================}
procedure Tfrm_Main.Button4Click(Sender: TObject);
var
  s, Rx: String;
  strData: TStrings;    // Stores parsed values
  mins: Integer;        // Minute counter storage
  ovfl: Integer;        // Overflow counter storage
  total: Integer;       // Total calculated usage in minutes
  i,j:Integer;
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values
  timeStr: String;      // Time string
begin
  if DevicePresent then
  begin
    s := 'AT+TIMER=';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);      // Lose NL/CR etc
      j:=StrLen(PChar(Rx));          // Save string lenght
      Memo1.lines.add(Rx);
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+TIMER' then
      begin
        strData := TStringList.Create;
        Split(vals,',',strData);
        mins := StrToInt(strData[0]);
        ovfl := StrToInt(strData[1]);
        Total := (ovfl*65533) + mins;
        TimeStr := MinutesToDaysHoursMinutes(Total);
        Memo1.Lines.Add(TimeStr);
        lbl_Usage.Caption := TimeStr;
      end;
    end;
  end;
end;



{==========================================================================
 Procedure: Button5Click
 Inputs:    None
 Ouputs:    None
 About:     Clear the usage timers
 ==========================================================================}
procedure Tfrm_Main.Button5Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+TMRCL=';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;



{==========================================================================
 Procedure: Button6Click
 Inputs:    None
 Ouputs:    None
 About:     Clear the memo
 ==========================================================================}
procedure Tfrm_Main.Button6Click(Sender: TObject);
begin
  Memo1.lines.Clear;
end;



{==========================================================================
 Procedure: Button7Click
 Inputs:    None
 Ouputs:    None
 About:     Get the user settings from EEPROM
 ==========================================================================}
procedure Tfrm_Main.Button7Click(Sender: TObject);
var
  s, Rx: String;
  strData: TStrings;    // Stores parsed values
  i,j,k:Integer;        // Counters
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values

begin
  if DevicePresent then
  begin
    s := 'AT+GETST=';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);      // Lose NL/CR etc
      j:=StrLen(PChar(Rx));          // Save string lenght
      Memo1.lines.add(Rx);
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETST' then
      begin
        strData := TStringList.Create;
        Split(vals,',',strData);        // Parse value string

        k := StrToInt(strData[0]);       // Extract values
        lbl_Bank.Caption := IntToStr(k);
        for i := 1 to 8 do
        begin
          k := StrToInt(strData[i]);       // Extract values in BANK1
          StringGrid1.Cells[i,1] := IntToStr(k);
        end;
        for i := 1 to 8 do
        begin
          k := StrToInt(strData[i+8]);       // Extract values in BANK2
          StringGrid1.Cells[i,2] := IntToStr(k);
        end;
        for i := 1 to 8 do
        begin
          k := StrToInt(strData[i+16]);       // Extract values in BANK3
          StringGrid1.Cells[i,3] := IntToStr(k);
        end;
      end;
    end;
  end;
end;



{==========================================================================
 Procedure: Button8Click
 Inputs:    None
 Ouputs:    None
 About:     Send custom command
 ==========================================================================}
procedure Tfrm_Main.Button8Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := Edit1.Text;
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;



{==========================================================================
 Function:  Split
 Inputs:    Chaine, Casseur, Retour
 Ouputs:    None
 About:     Parse delimeted string, stores results in TStrings
 ==========================================================================}
function Tfrm_Main.Split(Chaine: string; Casseur: string; Retour: TStrings): integer;
var
  Start: integer;
  LeftString: string;
  TempString: string;
begin
  TempString := Chaine;                                 //used to check if the last char from
                                                        // "Chaine" is the LineBreaker
  Delete(TempString,1,length(TempString)-1);            // keep the last Char
  if TempString <> Casseur then Chaine := Chaine + Casseur;
                                                        // if the last char is not "Casseur" then put it at the end
  repeat
    Start := Pos(Casseur,Chaine);                       // find the "casseur"
    LeftString := Chaine;                               // Create tempory handler
    Delete(LeftString,Start,Length(LeftString));        // keep the LeftToken
    Delete(Chaine,1,Start);                             // keep the rest of "Chaine"
    Retour.Add (LeftString);                            // Add the new Parse to "Retour"
    until Pos(Casseur,Chaine) = 0;                      // Reapet until end
end;



{==========================================================================
 Function:  MinutesToDaysHoursMinutes
 Inputs:    Minutes
 Ouputs:    String
 About:     Convert minutes into days, hours, minutes
 ==========================================================================}
function Tfrm_Main.MinutesToDaysHoursMinutes(AMinutes: Integer): string;
const
  HOURSPERDAY = 24;
var
  Days: Integer;
  Hours: Integer;
  Minutes: Integer;
begin
  if (AMinutes > 0) then
  begin
    Hours   := AMinutes div 60;
    Minutes := AMinutes mod 60;
    Days    := Hours div HOURSPERDAY;
    Hours   := Hours mod HOURSPERDAY;
  end
  else
  begin
    Hours   := 0;
    Minutes := 0;
    Days    := 0;
  end;
  Result := Format('%.2d Days %.2d Hrs %.2d Minutes', [Days, Hours, Minutes]);
end;


{==========================================================================
 Function:  Button9Click
 Inputs:    None
 Ouputs:    None
 About:     Get the firmware revision
 ==========================================================================}
procedure Tfrm_Main.Button9Click(Sender: TObject);
var
 s, Rx: String;
  i,j:Integer;        // Counters
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values
begin
  if DevicePresent then
  begin
    s := 'AT+GETRV=';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);      // Lose NL/CR etc
      j:=StrLen(PChar(Rx));          // Save string lenght
      Memo1.lines.add(Rx);
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETRV' then
        lbl_SWRev.Caption := vals;
    end;
  end;
end;



{==========================================================================
 Function:  Button10Click
 Inputs:    None
 Ouputs:    None
 About:     Get the firmware release date
 ==========================================================================}
procedure Tfrm_Main.Button10Click(Sender: TObject);
var
  s, Rx: String;
  i,j:Integer;          // Counters
  cmd: String;          // Stores received AT command
  vals: String;         // Stores received values
begin
  if DevicePresent then
  begin
    s := 'AT+GETDT=';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);      // Lose NL/CR etc
      j:=StrLen(PChar(Rx));          // Save string lenght
      Memo1.lines.add(Rx);
      i := AnsiPos('=',Rx);         // Look for '=' sign
      Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
      vals := RightStr(Rx,j-i);     // Save only values
      if Cmd = 'AT+GETDT' then
        lbl_ProdDate.Caption := vals;
    end;
  end;
end;



{==========================================================================
 Function:  Button11Click
 Inputs:    None
 Ouputs:    None
 About:     Turn on USB power
 ==========================================================================}
procedure Tfrm_Main.Button11Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+USBPW=1';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;



{==========================================================================
 Function:  Button12Click
 Inputs:    None
 Ouputs:    None
 About:     Turn off USB power
 ==========================================================================}
procedure Tfrm_Main.Button12Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+USBPW=0';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;



{==========================================================================
 Function:  Button13Click
 Inputs:    None
 Ouputs:    None
 About:     Get battery voltage
 ==========================================================================}
procedure Tfrm_Main.Button13Click(Sender: TObject);
var
  s, Rx, volts: String;
  i: Integer;
  d: Double;
  f: Extended;

begin
  if DevicePresent then
  begin
    s := 'AT+BATVS=';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);     // Save Rx string
      Memo1.lines.add(Rx);
      i := AnsiPos('=',Rx)-1;       // Look for '=' sign
      Rx := RightStr(Rx,i);         // Save only voltage
      f := StrToFloat(Rx);
      lbl_Voltage.Caption := FloatToStr(f);
    end;
  end;
end;



{==========================================================================
 Function:  Button14Click
 Inputs:    None
 Ouputs:    None
 About:     Check the Power Present Flag
 ==========================================================================}
procedure Tfrm_Main.Button14Click(Sender: TObject);
var
  s, Rx: String;
  i,j: Integer;
begin
  if DevicePresent then
  begin
    s := 'AT+CGPPR=?';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      j:=StrLen(PChar(Rx));          // Save string lenght
      Memo1.lines.add(Rx);
      i := AnsiPos('=',Rx);       // Look for '=' sign
      Rx := RightStr(Rx,j-i);       // Save only flag
      i := StrToInt(Rx);
      case i of
      0: JSILed1.State := True;
      1: JSILed1.State := False;
      end;
    end;
  end;
end;



{==========================================================================
 Function:  Button15Click
 Inputs:    None
 Ouputs:    None
 About:     Check the Charging status Flag
 ==========================================================================}
procedure Tfrm_Main.Button15Click(Sender: TObject);
var
  s, Rx: String;
  i,j: Integer;
begin
  if DevicePresent then
  begin
    s := 'AT+CGCHG=?';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      j:=StrLen(PChar(Rx));          // Save string lenght
      Memo1.lines.add(Rx);
      i := AnsiPos('=',Rx);       // Look for '=' sign
      Rx := RightStr(Rx,j-i);       // Save only flag
      i := StrToInt(Rx);
      case i of
      0: JSILed2.State := True;
      1: JSILed2.State := False;
      end;
    end;
  end;
end;



{==========================================================================
 Function:  Button16Click
 Inputs:    None
 Ouputs:    None
 About:     Enable USB charging
 ==========================================================================}
procedure Tfrm_Main.Button16Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+USBCH=1';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;



{==========================================================================
 Function:  Button17Click
 Inputs:    None
 Ouputs:    None
 About:     Disable USB charging
 ==========================================================================}
procedure Tfrm_Main.Button17Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+USBCH=0';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;



{==========================================================================
 Function:  Button18Click
 Inputs:    None
 Ouputs:    None
 About:     Enable charger
 ==========================================================================}
procedure Tfrm_Main.Button18Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+CHGEN=0';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;



{==========================================================================
 Function:  Button19Click
 Inputs:    None
 Ouputs:    None
 About:     Enable USB charging
 ==========================================================================}
procedure Tfrm_Main.Button19Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+CHGEN=1';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;

procedure Tfrm_Main.Button20Click(Sender: TObject);
begin
  frm_Logger.Show;  
end;



{==========================================================================
 Function:  Button21Click
 Inputs:    None
 Ouputs:    None
 About:     Get charge summary
 ==========================================================================}
procedure Tfrm_Main.Button21Click(Sender: TObject);
var
  s, Rx: String;
begin
  if DevicePresent then
  begin
    s := 'AT+CHGSU=?';
    sendUSBstring(s);
    FT_In_Buffer := '';
    if Read_USB_Device_Buffer(256) > 0 then
    begin
      Rx := Trim(FT_In_Buffer);
      Memo1.lines.add(Rx);
    end;
  end;
end;

procedure Tfrm_Main.Button22Click(Sender: TObject);
var
  int: Integer;
begin
  int := StrToInt(Edit2.Text);
  Edit3.Text := MinutesToDaysHoursMinutes(int);
end;

end.
