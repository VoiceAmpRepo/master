unit AudioMix;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, ExTrackBar, StdCtrls, D2XXUnit, FT232Comms;

type
  Tfrm_AudioMix = class(TForm)
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    tb_AAF: TExTrackBar;
    tb_Phone: TExTrackBar;
    tb_MasterRcv: TExTrackBar;
    pnlAAF: TPanel;
    pnlPhone: TPanel;
    pnlMasterRcv: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    tb_Voice: TExTrackBar;
    tb_Playback: TExTrackBar;
    tb_MasterSnd: TExTrackBar;
    pnl_Voice: TPanel;
    pnl_Playback: TPanel;
    pnl_MasterSnd: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure tb_AAFChange(Sender: TObject);
    procedure tb_MasterRcvChange(Sender: TObject);
    procedure tb_PhoneChange(Sender: TObject);
    procedure tb_VoiceChange(Sender: TObject);
    procedure tb_PlaybackChange(Sender: TObject);
    procedure tb_MasterSndChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
 DevicePresent : Boolean;                   // To check availability
 DeviceBusy : Boolean;                      // Flag for comms busy
 Device_Description : String = 'VoiceAmp 601';

  frm_AudioMix: Tfrm_AudioMix;

implementation

uses VA601_USB, StrUtils;

{$R *.dfm}

procedure Tfrm_AudioMix.tb_AAFChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 255-tb_AAF.Position;
  s := 'AT+POTST=1,' +  IntToStr(val);
  val := round((val/255)*100);
  pnlAAF.Caption := IntToStr(val) + '%';
  if frmMain.DevicePresent then
    write_value(s,Device_Description);
end;

procedure Tfrm_AudioMix.tb_MasterRcvChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 255-tb_MasterRcv.Position;
  s := 'AT+POTST=3,' +  IntToStr(val);
  val := round((val/255)*100);
  pnlMasterRcv.Caption := IntToStr(val) + '%';
  if frmMain.DevicePresent then
    write_value(s,Device_Description);
end;

procedure Tfrm_AudioMix.tb_PhoneChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 255-tb_Phone.Position;
  s := 'AT+POTST=2,' +  IntToStr(val);
  val := round((val/255)*100);
  pnlPhone.Caption := IntToStr(val) + '%';
  if frmMain.DevicePresent then
    write_value(s,Device_Description);
end;

procedure Tfrm_AudioMix.tb_VoiceChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 255-tb_Voice.Position;
  s := 'AT+POTST=4,' +  IntToStr(val);
  val := round((val/255)*100);
  pnl_Voice.Caption := IntToStr(val) + '%';
  if frmMain.DevicePresent then
    write_value(s,Device_Description);
end;

procedure Tfrm_AudioMix.tb_PlaybackChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 255-tb_Playback.Position;
  s := 'AT+POTST=5,' +  IntToStr(val);
  val := round((val/255)*100);
  pnl_Playback.Caption := IntToStr(val) + '%';
  if frmMain.DevicePresent then
    write_value(s,Device_Description);
end;

procedure Tfrm_AudioMix.tb_MasterSndChange(Sender: TObject);
var
  s: String;
  val: Integer;
begin
  val := 255-tb_MasterSnd.Position;
  s := 'AT+POTST=6,' +  IntToStr(val);
  val := round((val/255)*100);
  pnl_MasterSnd.Caption := IntToStr(val) + '%';
  if frmMain.DevicePresent then
    write_value(s,Device_Description);
end;

procedure Tfrm_AudioMix.Button1Click(Sender: TObject);
var
  s: String;
  val1,val2,val3,val4,val5,val6 : Integer;

 strData: TStrings;    // Stores parsed values
 i,j:Integer;
 Rx, cmd, vals: String;          // Stores received AT command

begin
  val1 := 255-tb_AAF.Position;
  val2 := 255-tb_Phone.Position;
  val3 := 255-tb_MasterRcv.Position;
  val4 := 255-tb_Voice.Position;
  val5 := 255-tb_Playback.Position;
  val6 := 255-tb_MasterSnd.Position;

  s := 'AT+POTEE=' +  IntToStr(val1) + ','
                   +  IntToStr(val2) + ','
                   +  IntToStr(val3) + ','
                   +  IntToStr(val4) + ','
                   +  IntToStr(val5) + ','
                   +  IntToStr(val6);

//  if frmMain.DevicePresent then
//   write_value(s,Device_Description);

     if frmMain.DevicePresent then
     begin
       write_value(s,Device_Description);
       Rx := Trim(FT_In_Buffer);     // Lose NL/CR etc
       j:=StrLen(PChar(Rx));         // Save string lenght
       i := AnsiPos('=',Rx);         // Look for '=' sign
       Cmd := LeftStr(Rx,i-1);       // Save only AT cmd
       vals := RightStr(Rx,j-i);     // Save only values
       if Cmd = 'AT+POTEE' then
         showMessage('Values saved!')
       else
         showMessage('Failed!')
      end;

end;

procedure Tfrm_AudioMix.Button2Click(Sender: TObject);
var
 s:string;
begin
  s := 'AT+PLAYD=1';
    if frmMain.DevicePresent then
     begin
       write_value(s,Device_Description);
     end;
end;

end.

