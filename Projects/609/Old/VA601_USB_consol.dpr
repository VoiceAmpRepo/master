program VA601_USB_consol;

uses
  Forms,
  VA601_USB in 'VA601_USB.pas' {frmMain},
  D2XXUnit in 'D2XXUnit.pas',
  CheckPrevious in 'CheckPrevious.pas',
  About in 'About.pas' {frmAbout},
  MahFileVer in 'MahFileVer.pas',
  Message in 'Message.pas' {frm_Message},
  Feedback in 'Feedback.pas' {frm_Feedback},
  FT232Comms in 'FT232Comms.pas',
  AudioMix in 'AudioMix.pas' {frm_AudioMix};

{$R *.res}

begin
  // Only allow one instance of the software to run
  if not CheckPrevious.RestoreIfRunning(Application.Handle,1) then
  begin
    Application.Initialize;
    Application.Title := 'VoiceAmp 601 Control Panel';
    Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.CreateForm(Tfrm_Message, frm_Message);
  Application.CreateForm(Tfrm_Feedback, frm_Feedback);
  Application.CreateForm(Tfrm_AudioMix, frm_AudioMix);
  Application.Run;
  end;
end.
