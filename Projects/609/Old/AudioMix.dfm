object frm_AudioMix: Tfrm_AudioMix
  Left = 777
  Top = 307
  Width = 383
  Height = 339
  Caption = 'TAD Audio Mixer Interface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 177
    Height = 257
    Caption = 'Receive Mixer'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    object Label13: TLabel
      Left = 66
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Phone'
    end
    object Label12: TLabel
      Left = 17
      Top = 24
      Width = 20
      Height = 13
      Caption = 'AAF'
    end
    object Label14: TLabel
      Left = 123
      Top = 24
      Width = 32
      Height = 13
      Caption = 'Master'
    end
    object tb_AAF: TExTrackBar
      Left = 8
      Top = 40
      Width = 45
      Height = 185
      Ctl3D = True
      Max = 255
      Orientation = trVertical
      ParentCtl3D = False
      ParentShowHint = False
      Frequency = 24
      Position = 255
      ShowHint = True
      TabOrder = 0
      TickMarks = tmBoth
      OnChange = tb_AAFChange
      Color = clBtnFace
      ParentColor = False
      Labels = False
      LabelsFactor = 1
      LabelsFactorOperation = foDiv
      LabelsInterval = 2
      SelEnable = False
    end
    object tb_Phone: TExTrackBar
      Left = 64
      Top = 40
      Width = 45
      Height = 185
      Max = 255
      Orientation = trVertical
      Frequency = 24
      Position = 255
      TabOrder = 1
      TickMarks = tmBoth
      OnChange = tb_PhoneChange
      Labels = False
      LabelsFactor = 1
      LabelsFactorOperation = foMul
      LabelsInterval = 2
      SelEnable = False
    end
    object tb_MasterRcv: TExTrackBar
      Left = 120
      Top = 40
      Width = 45
      Height = 185
      LineSize = 3270
      Max = 255
      Orientation = trVertical
      Frequency = 24
      Position = 255
      TabOrder = 2
      TickMarks = tmBoth
      OnChange = tb_MasterRcvChange
      Labels = False
      LabelsFactor = 1
      LabelsFactorOperation = foMul
      LabelsInterval = 2
      SelEnable = False
    end
    object pnlAAF: TPanel
      Left = 9
      Top = 232
      Width = 42
      Height = 17
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 3
    end
    object pnlPhone: TPanel
      Left = 65
      Top = 232
      Width = 42
      Height = 17
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 4
    end
    object pnlMasterRcv: TPanel
      Left = 121
      Top = 232
      Width = 42
      Height = 17
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 5
    end
  end
  object GroupBox2: TGroupBox
    Left = 192
    Top = 8
    Width = 177
    Height = 257
    Caption = 'Send Mixer'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 58
      Top = 24
      Width = 44
      Height = 13
      Caption = 'Playback'
    end
    object Label2: TLabel
      Left = 9
      Top = 24
      Width = 27
      Height = 13
      Caption = 'Voice'
    end
    object Label3: TLabel
      Left = 123
      Top = 24
      Width = 32
      Height = 13
      Caption = 'Master'
    end
    object tb_Voice: TExTrackBar
      Left = 8
      Top = 40
      Width = 45
      Height = 185
      Ctl3D = True
      Max = 255
      Orientation = trVertical
      ParentCtl3D = False
      ParentShowHint = False
      Frequency = 24
      Position = 255
      ShowHint = True
      TabOrder = 0
      TickMarks = tmBoth
      OnChange = tb_VoiceChange
      Color = clBtnFace
      ParentColor = False
      Labels = False
      LabelsFactor = 1
      LabelsFactorOperation = foDiv
      LabelsInterval = 2
      SelEnable = False
    end
    object tb_Playback: TExTrackBar
      Left = 64
      Top = 40
      Width = 45
      Height = 185
      Max = 255
      Orientation = trVertical
      Frequency = 24
      Position = 255
      TabOrder = 1
      TickMarks = tmBoth
      OnChange = tb_PlaybackChange
      Labels = False
      LabelsFactor = 1
      LabelsFactorOperation = foMul
      LabelsInterval = 2
      SelEnable = False
    end
    object tb_MasterSnd: TExTrackBar
      Left = 120
      Top = 40
      Width = 45
      Height = 185
      LineSize = 3270
      Max = 255
      Orientation = trVertical
      Frequency = 24
      Position = 255
      TabOrder = 2
      TickMarks = tmBoth
      OnChange = tb_MasterSndChange
      Labels = False
      LabelsFactor = 1
      LabelsFactorOperation = foMul
      LabelsInterval = 2
      SelEnable = False
    end
    object pnl_Voice: TPanel
      Left = 9
      Top = 232
      Width = 42
      Height = 17
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 3
    end
    object pnl_Playback: TPanel
      Left = 65
      Top = 232
      Width = 42
      Height = 17
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 4
    end
    object pnl_MasterSnd: TPanel
      Left = 121
      Top = 232
      Width = 42
      Height = 17
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 5
    end
  end
  object Button1: TButton
    Left = 288
    Top = 272
    Width = 75
    Height = 25
    Caption = 'Commit'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 272
    Width = 75
    Height = 25
    Caption = 'Playback'
    TabOrder = 3
    OnClick = Button2Click
  end
end
